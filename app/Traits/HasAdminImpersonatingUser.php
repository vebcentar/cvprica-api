<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait HasAdminImpersonatingUser
{
    public function getUser(): ?User
    {
        /** @var User $actualUser */
        if (!$actualUser = Auth::user()) {
            return null;
        }

        if (!$actualUser->isAdmin() || !request()->user_id) {
            return null;
        }

        return User::findOrFail(request()->user_id);
    }
}
