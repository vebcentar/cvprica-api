<?php

namespace App\Http\Controllers;

use App\Services\EmailVerificationService;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CallbackController extends BaseController
{
    public function verifyEmail($code, EmailVerificationService $service)
    {
        if (!\is_string($code)) {
            return new BadRequestHttpException();
        }

        $service->verifyEmail($code);

        return view('emails.email_verification_success');
    }
}
