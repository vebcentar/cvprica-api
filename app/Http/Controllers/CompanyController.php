<?php

namespace App\Http\Controllers;

use App\Models\CvVideo;
use App\Services\MediaService;
use App\Services\S3ClientService;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CompanyUser;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\Blog;
use App\Models\BlogImages;
use App\Models\SocialMedia;
use App\Models\Package;
use App\Models\PackagePurchaseHistory;
use App\Models\Ad;
use App\Models\AdSharedInfo;
use App\Models\AdQuestion;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    // function for updating company
    public function updateCompany(Request $request)
    {

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }


        $company = auth()->user();

        if (!$company) {
            return response()->json(['success' => false, 'message' => 'Company not found'], 404);
        }


        $validator = Validator::make($request->all(), [
            'full_name' => 'nullable|string',
            'email' =>  'nullable|email|unique:users,email,' . $user->id,
            'password' => 'nullable|string|min:8',
            'phone' => User::phoneRulesArray(false, true),
            'birth_year' => 'nullable|integer',
            'aditional_info' => 'nullable|string',
            'pib' => 'nullable|string',
            'address' => 'nullable|string',
            'zip_code' => 'nullable|integer',
            'website' => 'nullable|string',
            'facebook' => 'nullable|string',
            'instagram' => 'nullable|string',
            'contact_email' => 'nullable|string',
            'notification'  => 'nullable|boolean',
            'language_id'   => 'nullable|integer|exists:App\Models\Language,id',
            'company_activity'  =>  'nullable|string',
            // FROM FIRST COMPANY INFORMATION
            'contact_person'    =>  'nullable|string',
            'contact_person_position'    =>  'nullable|string',
            'contact_phone'    =>  'nullable|string',
            'contact_mail'    =>  'nullable|string',
            'contact_website'    =>  'nullable|string',
            'number_of_employees_from'  =>  'nullable|integer',
            'number_of_employees_to'    =>  'nullable|integer|gte:number_of_employees_from'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $validatedData  = $validator->valid();

        // if(isset($validatedData['company_activity']))
        //     unset($validatedData['company_activity']);


        $validatedData["modified_by"] = $user->id;
        if ($request->password) {
            $validatedData["password"] = bcrypt($validatedData['password']);
        }


        $user->update($validatedData);

        if($company = $user->company_users()->first())
            $company->update($validatedData);
        else
            $user->company_users()->create($validatedData);


        // $input = $request->except(['company_activity']);

        // $contact = CompanyUser::where("user_id", $company->id);
        // $activity = CompanyActivity::where("id", $company->company_activity);

        // if (!$contact->first()) {
        //     CompanyUser::create(["user_id" => $company->id, "contact_person" =>  $request->contact_person, "contact_person_position" => $request->contact_person_position, "contact_phone" => $request->contact_phone, "contact_mail" => $request->contact_mail]);
        // } else {
        //     $contact->update(["contact_person" =>  $request->contact_person, "contact_person_position" => $request->contact_person_position, "contact_phone" => $request->contact_phone, "contact_mail" => $request->contact_mail]);
        // }

        // if ($request->company_activity) {

        //     $activity = CompanyActivity::find($company->company_activity);

        //     if (!$activity) {
        //         $activity = CompanyActivity::create(["name" => $request->company_activity, "created_by" => $user->id]);
        //         $input["company_activity"] = $activity->id;
        //     } else {
        //         $activity->update(["name" => $request->company_activity]);
        //     }
        // }

        // $company->update($input);

        return response()->json(['success' => true, 'message' => 'Company updated'], 200);
    }

    // function for adding company user
    public function addCompanyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "contact_person" => "required",
            "contact_person_position" => "required",
            "contact_phone" => "required",
            "contact_mail" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $companyUser = CompanyUser::create($input);
        return response()->json(['success' => true, 'message' => "Company user created"], 200);
    }

    // function for updating particular company user
    public function updateParticularCompanyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\CompanyUser,id",
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->except(['id', 'user_id']);
        $input["modified_by"] = $user->id;
        $companyUser = CompanyUser::find($request->id);
        if (!$companyUser) {
            return response()->json(['success' => false, 'message' => "User not found"], 404);
        }
        if ($user->id != $companyUser->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $companyUser->update($input);
        return response()->json(['success' => true, 'message' => "Company user updated", "companyUser" => $companyUser], 200);
    }

    // function for removing company user
    public function removeCompanyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\CompanyUser,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $companyUser = CompanyUser::find($request->id);
        if ($companyUser->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $companyUser->delete();
        return response()->json(['success' => true, 'message' => "Company user deleted"], 200);
    }

    // function for adding company gallery
    public function addCompanyGallery(Request $request)
    {

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $companyGallery = Gallery::create($input);
        return response()->json(['success' => true, 'message' => "Gallery created"], 200);
    }

    // function for removing gallery
    public function removeCompanyGallery(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\Gallery,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $companyGallery = Gallery::find($request->id);
        if ($companyGallery->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $companyGallery->delete();
        return response()->json(['success' => true, 'message' => "Company user deleted"], 200);
    }

    // function for adding image in gallery
    public function addGalleryImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "gallery_id" => "required|integer|exists:App\Models\Gallery,id",
            "image" => MediaService::imageRequestRuleArray(),
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        if ($file = $request->file('image')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images/gallery/' . $request->gallery_id, $name);
            $input['image'] = 'images/gallery/' . $request->gallery_id . "/" . $name;
        }
        $input["created_by"] = $user->id;
        $image = GalleryImage::create($input);
        return response()->json(['success' => true, 'user' => $user, "message" => "Image uploaded"], 200);
    }

    // function for removing image from gallery
    public function removeGalleryImage(Request $request, MediaService $mediaService)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\GalleryImage,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $image = GalleryImage::find($request->id);
        if ($image->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $mediaService->deleteFile($image->image);
        $image->delete();
        return response()->json(['success' => true, 'message' => "Image deleted"], 200);
    }

    // function for adding company blog
    public function addCompanyBlog(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => "required",
            "text" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $companyBlog = Blog::create($input);
        return response()->json(['success' => true, 'message' => "Blog created"], 200);
    }

    // function for removing blog
    public function removeCompanyBlog(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\Blog,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $companyBlog = Blog::find($request->id);
        if ($companyBlog->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $companyBlog->delete();
        return response()->json(['success' => true, 'message' => "Company user deleted"], 200);
    }

    // function for adding image in blog
    public function addBlogImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "blog_id" => "required|integer|exists:App\Models\Blog,id",
            "image" => MediaService::imageRequestRuleArray()
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        if ($file = $request->file('image')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images/blog/' . $request->blog_id, $name);
            $input['image'] = 'images/blog' . $request->blog_id . "/" . $name;
        }
        $input["created_by"] = $user->id;
        $image = BlogImages::create($input);
        return response()->json(['success' => true, 'user' => $user, "message" => "Image uploaded"], 200);
    }

    // function for removing image from blog
    public function removeBlogImage(Request $request, MediaService $mediaService)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\BlogImages,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $image = BlogImages::find($request->id);
        if ($image->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $mediaService->deleteFile($image->image);
        $image->delete();
        return response()->json(['success' => true, 'message' => "Image deleted"], 200);
    }

    // function for adding social media link
    /**
     * Upis socijalnih medija
     *
     * dodavanje informacija o socijalnim mrežama. Šalje se name i link polja.
     *
     * @group Company
     * */
    public function addSocialMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "link" => "required",
            "name" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $socialMedia = SocialMedia::create($input);
        return response()->json(['success' => true, 'message' => "Social media linked"], 200);
    }

    // function for updating particular social media link
    /**
     * Izmjena socijalnih medija
     *
     * izmjena socijalnih medija
     *
     * @group Company
     * */
    public function updateParticularSocialMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\SocialMedia,id",
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        if ($user->id != $request->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->except(['id', 'user_id']);
        $input["modified_by"] = $user->id;
        $socialMedia = SocialMedia::find($request->id);
        if (!$socialMedia) {
            return response()->json(['success' => false, 'message' => "Not found"], 404);
        }
        if ($user->id != $socialMedia->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $socialMedia->update($input);
        return response()->json(['success' => true, 'message' => "Social media updated", "social_media" => $socialMedia], 200);
    }

    // function for removing social media link
    public function removeSocialMedia(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\SocialMedia,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $socialMedia = SocialMedia::find($request->id);
        if ($socialMedia->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $socialMedia->delete();
        return response()->json(['success' => true, 'message' => "Social media deleted"], 200);
    }

    // function for adding package
    public function addPackage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "description" => "required",
            "package_type" => "required|integer",
            "price" => "required|regex:/^\d*(\.\d{2})?$/",
            "duration" => "required|integer"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $package = Package::create($input);
        return response()->json(['success' => true, 'message' => "Package created"], 200);
    }

    // function for updating particular package
    public function updateParticularSocialPackage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\Package,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        if ($user->id != $request->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->except(['id', 'user_id']);
        $input["modified_by"] = $user->id;
        $package = Package::where('id', $request->id);
        $package->update($input);
        return response()->json(['success' => true, 'message' => "Package updated", "package" => $package->first()], 200);
    }

    // function for removing package
    public function removePackage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\Package,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $package = Package::find($request->id);
        if ($package->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $package->delete();
        return response()->json(['success' => true, 'message' => "Package deleted"], 200);
    }

    // function for adding package
    public function addPackagePurchaseHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "package_id" => "required|integer|exists:App\Models\PackagePurchaseHistory,id",
            "purchase_auto" => "required|boolean",
            "purchase_date" => "required|date",
            "expires_date" => "required|date"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $input = $request->all();
        $input["user_id"] = $user->id;
        $input["created_by"] = $user->id;
        $packagePurchaseHistory = PackagePurchaseHistory::create($input);
        return response()->json(['success' => true, 'message' => "Package purchase created"], 200);
    }

    // function for removing package
    public function removePackagePurchaseHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\PackagePurchaseHistory,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }
        $packagePurchaseHistory = PackagePurchaseHistory::find($request->id);
        if ($packagePurchaseHistory->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $packagePurchaseHistory->delete();
        return response()->json(['success' => true, 'message' => "Package purchase deleted"], 200);
    }

    // function for getting information about particular package
    public function getParticularPackage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required|integer|exists:App\Models\Package,id"
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $package = Package::find($request->id);
        if (!$package) {
            return response()->json(['success' => false, 'message' => "Package not found"], 404);
        }
        if ($package->user_id != $user->id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        return response()->json(['success' => true, 'package' => $package], 200);
    }

    // function for getting published ads
    /**
     * Objavljeni oglasi
     *
     * uzimanje objavljenih oglasa, offset i limit obavezna, dodatni filteri su tip posla i grad
     *
     * @group Company
     */
    public function getPublishedAds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'required|int|min:0',
            'limit' => 'required|int|min:0',
            'type_of_work_id' => 'nullable|integer|exists:App\ModelsTypeOfWork,id',
            'city_id' => 'nullable|integer|exists:App\Models\City,id',
            'term'  => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $type_of_work_id = $request->get("type_of_work_id");
        $city_id = $request->get("city_id");
        $term = $request->get("term");

        $ads = Ad::with(["country", "city"])->leftJoinSub('select id as typeOfWorkId , name as type_of_work from type_of_works', "type_of_works", "type_of_works.typeOfWorkId", "=", "ads.type_of_work_id")->where("is_active", 1)->where("is_archived", 0)->where("user_id", $user->id)->where(function ($query) use ($type_of_work_id, $city_id) {
            if ($city_id) {
                $query->where('city_id', $city_id);
            }

            if ($type_of_work_id) {
                $query->where('type_of_work_id', $type_of_work_id);
            }
        });

        if ($term) {
            $ads = $ads->where(function ($query) use ($term) {
                $query
                    ->where("ads.title", "like", "%" . $term . "%")
                    ->orWhere("ads.location", "like", "%" . $term . "%")
                    ->orWhere("ads.position", "like", "%" . $term . "%");
            });
        }

        $adsCount = $ads->count();

        $ads = $ads->limit($request->get("limit"))
            ->offset($request->get("offset"))
            ->orderBy("created_at", "DESC")
            ->get();

        $status = "Istekao";
        $ads = collect($ads)->map(function ($item) use ($status) {
            if ($item->is_active == 1 && $item->is_archived == 0 && $item->end_date >= date('Y-m-d')) {
                $status = "Aktuelan";
            }
            $item['status'] = $status;
            return $item;
        });

        return response()->json(['success' => true, 'ads' => $ads, 'count' => $adsCount], 200);
    }

    // function for getting arhived ads
    /**
     * Uzimanje arhiviranih oglasa
     *
     * uzimanje arhiviranih oglasa. Obavezna polja su limit i offset
     *
     * @group Company
     */
    public function getArhivedAds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'required|int|min:0',
            'limit' => 'required|int|min:0',
            'type_of_work_id' => 'nullable|integer|exists:TypeOfWork,id',
            'city_id' => 'nullable|integer|exists:City,id',
            'term'  => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $type_of_work_id = $request->get("type_of_work_id");
        $city_id = $request->get("city_id");
        $term = $request->get("term");

        $ads = Ad::with(["country", "city"])->leftJoinSub('select id as typeOfWorkId , name as type_of_work from type_of_works', "type_of_works", "type_of_works.typeOfWorkId", "=", "ads.type_of_work_id")->where("is_archived", 1)->where("user_id", $user->id)->where(function ($query) use ($type_of_work_id, $city_id) {
            if ($city_id) {
                $query->where('city_id', $city_id);
            }

            if ($type_of_work_id) {
                $query->where('type_of_work_id', $type_of_work_id);
            }
        });

        if ($term) {
            $ads = $ads->where(function ($query) use ($term) {
                $query
                    ->where("ads.title", "like", "%" . $term . "%")
                    ->orWhere("ads.location", "like", "%" . $term . "%")
                    ->orWhere("ads.position", "like", "%" . $term . "%");
            });
        }

        $adsCount = $ads->count();

        $ads = $ads->limit($request->get("limit"))
            ->offset($request->get("offset"))
            ->orderBy("created_at", "DESC")
            ->get();

        $status = "Istekao";
        $ads = collect($ads)->map(function ($item) use ($status) {
            if ($item->is_archived == 1) {
                $status = "Arhiviran";
            }
            if ($item->is_active == 0 && $item->is_archived == 0) {
                $status = "Sačuvan";
            }
            if ($item->is_active == 1 && $item->is_archived == 0 && $item->end_date >= date('Y-m-d')) {
                $status = "Aktuelan";
            }
            $item['status'] = $status;
            return $item;
        });

        return response()->json(['success' => true, 'ads' => $ads, 'count' => $adsCount], 200);
    }

    // function for getting saved ads
    /**
     * Sacuvani oglasi
     *
     * Uzimanje sacuvanih oglasa
     *
     * @group Company
     */
    public function getSavedAds(Request $request)
    {

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $validator = Validator::make($request->all(), [
            'offset' => 'required|int|min:0',
            'limit' => 'required|int|min:0',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $type_of_work_id = $request->get("type_of_work_id");
        $city_id = $request->get("city_id");
        $term = $request->get("term");

        $ads = Ad::with(["country", "city", "questions"])->leftJoinSub('select id as typeOfWorkId , name as type_of_work from type_of_works', "type_of_works", "type_of_works.typeOfWorkId", "=", "ads.type_of_work_id")->where("is_active", 0)->where("is_archived", 0)->where("user_id", $user->id)->where(function ($query) use ($type_of_work_id, $city_id) {
            if ($city_id) {
                $query->where('city_id', $city_id);
            }

            if ($type_of_work_id) {
                $query->where('type_of_work_id', $type_of_work_id);
            }
        });

        if ($term) {
            $ads = $ads->where(function ($query) use ($term) {
                $query
                    ->where("ads.title", "like", "%" . $term . "%")
                    ->orWhere("ads.location", "like", "%" . $term . "%")
                    ->orWhere("ads.position", "like", "%" . $term . "%");
            });
        }

        $adsCount = $ads->count();

        $ads = $ads->limit($request->get("limit"))
            ->offset($request->get("offset"))
            ->orderBy("created_at", "DESC")
            ->get();

        $status = "Istekao";
        $ads = collect($ads)->map(function ($item) use ($status) {
            if ($item->is_archived == 1) {
                $status = "Arhiviran";
            }
            if ($item->is_active == 0) {
                if($item->title && $item->country_id && $item->city_id && $item->type_of_work_id && $item->job_type_id && $item->work_time_id) {
                    $status = "Neobjavljen";
                } else {
                    $status = "U pripremi";
                }
            }
            // if ($item->is_active == 1 && $item->is_archived == 0 && $item->end_date >= date('Y-m-d')) {
            //     $status = "Aktuelan";
            // }
            $item['status'] = $status;
            return $item;
        });

        return response()->json(['success' => true, 'ads' => $ads, 'count' => $adsCount], 200);
    }

    // function for getting particular ad
    public function getParticularAd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|int|exists:App\Models\Ad,id'
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $ad = Ad::find($request->id);

        return response()->json(['success' => true, 'ad' => $ad], 200);
    }

    // function for getting dashboard info
    /**
     * Dashboard kompanije
     *
     * UZMINAJE INFORMACIJA o profilu. Šalje se godina kao string da bi se uzele informacije o mjesečnim prijavama.
     *
     * @group Company
     */
    public function getCompanyDashboard(Request $request)
    {
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);

        $validator = Validator::make($request->all(), [
            'year' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        }
        $adsStats = Ad::select(DB::raw('count(id) as `data`'), DB::raw('MONTH(created_at) as month'))->where('user_id', $user->id)->whereYear('created_at', $request->year);

        $adsStats = $adsStats->groupBy('month')->orderBy("created_at", "ASC")->get();

        $activeAdsList = Ad::with(['city', 'country'])->where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0)->leftJoinSub('select id as owner_id , full_name as company, profile_image from users', "users", "users.owner_id", "=", "ads.user_id")->latest('created_at')->limit(5)->get();

        $activeAdsNumber = Ad::where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0)->count();

        $latestApllicationNumber = 0;

        if ($numberOfApplicationsLatest = Ad::where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0)->latest('created_at')->first()) {
            $latestApllicationNumber = $numberOfApplicationsLatest->number_of_applications;
        }

        // $numberOfApplicationsAll = Ad::where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0)->sum('number_of_applications');

        $numberOfApplicationsAll = AdSharedInfo::whereHas('ad', function($query) use ($user) {
            return $query->where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0);
        })->count();



        $numberOfSelectedCandidatesForLatestAd = 0;

        $latestAdId = Ad::where('user_id', $user->id)->where("is_active", 1)->where("is_archived", 0)->select('id')->latest('created_at');

        if ($latestAdId->first()) {
            $lastAd = $latestAdId->first();
            $numberOfSelectedCandidatesForLatestAd = AdSharedInfo::where('ad_id', $lastAd->id)->where('selected', 1)->count();
        }



        $adIds = Ad::where("user_id", $user->id)->pluck('id')->toArray();
        $applyUserIds = AdSharedInfo::whereIn('ad_id', $adIds)->pluck('user_id')->toArray();

        $newCandidates = User::where('role', 'employee')->with(['educations'])->whereIn("id", $applyUserIds)->leftJoinSub('select id as cityId , name as city from cities', "cities", "cities.cityId", "=", "users.city_id")
            ->latest('created_at')
            ->limit(10)
            ->get();

        return response()->json([
            'success' => true,
            'adsStats' => $adsStats,
            'activeAdsList' => $activeAdsList,
            'activeAdsNumber' => $activeAdsNumber,
            'numberOfApplicationsLatest' => $latestApllicationNumber,
            'numberOfApplicationsAll' => $numberOfApplicationsAll,
            'numberOfSelectedCandidatesForLatestAd' => $numberOfSelectedCandidatesForLatestAd,
            'newCandidates' => $newCandidates
        ], 200);
    }

    // function for getting all companies depending on term or not
    public function getCompanies(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'required|int|min:0',
            'limit' => 'required|int|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        if ($request->term) {
            $companies = User::where('is_hidden', 0)
            ->where("full_name", "like", "%" . $request->term . "%")->where("is_active", 1)->where("is_archived", 0)->where("role", "=", "company")->with(['country', 'city'])->withCount('activeAds');
            $companiesCount = $companies->count();
            $companies = $companies->limit($request->get("limit"))
                ->offset($request->get("offset"))
                ->orderBy("created_at", "DESC")
                ->get();
        } else {
            $companies = User::where('is_hidden', 0)->where("role", "=", "company")->where("is_active", 1)->where("is_archived", 0)->with(['country', 'city'])->withCount('activeAds');
            $companiesCount = $companies->count();
            $companies = $companies->limit($request->get("limit"))
                ->offset($request->get("offset"))
                ->orderBy("created_at", "DESC")
                ->get();
        }

        $companies = collect($companies)->map(function ($item) {
            $item['field'] = "company";
            return $item;
        });

        return response()->json([
            'success' => true,
            'companies' => $companies,
            'count' => $companiesCount,
        ], 200);
    }

    // function for setting ad view
    /**
     * Postavka prijave na vidjeno
     *
     * updejtuje se prijava da je viđena korisniku
     *
     * @group Company
     */
    public static function updateSeen($id)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:App\Models\AdSharedInfo,id',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }


        $apply = AdSharedInfo::find($id);

        if (!$apply) {
            return response()->json(['success' => false, 'message' => "Not found"], 404);
        }

        if (!$user->id != $apply->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $apply->update(["viewed" => date('Y-m-d')]);
        return response()->json(['success' => true, 'message' => "Apply updated"], 200);
    }

    // function for setting reminder for apply
    public static function setReminder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'requiredinteger|exists:App\Models\AdSharedInfo,id',
            'text' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }


        $apply = AdSharedInfo::find($request->id);

        if (!$apply) {
            return response()->json(['success' => false, 'message' => "Not found"], 404);
        }

        $ad = Ad::find($apply->ad_id);

        if ($user->id != $ad->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $apply->update(["reminder" => $request->text, "modified_by" => $user->id]);
        return response()->json(['success' => true, 'message' => "Apply updated"], 200);
    }

    // function for getting adds for admin basic and companies
    /**
     * Objavljeni oglasi
     *
     * uzimanje objavljenih oglasa
     *
     * @group Company
     */
    public function getPersonalAds(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'offset' => 'required|int|min:0',
            'limit' => 'required|int|min:0',
            'type_of_work_id'   => 'nullable|integer|exists:App\Models\TypeOfWork,id',
            'city_id'   => 'nullable|integer|exists:App\Models\City,id',
            'is_active'   => 'nullable|boolean',
            'is_archived'   => 'nullable|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $type_of_work_id = $request->get("type_of_work_id");
        $city_id = $request->get("city_id");
        $is_active = $request->get("is_active");
        $is_archived = $request->get("is_archived");



        $ads = Ad::with(["country", "city", "type_of_work"])->with(['creator' => function ($query) {
            $query->select('id', 'full_name', 'profile_image');
        }])->withCount("shared_adds")->where("user_id", $user->id)->where(function ($query) use ($type_of_work_id, $city_id, $is_active, $is_archived, $userOwner) {
            if ($city_id) {
                $query->where('city_id', $city_id);
            }

            if ($type_of_work_id) {
                $query->where('type_of_work_id', $type_of_work_id);
            }

            if ($is_active == 1) {
                $query->where('ads.is_active', "=", 1);
            }

            if ($is_active == 2) {
                $query->where('ads.is_active', "=", 0);
            }

            if ($is_archived) {
                $query->where('is_archived', $is_archived);
            }
        });

        $adsCount = $ads->count();

        $ads = $ads->limit($request->get("limit"))
            ->offset($request->get("offset"))
            ->orderBy("created_at", "DESC")
            ->get();

        return response()->json(['success' => true, 'ads' => $ads, 'count' => $adsCount], 200);
    }

    // get company by public route
    public function getCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|int|exists:App\Models\User,id'
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $company = User::where("id", $request->id)->where("is_active", 1)->where("is_archived", 0)->select('id', 'role', 'full_name', 'profile_image', 'company_description', 'background_image', 'phone', 'address', 'website', 'facebook', 'instagram', 'linkedin', 'pib', 'pdv', 'email', 'zip_code', 'employees_number', 'country_id', 'city_id')->with(['country', 'city', 'videos', 'company_users'])->first();

        if (!$company || $company->role != "company") {
            return response()->json(['success' => false, 'message' => 'Company does not exist or not active'], 404);
        }

        return response()->json(['success' => true, 'company' => $company], 200);
    }

    public function updateAd(Request $request, MediaService $mediaService, S3ClientService $s3ClientService)
    {
        /** @var User $user */
        $user = auth()->user();
        if (!$user) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:App\Models\Ad,id',
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'location' => 'nullable',
            'video' => 'nullable|string|regex:'.$s3ClientService->regex(),
            'document' => 'nullable|string',
            'start_date' => 'nullable',
            'end_date' => 'nullable',
            'is_active' => 'nullable|boolean',
            'is_archived' => 'nullable|boolean',
            'city_id' => 'nullable|integer|exists:App\Models\City,id',
            'country_id' => 'nullable|integer|exists:App\Models\Country,id',
            'type_of_work_id' => 'nullable|integer|exists:App\Models\TypeOfWork,id',
            'position' => 'nullable',
            'job_type_id' => 'nullable|integer|exists:App\Models\JobType,id',
            'work_time_id' => 'nullable|integer|exists:App\Models\WorkTime,id',
            'education_level_id' => 'nullable|integer|exists:App\Models\EducationLevel,id',
            'short_description' => 'nullable|string',
            'visible' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $validatedData  = $validator->valid();

        $ad = Ad::findOrFail($request->id);

        if ($user->id != $ad->user_id) {
            return response()->json(['success' => false, 'message' => "User unauthorized"], 401);
        }

        // document upload
        if ($file = $request->file('document')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('documents/ads/' . $user->id, $name);
            $validatedData['document'] = 'documents/ads/' . $user->id . "/" . $name;
            unlink(public_path() . "/" . $ad->document);
        }

        if ($request->video) {
            $mediaService->updateAdVideo(
                $ad,
                $request->video
            );
        }

        $validatedData['modified_by'] = $user->id;

        $ad->update($validatedData);

        if ($request->ads_questions) {

            $ad_questions = json_decode($request->ads_questions);

            AdQuestion::where("ad_id", $ad->id)->delete();
            foreach ($ad_questions as $question) {
                AdQuestion::create(["ad_id" => $ad->id, "text_question" => $question->text_question, "created_by" => $user->id]);
            }
        }

        return response()->json(['success' => true,"message" => 'Ad changed successfully.', "ad" => $ad, 'request' => $request->all()], 200);

    }
}
