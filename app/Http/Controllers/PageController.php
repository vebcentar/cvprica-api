<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class PageController extends Controller
{
    public function download(Request $request)
    {
        // Fetch the file info.
        $filePath = $request->file;

        if(file_exists($filePath)) {
            $fileName = basename($filePath);
            $fileSize = filesize($filePath);

            if(in_array(pathinfo($fileName, PATHINFO_EXTENSION), ['mp4', 'pdf'])) {
                // Output headers.
                header("Cache-Control: private");
                header("Content-Type: application/stream");
                header("Content-Length: ".$fileSize);
                header("Content-Disposition: attachment; filename=".$fileName);

                // Output file.
                readfile ($filePath);                   
                exit();
            }
            return 'Not supported format.';

        }
        else {
            return 'The provided file path is not valid.';
        }
    }
}
