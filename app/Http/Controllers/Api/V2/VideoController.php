<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Requests\VideoUploadRequest;
use App\Http\Resources\VideoResource;
use App\Models\Video;
use App\Services\MediaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 * @authenticated
 */
class VideoController
{
    /**
     * Upload video
     */
    public function store(VideoUploadRequest $request, MediaService $mediaService): VideoResource
    {
        $video = $mediaService->createVideo(
            $request->user(),
            $request->file('video')
        );

        return new VideoResource($video);
    }

    /**
     * Delete video
     */
    public function delete($id, MediaService $mediaService): JsonResponse
    {
        /** @var Video $video */
        $video = Video::query()->findOrFail($id);

        if ($video->created_by !== Auth::id()) {
            return response()->json([ 'message' => 'Forbidden' ], 403);
        }

        $mediaService->deleteVideo($video);

        return response()->json([ 'message' => 'Deleted successfully' ]);
    }
}
