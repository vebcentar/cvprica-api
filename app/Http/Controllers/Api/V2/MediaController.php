<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Services\S3ClientService;
use Illuminate\Http\JsonResponse;

/**
 * @group V2
 * @authenticated
 */
class MediaController extends Controller
{
    public function videoUploadForm(S3ClientService $s3ClientService): JsonResponse
    {
        return new JsonResponse($s3ClientService->createVideoSignedPost());
    }
}
