<?php

namespace App\Http\Controllers\Api\V2;;

use App\Exceptions\SafeException;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyQuestionCollection;
use App\Http\Resources\CompanyQuestionResource;
use App\Models\CompanyQuestion;
use App\Models\User;
use App\Repositories\CompanyQuestionRepository;
use App\Services\CompanyQuestionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 * @authenticated
 */
class CompanyQuestionController extends Controller
{
    /**
     * Company questions list
     */
    public function index(CompanyQuestionService $companyQuestionService): CompanyQuestionCollection
    {
        return $companyQuestionService->findByCompanyId(Auth::id());
    }

    /**
     * Create company question
     *
     * @bodyParam question string New question value. Example: 'question?'
     */
    public function store(Request $request, CompanyQuestionService $companyQuestionService): JsonResponse
    {
        $request->validate([
            'question' => 'required|string|max:300',
        ]);

        /** @var User $user */
        $user = Auth::user();

        try {
            $question = $companyQuestionService->create(
                $request->question,
                $user
            );
        } catch (SafeException $ex) {
            return response()->json(['message' => $ex->getMessage()], 400);
        }

        return response()->json((new CompanyQuestionResource($question->load('company'))), 201);
    }

    /**
     * Display the company question.
     *
     * @urlParam id integer Id of the question to show. Example: 1
     */
    public function show($questionId): CompanyQuestionResource
    {
        $companyQuestion = CompanyQuestion::findOrFail($questionId);

        if (Auth::id() !== $companyQuestion->created_by) {
            abort(403, 'Forbidden.');
        }

        return (new CompanyQuestionResource($companyQuestion));
    }

    /**
     * Update the question.
     *
     * @urlParam id integer Id of the question to update. Example: 1
     * @bodyParam question string New question value. Example: 'question?'
     */
    public function update(Request $request, $questionId): CompanyQuestionResource
    {
        $request->validate([
            'question' => ['string|max:300'],
        ]);

        /** @var CompanyQuestion $companyQuestion */
        $companyQuestion = CompanyQuestion::findOrFail($questionId);

        if (Auth::id() !== $companyQuestion->created_by) {
            abort(403, 'Forbidden.');
        }

        $companyQuestion->updateQuestion($request->question);
        $companyQuestion->save();

        return (new CompanyQuestionResource($companyQuestion));
    }

    /**
     * Delete company questions(s)
     *
     * @bodyParam ids.* integer Array of ids to delete
     */
    public function delete(Request $request, CompanyQuestionRepository $companyQuestionRepository): JsonResponse
    {
        $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\CompanyQuestion,id'],
        ]);

        $companyQuestionRepository->delete(Auth::id(), $request->ids);

        return response()->json([]);
    }
}
