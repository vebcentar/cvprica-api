<?php

namespace App\Http\Controllers\Api\V2\Candidate;

use App\Exceptions\SafeException;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityNotificationCollection;
use App\Http\Resources\CompanyNotificationCollection;
use App\Http\Resources\CompanyNotificationResource;
use App\Http\Resources\TypeOfWorkNotificationCollection;
use App\Models\User;
use App\Services\NotificationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 * @authenticated
 */
class NotificationController extends Controller
{
    /**
     * List of all the cities, types of work and companies the user wants to receive the notifications for
     */
    public function allSubscriptions(NotificationService $notificationService): array
    {
        return $notificationService->allNotificationsForUser(Auth::user());
    }

    /**
     * List of cities the candidate wants to get notifications for
     */
    public function citySubscriptions(NotificationService $notificationService): CityNotificationCollection
    {
        return $notificationService->getCityNotificationsForUser(Auth::user());
    }

    /**
     * List of types of work the candidate wants to get notifications for
     */
    public function typeOfWorkSubscriptions(NotificationService $notificationService): TypeOfWorkNotificationCollection
    {
        return $notificationService->getTypeOfWorkNotificationsForUser(Auth::user());
    }

    /**
     * List of companies the candidate wants to get notifications for
     */
    public function companySubscriptions(NotificationService $notificationService): CompanyNotificationCollection
    {
        return $notificationService->getCompanyNotificationsForUser(Auth::user());
    }

    /**
     * Subscribe to company to get notifications for
     * @urlParam company_id integer Id of a company user wants to subscribe to
     */
    public function subscribeToCompany($companyId, NotificationService $notificationService)
    {
        /** @var User $company */
        if (!$company = User::findOrFail($companyId)) {
            return response()->json([ 'message' => 'Not found.'], 404);
        }

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Not a company.'], 400);
        }

        try {
            $subscription = $notificationService->subscribeUserToCompany(Auth::user(), $company);

            return new CompanyNotificationResource($subscription);
        } catch (SafeException $ex) {
            return response()->json([ 'message' => $ex->getMessage() ], 400);
        }
    }

    /**
     * Unsubscribe from company to stop getting notifications for
     * @urlParam company_id integer Id of a company user wants to unsubscribe from
     */
    public function unsubscribeFromCompany($companyId, NotificationService $notificationService): JsonResponse
    {
        /** @var User $company */
        if (!$company = User::findOrFail($companyId)) {
            return response()->json([ 'message' => 'Not found.'], 404);
        }

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Not a company.'], 400);
        }

        try {
            $notificationService->unsubscribeUserFromCompany(Auth::user(), $company);

            return response()->json([ 'message' => 'Unsubscribed successfully' ]);
        } catch (SafeException $ex) {
            return response()->json([ 'message' => $ex->getMessage() ], 400);
        }
    }
}
