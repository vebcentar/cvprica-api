<?php

namespace App\Http\Controllers\Api\V2\Candidate;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Favorite;
// Resources
use App\Http\Resources\AdResource;

/**
 * @group V2
 * @authenticated
 */
class AdController extends Controller
{
    /**
     * Candidate - Ad set favorite
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     *
     * @authenticated
     * @return \Illuminate\Http\Response
     */
    public function setFavorite(Request $request, $id)
    {
        //@todo Refactor
        $validated = $request->validate([
            'favorite'    =>  ['nullable', 'boolean']
        ]);

        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $ad = Ad::findOrFail($id);

        if(isset($validated['favorite']) && $validated['favorite'] == false) {
            if($favorite = Favorite::where("ad_id", $ad->id)->where("user_id", $user->id)->first())
                $favorite->delete();

            return (new AdResource($ad))
                ->additional([
                    'message' => 'Ad removed from favorites.',
                    'request_all' => $request->all(),
                ])
                ->response()
                ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
        } else {
            Favorite::firstOrCreate(["ad_id" => $ad->id, "user_id" => $user->id]);
            return (new AdResource($ad))
                ->additional([
                    'message' => 'Ad added to favorites.',
                    'request_all' => $request->all(),
                    'request_validated' => $validated,
                ])
                ->response()
                ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
        }

    }

}
