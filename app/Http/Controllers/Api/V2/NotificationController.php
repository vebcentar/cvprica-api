<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Models
use App\Models\User;
use App\Models\Notification;
// Resources
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\NotificationResource;

/**
 * @group V2
 * @authenticated
 */

class NotificationController extends Controller
{
    /**
     * Notifications
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam seen boolean Seen (true|false).
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validated = $request->validate([
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'seen'    =>  ['nullable', 'boolean'],
            'limit'    =>  ['nullable', 'integer'],
            'offset'    =>  ['nullable', 'integer'],
        ]);

        $user = $request->user();
        if($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $notifications = $user->notifications()
            ->when(isset($request->seen), function($query) use ($request) {
                return $query->where("seen", $request->seen);
            });
        $notifications_count = $notifications->count();
        $notifications = $notifications->offset($request->offset ?? 0)
            ->limit($request->limit ?? 20)
            ->get();

        return (new NotificationCollection($notifications))->additional(
            ['meta' => [
                    'total' => $notifications_count,
                ]
            ]
            );
    }


    /**
     * Notification set seen
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     *
     * @authenticated
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setSeen(Request $request, $id)
    {
        $validated = $request->validate([
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'seen'    =>  ['nullable', 'boolean'],
        ]);

        $user = $request->user();
        if($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $notification = $user->notifications()->findOrFail($id);

        $notification->update([
            'seen'  =>  $validated['seen'] ?? 1
        ]);

        return (new NotificationResource($notification))
            ->additional(['message' => 'Notification successfully updated.'])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
