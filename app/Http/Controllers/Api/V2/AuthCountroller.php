<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\TokenLoginRequest;
use App\Services\EmailVerificationService;
use App\Services\UserService;
use App\Services\NotificationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

/**
 * @group V2
 */
class AuthCountroller extends Controller
{
    /**
     * Login
     *
     * @param  \App\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request) {
        $login_credentials=[
            'email'=>$request->email,
            'password'=>$request->password,
        ];
        if(auth()->attempt($login_credentials)){
            //generate the token for the user
            $user_login_token= auth()->user()->createToken('LaravelAuthApp')->accessToken;
            //now return this token on success login attempt
            return response()->json([
                'token' => $user_login_token,
                'role' => auth()->user()->role,
            ], 200);
        }
        else{
            //wrong login credentials, return, user not authorised to our system, return error code 401
            return response()->json(['message' => 'Unauthorised'], 401);
        }
    }

    /**
     * Register
     *
     * @bodyParam password_confirmation string required Must be identical as password
     * @param  \App\Http\Requests\RegisterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request, NotificationService $service, EmailVerificationService $emailVerificationService) {

        $language_id = 1;

        if($request->language_id){
            $language_id = $request->language_id;
        }

        /** @var User $user */
        $user = User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'language_id' => $language_id,
            'role' => $request->role,
            'is_active' => 0,
            'password' => $request->password
        ]);

        $user->refresh();

        $token = $user->createToken('LaravelAuthApp')->accessToken;

        $emailVerificationService->sendVerificationEmailForUser($user);

        $service->sendNotificationForNewUser($user);

        return response()->json([
                'message' => 'You have successfully registered.',
                'token' => $token,
                'role' => $user->role,
            ], \Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Reset password
     *
     * @bodyParam email string Must be a valid email address.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:App\Models\User,email',
        ]);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
                    ? response()->json([
                        "message" => 'Mail to restart password has been sent.'
                    ])
                    : response()->json([
                        "message" => 'Mail to restart the password could not be sent.'
                    ], \Illuminate\Http\Response::HTTP_ACCEPTED);

    }

    /**
     * @bodyParam old_password string User's old password
     * @bodyParam new_password string User's new password
     */
    public function changePassword(Request $request): JsonResponse
    {
        $request->validate([
            'old_password' => 'required|string',
            'new_password' => 'required|string|min:8',
        ]);

        /** @var User $user */
        $user = Auth::user();

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json([ 'message' => 'Old password is not correct.' ], 400);
        }

        $user->password = $request->new_password;

        $user->save();

        return response()->json([]);
    }

    /**
     * Logout
     * @authenticated
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        if (\Auth::check()) {
            $token = \Auth::user()->token();
            $token->revoke();
        }
        return response()->json(["message" => "User logout."], \Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Exchange google idToken for CVP token; Create user if not found;
     *
     * @bodyParam token string Google Id Token
     * @bodyParam role string (employee|company)
     */
    public function googleVerifyToken(TokenLoginRequest $request, UserService $userService): JsonResponse
    {
        $client = new \Google_Client([
            'client_id' => config('app.google.oauth_client_id'),
            'client_secret' => config('app.google.oauth_client_secret')
        ]);

        try {
            $payload = $client->verifyIdToken($request->get('token'));
            if (false === $payload) {
                return new JsonResponse([ 'message' => 'Unable to verify idToken'], 400);
            }
            $user = $userService->createFromGoogleIdToken($payload, $request->get('role'));
            return new JsonResponse([
                'token' => $userService->generateAccessTokenForUser($user),
                'role' => $user->role
            ]);

        } catch (\Exception $ex) {
            return new JsonResponse([ 'message' => 'Error verifying idToken' ], 400);
        }
    }
}
