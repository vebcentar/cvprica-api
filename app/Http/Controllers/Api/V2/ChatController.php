<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Services\ChatService;
use Illuminate\Http\Request;
// Models
use App\Models\User;
use App\Models\Message;
// Resources
use App\Http\Resources\MessageCollection;
use App\Http\Resources\MessageResource;

/**
 * @group V2
 * @authenticated
 */

class ChatController extends Controller
{

    /**
     * Chat - Conversations
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam term string Filter by user full name. Example: 'Nikola'
     *
     * @return MessageCollection
     */
    public function conversations(Request $request, ChatService $chatService): MessageCollection
    {
        $request->validate([
            'user_id' =>  [ 'integer', 'exists:App\Models\User,id' ],
            'limit' =>  [ 'integer', 'min:5', 'max:100' ],
            'offset' =>  [ 'integer', 'min:0' ],
            'term' => [ 'string', 'max:100' ],
        ]);

        $user = $request->user();
        if ($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return $chatService->conversationsForUser(
            $user->id,
            $request->limit ?? 20,
            $request->offset ?? 0,
            $request->term ?? null
        );
    }


    /**
     * Chat - Conversation
     *
     * @urlParam id integer The user ID with which the conversation is. Example: 1
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam limit integer Limits number of returned results. Example: 20
     * @urlParam from_id integer id of message to return earlier from. Example: 2
     *
     * @return MessageCollection
     */
    public function conversation(Request $request, $id, ChatService $chatService): MessageCollection
    {
        $request->validate([
            'user_id' => [ 'integer', 'exists:App\Models\User,id'],
            'from_id' => [ 'integer', 'exists:App\Models\Message,id'],
            'limit' => [ 'integer', 'max:100' ],
        ]);

        $user = $request->user();
        if ($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $secondUser = User::findOrFail($id);

        return $chatService->conversationBetweenTwoUsers(
            $user->id,
            $secondUser->id,
            $request->limit ?? 20,
            $request->from_id ?? null
        );
    }

    /**
     * Chat - Conversation set seen
     *
     * @urlParam id integer The user ID with which the conversation is. Example: 1
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @return \Illuminate\Http\Response
     */
    public function conversationSetSeen(Request $request, $id)
    {
        $validated = $request->validate([
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'limit'    =>  ['nullable', 'integer'],
            'offset'    =>  ['nullable', 'integer'],
        ]);

        $user = $request->user();
        if($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $twoUser = User::findOrFail($id);

        $messages = Message::whereIn('user_id', [$user->id, $twoUser->id])->whereIn('created_by', [$user->id, $twoUser->id]);

        $messages->update([
            'seen'  =>  true,
        ]);

        $messages_count = $messages->count();
        $messages = $messages->offset($request->offset ?? 0)
            ->limit($request->limit ?? 20)
            ->get();

        return (new MessageCollection($messages))->additional(
            ['meta' => [
                    'total' => $messages_count,
                ]
            ]
            );
    }

    /**
     * Chat - Send message
     *
     * @urlParam id integer The user ID to whom to send a message. Example: 1
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request, $id)
    {
        $validated = $request->validate([
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'title' => ['nullable', 'string', 'max:255'],
            'text' => ['required', 'string', 'max:1000'],
        ]);

        $user = $request->user();
        if ($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        if ((int) $id === $user->id) {
            return response()->json([ 'message' => 'Cannot send message to yourself.'], 403);
        }

        $twoUser = User::findOrFail($id);

        $message = $twoUser->messages()->create([
            'title' => $validated['title'] ?? '',
            'text' => $validated['text'],
            'created_by' => $user->id
        ]);

        return (new MessageResource($message))
            ->additional(['message' => 'The message was sent successfully.'])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_CREATED);
    }




    /**
     * Chat - Send messages
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @return \Illuminate\Http\Response
     */
    public function sendMessages(Request $request)
    {
        $validated = $request->validate([
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'title' => ['nullable', 'string', 'max:255'],
            'text' => ['required', 'string', 'max:1000'],
            'to_user_ids'    =>  ['required', 'array'],
            'to_user_ids.*'  =>  ['required', 'integer', 'exists:App\Models\User,id'],
        ]);
        $user = $request->user();
        if($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $messages = collect();

        foreach ($validated['to_user_ids'] as $id) {
            $twoUser = User::findOrFail($id);

            $message = $twoUser->messages()->create([
                'title' => $validated['title'] ?? '',
                'text' => $validated['text'],
                'created_by' => $user->id
            ]);

            $messages->push($message);
        }

        return (new MessageCollection($messages))
            ->additional(['message' => 'Messages sent successfully.'])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_CREATED);
    }
}
