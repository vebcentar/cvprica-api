<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Exceptions\SafeException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdListRequest;
use App\Http\Requests\Admin\CreateAdRequest;
use App\Models\User;
use App\Services\AdService;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Http\Resources\Admin\AdResource;
use App\Http\Resources\Admin\AdCollection;

/**
 * @group V2
 * @authenticated
 */
class AdController extends Controller
{
    /**
     * Admin - Ads
     *
     * @urlParam user_id integer User id. Example: 1
     * @urlParam is_active boolean Filter by users is activate (true|false). Example: true
     * @urlParam is_archived boolean Filter by users is archived (true|false). Example: true
     * @urlParam type_of_work_id integer Filter by type of work. Example: 1
     * @urlParam city_id integer Filter by city. Example: 1
     * @urlParam term string Filter by ad title, description or company name. Example: 'react'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam status integer Filter by ad status (from 0 to 6). Example: 1
     * @urlParam order_by string (id, title, created_at).
     * @urlParam order_dir string (asc, desc).
     *
     */
    public function index(AdListRequest $request, AdService $adService): AdCollection
    {
        return $adService->adminAdSearch($request);
    }

    /**
     * Admin - create ad
     *
     * @bodyParam title string required Title of the ad. Maximum 190 characters
     * @bodyParam country_id integer Country id for the ad. Example: 1
     * @bodyParam city_id integer City id for the ad. Example: 22
     * @bodyParam type_of_work_id integer Type of work id for the ad. Example: 2
     * @bodyParam description string Description for teh ad. Example: 'Opis'
     * @bodyParam start_date string Start date of the ad. Example: 2021-01-01
     * @bodyParam end_date string Date when ad expires. Example: 2021-01-01
     * @bodyParam ads_questions string Stringified json of an array of string questions. Example: "[\"question 1?\",\"question 2?\",\"question 3?\"]"
     * @bodyParam short_description string Short description of an ad.
     * @bodyParam is_active boolean Active status for the ad. Can be set to active only if all the necessary fields are filled. Default: 0 Example: 1
     * @bodyParam job_type_id integer Job type id of the ad. Example: 1
     * @bodyParam work_time_id integer Work time id of the ad. Example: 1
     * @bodyParam education_level_id integer Education level id asked of candidates. Example: 6
     * @bodyParam user_id integer User id of creator of ad. Example: 2
     */
    public function store(CreateAdRequest $request, AdService $adService)
    {
        /** @var User $user */
        $user = User::query()->findOrFail($request->user_id);

        if (!$user->isCompany()) {
            abort(403, 'Forbidden.');
        }

        try {
            $ad = $adService->create($user, $request);

            return new \App\Http\Resources\AdResource($ad);
        } catch (SafeException $ex) {
            return response()->json([ 'message' => $ex->getMessage()], 400);
        }
    }

    /**
     * Admin - Ads show
     *
     * @param  int  $id
     * @return \App\Http\Resources\Admin\AdResource
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);
        return new AdResource($ad);
    }

    /**
     * Admin - Ads update
     *
     * @param  App\Http\Requests\Admin  $request
     * @param  int  $id
     * @return \App\Http\Resources\Admin\AdResource
     */
    public function update(AdResource $request, $id)
    {
        $ad = Ad::findOrFail($id);
        $ad->update($request->validated());

        return (new AdResource($ad))
            ->additional(['message' => 'Ad successfully updated.'])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Admin - Ad set archived
     *
     * @return \App\Http\Resources\Admin\AdResource
     */
    public function setArchived(Request $request, $id)
    {
        $request->validate([
            'is_archived'    =>  ['nullable', 'boolean']
        ]);
        /** @var Ad $ad */
        $ad = Ad::findOrFail($id);
        $ad->setArchived((bool) $request->is_archived);
        return (new AdResource($ad))
            ->additional([
                'message' => 'The ad successfully updated.',
                'request_all'    =>  $request->all(),
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - Ads set artived multiple
     *
     * @return \App\Http\Resources\Admin\AdCollection
     */

    public function setArchivedMultiple(Request $request)
    {
        $validated = $request->validate([
            'is_archived'    =>  ['nullable', 'boolean'],
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);


        $ads = Ad::whereIn('id', $request->ids)->get();
        foreach($ads as $ad)
        {
            $ad->setArchived($request->is_archived);
        }
        return (new AdCollection($ads))
            ->additional([
                'message' => 'The ads successfully updated.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - Ad reset
     *
     * @return \App\Http\Resources\Admin\AdResource
     */
    public function reset(Request $request, $id)
    {
        $ad = Ad::findOrFail($id);
        $ad->reset();
        return (new AdResource($ad))
            ->additional([
                'message' => 'The ad successfully reseted.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Admin - Ad reset multiple
     *
     * @return \App\Http\Resources\Admin\AdCollection
     */

    public function resetMultiple(Request $request)
    {
        $validated = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $ads = Ad::whereIn('id', $request->ids)->get();
        foreach($ads as $ad)
        {
            $ad->reset();
        }
        return (new AdCollection($ads))
            ->additional([
                'message' => 'The ads successfully reseted.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Admin - Ad set hidden
     *
     * @return \App\Http\Resources\Admin\UserCollection
     */
    public function setHidden(Request $request, $id)
    {
        $validated = $request->validate([
            'is_hidden'    =>  ['nullable', 'boolean']
        ]);
        $ad = Ad::findOrFail($id);
        $ad->setHidden($request->is_hidden);
        return (new AdResource($ad))
            ->additional([
                'message' => 'The ads successfully updated.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }



    /**
     * Admin - Ad delete
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->delete();

        return response()
            ->json(['message' => 'Ad successfully removed.'])
            ->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }


    /**
     * Admin - Ad delete multiple
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMultiple(Request $request)
    {
        $validated = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $ads = Ad::whereIn('id', $request->ids)->get();
        foreach($ads as $ad)
        {
            $ad->delete();
        }

        return response()
            ->json(['message' => 'Ads successfully removed.'])
            ->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }
}
