<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\UserResource;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Resources\Admin\UserResource as AdminUserResource;
use App\Http\Resources\Admin\UserCollection;

/**
 * @group V2
 * @authenticated
 */
class UserController extends Controller
{
    /**
     * Admin - Users
     *
     * @urlParam role string Filter by users role (admin|company|employee).
     * @urlParam is_active boolean Filter by users is activate (1|0). Example: 1
     * @urlParam is_archived boolean Filter by users is archived (1|0). Example: 1
     * @urlParam term string Search users by term parameters. Example: Web
     * @urlParam birth_year_min integer Minimum birth year. Example: 1990
     * @urlParam birth_year_max integer Maximum birth year. Example: 1990
     * @urlParam country_id integer Country id . Example: 1
     * @urlParam city_id integer City id . Example: 1
     * @urlParam education_level_id integer Education level id. Example: 1
     * @urlParam gender_id integer Gender id. Example: 1
     * @urlParam eminent boolean Eminent (1|0). Example: 1
     * @urlParam work_experience boolean Work experience (1|0). Example: 1
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam order_by string (id, full_name, created_at, email).
     * @urlParam order_dir string (asc, desc).
     *
     * @return UserCollection
     */
    public function index(UserRequest $request): UserCollection
    {
        //@todo Refactor into repository
        $qb = User::query();

        $qb->when($request->role, function($query) use ($request) {
            return $query->where("role", $request->role);
        })
        ->when(isset($request->is_active), function($query) use ($request) {
            return $query->where("is_active", $request->is_active);
        })
        ->when(isset($request->is_archived), function($query) use ($request) {
            return $query->where("is_archived", $request->is_archived);
        })
        ->when($request->term, function($query) use ($request) {
            return $query->where("full_name", "LIKE", "%{$request->term}%");
        })
        ->when($request->birth_year_min, function($query) use ($request) {
            return $query->whereYear("birth_year", ">=", $request->birth_year_min);
        })
        ->when($request->birth_year_max, function($query) use ($request) {
            return $query->whereYear("birth_year", "<=", $request->birth_year_max);
        })
        ->when($request->country_id, function($query) use ($request) {
            return $query->where("country_id", $request->country_id);
        })
        ->when($request->city_id, function($query) use ($request) {
            return $query->where("city_id", $request->city_id);
        })
        ->when($request->education_level_id, function($query) use ($request) {
            return $query->where("education_level_id", $request->education_level_id);
        })
        ->when($request->gender_id, function($query) use ($request) {
            return $query->where("gender_id", $request->gender_id);
        })
        ->when(isset($request->eminent), function($query) use ($request) {
            return $query->where("eminent", $request->eminent);
        })
        ->when(isset($request->work_experience), function($query) use ($request) {
            if($request->work_experience)
                return $query->has('work_experiences');
            else
                return $query->doesntHave('work_experiences');
        });

        if ($request->has('order_by')) {
            $direction = $request->has('order_dir') ? $request->order_dir : 'asc';
            $qb->orderBy($request->order_by, $direction);
        }

        $users_count = $qb->count();
        $users = $qb->offset($request->offset ?? 0)
        ->limit($request->limit ?? 20)
        ->get();

        return (new UserCollection($users))->additional(['meta' => [ 'total' => $users_count ]]);
    }

    /**
     * Admin - Users store (create)
     *
     * @bodyParam password_confirmation string required Must be identical as password
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'full_name'    =>  ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'confirmed'],
            'country_id' => ['nullable', 'integer', 'exists:App\Models\Country,id'],
            'language_id' => ['nullable', 'integer', 'exists:App\Models\UserLanguage,id'],
            'role' => ['required', 'string', 'in:employee,company,admin'],
        ]);

        $language_id = 1;

        if($request->language_id){
            $language_id = $request->language_id;
        }

        $user = User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'language_id' => $language_id,
            'role' => $request->role,
            'is_active' => 0,
            'password' => $request->password
        ]);


        return (new AdminUserResource($user))
            ->additional([
                'message' => 'The user successfully created.',
                'request_all'    =>  $request->all(),
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $id)
    {
        //
    }

    /**
     * Admin - Update user
     *
     * @param  UserRequest  $request
     * @param  int  $id
     * @return AdminUserResource
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->validated());

        return new UserResource($user);
    }


    /**
     * Admin - User set archived
     *
     * @return AdminUserResource
     */
    public function setArchived(Request $request, $id)
    {
        $request->validate([
            'is_archived'    =>  ['nullable', 'boolean']
        ]);
        $user = User::findOrFail($id);
        $user->setArchived($request->is_archived);
        return (new AdminUserResource($user))->additional([
                'message' => 'The user successfully updated.',
                'request_all'    =>  $request->all(),
            ]);
    }


    /**
     * Admin - User set artived multiple
     *
     * @return UserCollection
     */

    public function setArchivedMultiple(Request $request)
    {
        $validated = $request->validate([
            'is_archived'    =>  ['nullable', 'boolean'],
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\User,id'],
        ]);


        $users = User::whereIn('id', $request->ids)->get();
        foreach($users as $user)
        {
            $user->setArchived($request->is_archived);
        }
        return (new UserCollection($users))
            ->additional([
                'message' => 'The users successfully updated.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - User reset
     *
     * @return UserCollection
     */
    public function reset(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->reset();
        return (new AdminUserResource($user))
            ->additional([
                'message' => 'The user successfully reseted.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - User set hidden
     *
     * @return UserResource
     */
    public function setHidden(Request $request, $id)
    {
        $request->validate([
            'is_hidden'    =>  ['nullable', 'boolean']
        ]);
        /** @var User $user */
        $user = User::findOrFail($id);
        $user->is_hidden = (bool) $request->is_hidden;
        $user->save();
        $user->refresh();

        return (new AdminUserResource($user))->additional([ 'message' => 'The user successfully updated.']);
    }


    /**
     * Admin - User reset multiple
     *
     * @return UserCollection
     */

    public function resetMultiple(Request $request)
    {
        $validated = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\User,id'],
        ]);

        $users = User::whereIn('id', $request->ids)->get();
        foreach($users as $user)
        {
            $user->reset();
        }
        return (new UserCollection($users))
            ->additional([
                'message' => 'The users successfully reseted.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - User delete
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()
            ->json(['message' => 'User successfully removed.'])
            ->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }


    /**
     * Admin - User delete multiple
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMultiple(Request $request)
    {
        $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\User,id'],
        ]);

        User::whereIn('id', $request->ids)->delete();

        return response()
            ->json(['message' => 'Users successfully removed.'])
            ->setStatusCode(\Illuminate\Http\Response::HTTP_OK);
    }
}
