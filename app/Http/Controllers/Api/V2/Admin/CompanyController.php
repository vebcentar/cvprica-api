<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Company\InfoResource;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @group V2
 * @authenticated
 */
class CompanyController extends Controller
{
    /**
     * Set eminent status for company
     *
     * @urlParam company_id integer Primary key for company id. Example 1
     * @bodyParam is_eminent boolean Eminent status (0|1). Example 1
     */
    public function setEminent($companyId, Request $request)
    {
        $request->validate([
            'is_eminent'    =>  [ 'nullable', 'boolean' ],
        ]);
        /** @var User $company */
        $company = User::findOrFail($companyId);

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Not a company' ], 400);
        }

        $company
            ->setEminent((bool) $request->is_eminent)
            ->save();

        $company->refresh();

        return new InfoResource($company);
    }
}
