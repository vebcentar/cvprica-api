<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\UserResource as AdminUserResource;
use App\Models\User;
use App\Services\MediaService;
use Illuminate\Http\Request;
// Requests
use App\Http\Requests\Admin\DashboardRequest;
use App\Http\Requests\Admin\UpdateProfileImageRequest;
// Resources
use App\Http\Resources\Admin\DashboardResource;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 * @authenticated
 */

class AdminController extends Controller
{
    /**
     * Returns user resource for self; If user doesn't have admin role the 403 is returned
     */
    public function me(): AdminUserResource
    {
        return new AdminUserResource(Auth::user());
    }

    /**
     * Admin - Dashboard
     * @authenticated
     * @param  \App\Http\Requests\Admin\DashboardRequest  $request
     * @return \App\Http\Resources\Admin\DashboardResource
     */
    public function dashboard(DashboardRequest $request)
    {
        $user = $request->user();
        return new DashboardResource($user);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Admin - Update
     *
     * @authenticated
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
            'full_name'    =>  ['nullable', 'string'],
            'email'    =>  ['nullable', 'string', 'email'],
            'password'    =>  ['nullable', 'string'],
            'language_id'    =>  ['nullable', 'integer', 'exists:App\Models\UserLanguage,id'],
        ]);

        $user = $request->user();
        $user->update($validated);

        return (new UserResource($user))
            ->additional([
                'message' => 'The admin successfully updated.'
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }


    /**
     * Admin - Update profile image
     * @authenticated
     */
    public function fieldsProfileImageUpdate(UpdateProfileImageRequest $request, MediaService $mediaService): UserResource
    {
        /** @var User $user */
        $user = $request->user();

        $mediaService->updateUserProfileImage($user, $request->file('image'));

        return (new UserResource($user))
            ->additional(['message' => 'The company "profile image" successfully updated.']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
