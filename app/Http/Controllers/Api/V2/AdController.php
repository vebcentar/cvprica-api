<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\JobSearchRequest;
use App\Http\Resources\PublicAdCollection;
use App\Models\User;
use App\Services\AdService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// Models
use App\Models\Ad;
// Requests
use App\Http\Requests\AdRequest;
// Resources
use App\Http\Resources\AdResource;
use App\Http\Resources\AdCollection;

/**
 * @group V2
 */


class AdController extends Controller
{
    /**
     * Ads
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam type_of_work_id integer The id of the Type of work. Example: 1
     * @urlParam location string Location.
     * @urlParam city_id string  The id of the City. Example: 1
     * @urlParam country_id integer The id of the country. Example: 1
     * @urlParam term string Term.
     * @urlParam favorite string Favorite (1|0) (Parameter for authorized users).
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam status integer 1 or 6
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //@todo Refactor all, move to service/repository; Validate input
        if (Auth::check()) {
            $user = $request->user();
            if($request->user()->role === 'admin' && $request->user_id) {
                $user = User::findOrFail($request->user_id);
            }
        }

        $qb = Ad::query();

        $qb
            ->where('is_active', true)
            ->where('is_archived', false)
            ->where('end_date', '>=', date('Y-m-d'))
            ->when($request->type_of_work_id, function($query) use ($request) {
                return $query->where("type_of_work_id", $request->type_of_work_id);
            })
            ->when(isset($request->location), function($query) use ($request) {
                return $query->where("location", 'like', '%{$request->location}%');
            })
            ->when(isset($request->city_id), function($query) use ($request) {
                return $query->where("city_id", $request->city_id);
            })
            ->when(isset($request->search), function($query) use ($request) {
                return $query->search($request->search);
            })
            ->when(isset($request->country_id), function($query) use ($request) {
                return $query->where('country_id', $request->country_id);
            })
        ;

        if ($request->has('status')) {
            $status = (int) $request->status;
            if (Ad::STATUS_ACTIVE === $status) {
                $qb
                    ->where('is_active', 1)
                    ->where('is_archived', 0)
                    ->where('visible', 1)
                    ->where('end_date', '>=', date('Y-m-d H:i:s'))
                    ->where('start_date', '<=', date('Y-m-d H:i:s'))
                ;
            } else if (Ad::STATUS_EXPIRED === $status) {
                $qb
                    ->where('is_active', 1)
                    ->where('is_archived', 0)
                    ->where('end_date', '<', date('Y-m-d'))
                ;
            }
        }

        if (Auth::check()) {
            $qb->when(isset($request->favorite), function($query) use ($user, $request) {
                if($request->favorite == 1) {
                    return $query->whereHas('favorites', function($query) use ($user) {
                         $query->where('user_id', $user->id);
                    });
                } else {
                    return $query->whereDoesntHave('favorites', function($query) use ($user) {
                         $query->where('user_id', $user->id);
                    });
                }
            });
        }

        $ads_count = $qb->count();

        $ads = $qb->offset($request->offset ?? 0)
        ->limit($request->limit ?? 20)
        ->orderByDesc('created_at')
        ->get();

        return (new AdCollection($ads))->additional(
            ['meta' => [
                    'total' => $ads_count,
                ]
            ]
            );
    }

    /**
     * Ad show
     *
     * @param  \App\Http\Requests\Candidate\ComputerSkillRequest  $request
     * @return \App\Http\Resources\Candidate\ComputerSkillResource
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);
        return new AdResource($ad);
    }

    /**
     * Public ad list by a single company
     *
     * @urlParam id integer Id of a company. Example: 1
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     */
    public function publicCompanyJobs(Request $request, $companyId, AdService $adService)
    {
        /** @var User $company */
        $company = User::findOrFail($companyId);

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Company not found.' ], 404);
        }

        $request->validate([
            'limit' => [ 'numeric', 'max:100', 'min:5' ],
            'offset' => [ 'numeric', 'min:0' ],
        ]);

        return $adService->publicCompanyAds(
            $company->id,
            (int) $request->offset,
            (int) $request->limit
        );
    }

    /**
     * Public ad
     *
     * @urlParam companyId integer Id of a company. Example: 1
     * @urlParam adId integer Id of a ad. Example: 1
     */
    public function publicCompanyJobShow($companyId, $adId, AdService $adService)
    {
        /** @var User $company */
        $company = User::findOrFail($companyId);

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Company not found.' ], 404);
        }

        if (!$ad = $adService->publicCompanyAd((int) $adId)) {
            return response()->json([ 'message' => 'Ad not found.' ], 404);
        }

        return $ad;
    }

    /**
     * Job search
     *
     * @urlParam created_before integer Filter by when was ad created in days (0 for today). Example: 10
     * @urlParam companies array Filter by companies. Example: [ 1, 2, 3 ]
     * @urlParam work_time_id integer Filter by work time. Example: 1
     * @urlParam education_level_id integer Filter by education level. Example: 6
     * @urlParam country_id integer Filter by ad country id. Example: 1
     * @urlParam city_id integer Filter by ad city id. Example: 22
     * @urlParam job_type_id integer Filter by ad job type id. Example: 1
     * @urlParam types_of_work array Filter by ad types of work. Example: [ 1, 2, 3 ]
     * @urlParam term string Filter by ad title or description. Example: 'react'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam status integer status 1 or 6
     */
    public function jobSearch(JobSearchRequest $request, AdService $adService): PublicAdCollection
    {
        return $adService->jobSearch($request);
    }
}
