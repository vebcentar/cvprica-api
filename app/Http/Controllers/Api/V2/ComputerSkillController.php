<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Candidate\ComputerSkillCollection;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
// Requests
use App\Http\Requests\Candidate\ComputerSkillRequest;
// Resources
use App\Http\Resources\Candidate\ComputerSkillResource;

/**
 * @group V2
 * @authenticated
 */
class ComputerSkillController extends Controller
{
    /**
     * Candidate - Computer skills
     */
    public function index(Request $request): ComputerSkillCollection
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return new ComputerSkillCollection($user->computer_skills);
    }

    /**
     * Candidate - Computer skill store (create)
     *
     * @bodyParam computer_skill_name_id integer required The id of the ComputerSkillName object. Example: 1
     * @bodyParam computer_skill_knowledge_level_id integer required The id of the ComputerSkillKnowledgeLevel object. Example: 1
     */
    public function store(ComputerSkillRequest $request): ComputerSkillResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $computerSkill = $user->computer_skills()->create($request->validated());

        return (new ComputerSkillResource($computerSkill))->additional(['message' => 'Computer skill successfully added.']);
    }

    /**
     * Candidate - Computer skill update
     *
     * @bodyParam computer_skill_name_id integer required The id of the ComputerSkillName object. Example: 1
     * @bodyParam computer_skill_knowledge_level_id integer required The id of the ComputerSkillKnowledgeLevel object. Example: 1
     * @urlParam id integer required
     */
    public function update(ComputerSkillRequest $request, $id): ComputerSkillResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $computerSkill = $user->computer_skills()->findOrFail($id);
        $computerSkill->update($request->validated());

        return (new ComputerSkillResource($computerSkill))->additional(['message' => 'Computer skill successfully updated.']);
    }

    /**
     * Candidate - Computer skill delete
     *
     * @urlParam id integer required
     */
    public function destroy(ComputerSkillRequest $request, $id): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $computerSkill = $user->computer_skills()->findOrFail($id);
        $computerSkill->delete();

        return response()->json(['message' => 'Computer skill successfully removed.']);
    }
}
