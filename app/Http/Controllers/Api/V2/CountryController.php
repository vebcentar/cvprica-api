<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Models
use App\Models\Country;
// Requests
// use App\Http\Requests\Admin\UserRequest;
// Resources
use App\Http\Resources\CountryResource;

/**
 * @group V2
 */

class CountryController extends Controller
{
    /**
     * Countries
     *
     * @return App\Http\Resources\CountryResource
     */
    public function index()
    {
        return CountryResource::collection(Country::all());
    }

}
