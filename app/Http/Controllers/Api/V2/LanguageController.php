<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Candidate\LanguageCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
// Models
use App\Models\User;
// Requests
use App\Http\Requests\Candidate\LanguageRequest;
// Resources
use App\Http\Resources\Candidate\LanguageResource;

/**
 * @group V2
 * @authenticated
 */
class LanguageController extends Controller
{
    /**
     * Candidate - Languages
     */
    public function index(LanguageRequest $request): LanguageCollection
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return new LanguageCollection($user->languages);
    }

    /**
     * Candidate - Language store (create)
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam languages_id integer required The id of the Language object. Example: 1
     * @bodyParam language_reads_id integer required The id of the Language read object. Example: 1
     * @bodyParam language_writes_id integer required The id of the Language write object. Example: 1
     * @bodyParam language_speaks_id integer required The id of the Language speak object. Example: 1
     */
    public function store(LanguageRequest $request): LanguageResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $language = $user->languages()->create($request->validated());

        return (new LanguageResource($language))->additional(['message' => 'Language successfully added.']);
    }

    /**
     * Candidate - Language update
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam languages_id integer required The id of the Language object. Example: 1
     * @bodyParam language_reads_id integer required The id of the Language read object. Example: 1
     * @bodyParam language_writes_id integer required The id of the Language write object. Example: 1
     * @bodyParam language_speaks_id integer required The id of the Language speak object. Example: 1
     * @urlParam id integer required
     */
    public function update(LanguageRequest $request, $id): LanguageResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $language = $user->languages()->findOrFail($id);
        $language->update($request->validated());

        return (new LanguageResource($language))->additional(['message' => 'Language successfully updated.']);
    }

    /**
     * Candidate - Language delete
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam id integer required
     */
    public function destroy(LanguageRequest $request, $id): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $language = $user->languages()->findOrFail($id);
        $language->delete();

        return response()->json(['message' => 'Language successfully removed.']);
    }
}
