<?php

namespace App\Http\Controllers\Api\V2;

use App\Exceptions\BusinessLogicException;
use App\Http\Controllers\Controller;
use App\Http\Resources\MeResource;
use App\Models\DriversLicenceCategory;
use App\Services\EmailVerificationService;
use App\Services\MediaService;
use App\Services\S3ClientService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDocument;
use App\Models\AdSharedInfo;
use App\Models\DriversLicence;
use App\Http\Requests\Candidate\DashboardRequest;
use App\Http\Requests\Candidate\UpdateRequest;
use App\Http\Requests\Candidate\UpdateFieldsRequest;
use App\Http\Requests\Candidate\UpdateProfileImageRequest;
use App\Http\Requests\Candidate\UpdateCvDocumentRequest;
use App\Http\Requests\Candidate\UpdateDriverLicenceRequest;
use App\Http\Resources\Candidate\DashboardResource;
use App\Http\Resources\Candidate\InfoResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

/**
 * @group V2
 */

class CandidateController extends Controller
{
    /**
     * Candidate - Dashboard
     *
     * @authenticated
     */
    public function dashboard(DashboardRequest $request): DashboardResource
    {
        return new DashboardResource($request);
    }

    /**
     * Candidate - Info
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     *
     * @authenticated
     *
     */
    public function info(Request $request): InfoResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        return new InfoResource($user);
    }

    /**
     * Candidate - Update
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @authenticated

     * @return MeResource
     */
    public function update(UpdateRequest $request, EmailVerificationService $emailVerificationService, UserService $userService)
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        $updateData = $request->validated();

        if ($request->email && $user->email !== $request->email && User::query()->where('email', $request->email)->where('id', '<>', $user->id)->exists()) {
            throw new BusinessLogicException('Email must be unique');
        }

        $oldEmail = $user->email;

        if ($request->phone) {
            $userService->changeUserPhone($user, $request->phone);
        }

        if ($updateData['email'] != $oldEmail) {
            $user->email_verified_at = null;
            $emailVerificationService->sendVerificationEmailForUser($user);
        }

        $user->update($updateData);
        $user->refresh();

        return (new MeResource($user))
            ->additional([
                'message' => 'The candidate successfully updated.'
            ]);
    }

    /**
     * Candidate - Update optional fields
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam old_password string Old password required if the user is not registered via social networks.
     * @authenticated
     */
    public function fieldsUpdate(UpdateFieldsRequest $request, EmailVerificationService $emailVerificationService): MeResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $oldEmail = $user->email;

        if ($request->email && $user->email !== $request->email && User::query()->where('email', $request->email)->where('id', '<>', $user->id)->exists()) {
            throw new BusinessLogicException('Email must be unique');
        }

        if ($request->password && $user->isRegisteredViaSocialNetwork() === false) {
            if(! \Hash::check($request->old_password, $user->password))
            {
                return (new MeResource($user))
                    ->additional([
                        'message' => 'The old password is incorrect.',
                    ])
                    ->response()
                    ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $user->update($request->validated());

        if ($user->email !== $oldEmail) {
            $user->email_verified_at = null;
            $emailVerificationService->sendVerificationEmailForUser($user);
            $user->save();
        }

        if (isset($request->city_notifications_ids) || $request->city_notifications_ids === null) {

            $user->city_notifications()->whereNotIn('city_id', (array)$request->city_notifications_ids)->delete();

            $userCityNotificationsIds = $user->city_notifications()->pluck('city_id')->toArray();
            $cityNotificationsIds = array_diff((array)$request->city_notifications_ids, $userCityNotificationsIds);

            foreach($cityNotificationsIds as $id) {
                $user->city_notifications()->create([
                    'city_id' => $id
                ]);
            }
        }


        if (isset($request->type_of_work_notifications_ids)) {
            $user->type_of_work_notifications()->whereNotIn('type_of_work_id', (array)$request->type_of_work_notifications_ids)->delete();

            $userTypeOfWorkNotificationsIds = $user->type_of_work_notifications()->pluck('type_of_work_id')->toArray();
            $typeOfWorkNotificationsIds = array_diff((array)$request->type_of_work_notifications_ids, $userTypeOfWorkNotificationsIds);

            foreach($typeOfWorkNotificationsIds as $id) {
                $user->type_of_work_notifications()->create([
                    'type_of_work_id' => $id
                ]);
            }
        }

        return (new MeResource($user))
            ->additional([
                'message' => 'The candidate successfully updated.'
            ])
        ;
    }

    /**
     * Candidate - Update profile image
     * @authenticated
     * @param  \App\Http\Requests\Candidate\UpdateProfileImageRequest  $request
     * @return InfoResource
     */
    public function fieldsProfileImageUpdate(UpdateProfileImageRequest $request, MediaService $mediaService)
    {
        $user = $request->user();

        $mediaService->updateUserProfileImage(
            $user,
            $request->file('image')
        );

        return (new InfoResource($user))
            ->additional(['message' => 'The candidate "profile image" successfully updated.'])
        ;
    }

    /**
     * Candidate - Update CV document
     * @authenticated
     * @param  \App\Http\Requests\Candidate\UpdateCvDocumentRequest  $request
     * @return InfoResource
     */
    public function updateCvDocument(UpdateCvDocumentRequest $request)
    {
        $user = $request->user();
        //@todo Refactor document uploading process (put in a service, create interface).
        $document = UserDocument::where(["user_id" => $user->id])->first();

        if ($document) {
            $applyCv = AdSharedInfo::where('cv', $document->document_link);
            if ($document->document_link && !$applyCv->first()) {
                unlink(public_path() . "/" . $document->document_link);
            }
            $document->delete();
        }


        $input = $request->validated();
        if ($file = $request->file('document')) {
            $name = Uuid::uuid4().'.'.$file->getClientOriginalExtension();
            $name = str_replace(" ", "", $name);
            $file->move('user-data/documents/cv/' . $user->id, $name);
            $input['document_name'] = $name;
            $input['document_link'] = 'user-data/documents/cv/' . $user->id . "/" . $name;
        }

        $input["user_id"] = $user->id;
        $input["modified_by"] = $user->id;
        $input["created_by"] = $user->id;
        $cv = UserDocument::create($input);

        return (new InfoResource($user))
            ->additional(['message' => 'The candidate "CV document" successfully updated.'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Candidate - Delete CV document
     * @authenticated
     */
    public function deleteCvDocument(MediaService $service): MeResource
    {
        /** @var User $user */
        $user = Auth::user();

        $service->deleteUserCvDocument($user->id);

        $user->refresh();

        return new MeResource($user);
    }


    /**
     * Candidate - Update profile video
     *
     * @authenticated
     */
    public function updateProfileVideo(Request $request, MediaService $mediaService, S3ClientService $s3ClientService)
    {
        /** @var User $user */
        $user = $request->user();

        if ($user->isAdmin() && $request->user_id) {
            $user = User::query()->findOrFail($request->user_id);
        }

        $validator = Validator::make($request->all(), [
            'video' => 'required|string|regex:'.$s3ClientService->regex(),
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $mediaService->updateUserCVVideo($user, $request->video);

        $user->refresh();

        return (new InfoResource($user))->additional(['message' => 'The candidate "profile video" successfully updated.']);
    }

    /**
     * Candidate - Download profile video
     *
     * @authenticated
     */
    public function downloadProfileVideo(MediaService $mediaService)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->cv_video()->exists()) {
            return new JsonResponse([ 'message' => 'Not found'], 404);
        }

        $filePath = $mediaService->filePathForCvVideo($user->cv_video);
        $baseName = pathinfo($filePath, PATHINFO_BASENAME);

        return response()->download($filePath, $baseName, [ 'Content-Type' => File::mimeType($filePath)] );
    }

    /**
     * Candidate - Download cv document
     *
     * @authenticated
     */
    public function downloadCVDocument(MediaService $mediaService)
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var UserDocument $cvDocument */
        if (!$cvDocument = $user->cv_document()->first()) {
            return new JsonResponse([ 'message' => 'Not found'], 404);
        }

        $filePath = $mediaService->filePathForUserDocument($cvDocument);
        $baseName = pathinfo($filePath, PATHINFO_BASENAME);

        return response()->download($filePath, $baseName, [ 'Content-Type' => File::mimeType($filePath)] );
    }

    /**
     * Candidate - Delete profile video
     * @authenticated
     *
     * Removes profile video for user; File is not deleted if user applied to a job with this video.
     *
     * @return JsonResponse
     */
    public function deleteProfileVideo(MediaService $mediaService): JsonResponse
    {
        $mediaService->deleteUserProfileVideo(Auth::id());

        return response()->json([]);
    }

    /**
     * Candidate - Update driver licence
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @authenticated

     * @return InfoResource
     */
    public function updateDriverLicence(UpdateDriverLicenceRequest $request)
    {
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $licence = DriversLicence::where(["user_id" => $user->id])->first();

        $input = $request->validated();
        $input["modified_by"] = $user->id;

        if (!$licence) {
            $input["user_id"] = $user->id;
            $input["created_by"] = $user->id;
            $licence = DriversLicence::create($input);
            $licenceNew = $licence->first();
            $category = DriversLicenceCategory::findOrFail($licenceNew->drivers_licence_category_id);
            $licenceNew->category = $category->name;
        } else {
            $licence->update($input);
        }

        $user->refresh();
        return new InfoResource($user);
    }


    /**
     * Candidate - Deactivate
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @authenticated
     */
    public function deactivate(Request $request): InfoResource
    {
        $user = $request->user();
        if ($request->user()->role === 'admin' && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        $user->update(["is_active" => 0]);

        return (new InfoResource($user))->additional(['message' => 'The candidate successfully deactivate.']);
    }
}
