<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Candidate\WorkExperienceCollection;
use App\Models\WorkExperience;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
// Models
use App\Models\User;
// Requests
use App\Http\Requests\Candidate\WorkExperienceRequest;
// Resources
use App\Http\Resources\Candidate\WorkExperienceResource;

/**
 * @group V2
 * @authenticated
 */
class WorkExperienceController extends Controller
{
    /**
     * Candidate - Work experiences
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     */
    public function index(Request $request): WorkExperienceCollection
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return new WorkExperienceCollection($user->work_experiences);
    }

    /**
     * Candidate - Work experience store (create)
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam company_name string required
     * @bodyParam location string required
     * @bodyParam position company_name string required
     * @bodyParam type_of_work_id integer required The id of the Type of work. Example: 1
     * @bodyParam start_date date required
     * @bodyParam end_date date required_unless:ongoing,1
     * @bodyParam ongoing boolean
     */
    public function store(WorkExperienceRequest $request): WorkExperienceResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $workExperience = $user->work_experiences()->create($request->validated());

        return (new WorkExperienceResource($workExperience))->additional(['message' => 'Work experience successfully added.']);
    }

    /**
     * Candidate - Work experience update
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam company_name string required
     * @bodyParam location string required
     * @bodyParam position company_name string required
     * @bodyParam type_of_work_id integer required The id of the Type of work. Example: 1
     * @bodyParam start_date date required
     * @bodyParam end_date date required_unless:ongoing,1
     * @bodyParam ongoing boolean
     * @bodyParam ongoing string
     * @urlParam id integer required
     */
    public function update(WorkExperienceRequest $request, $id): WorkExperienceResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $workExperience = $user->work_experiences()->findOrFail($id);
        $workExperience->update($request->validated());

        return (new WorkExperienceResource($workExperience))->additional(['message' => 'Work experience successfully updated.']);
    }

    /**
     * Candidate - Work experience delete
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam id integer required
     */
    public function destroy(WorkExperienceRequest $request, $id): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $workExperience = WorkExperience::query()
            ->where('id', $id)
            ->where('user_id', $user->id)
            ->first()
        ;

        if (!$workExperience) {
            return response()->json([ 'message' => 'Not found.' ], 404);
        }
        $workExperience->delete();

        return response()->json([ 'message' => 'Work experience successfully removed.' ]);
    }
}
