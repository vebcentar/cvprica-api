<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Candidate\EducationCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
// Models
use App\Models\User;
// Requests
use App\Http\Requests\Candidate\EducationRequest;
// Resources
use App\Http\Resources\Candidate\EducationResource;

/**
 * @group V2
 * @authenticated
 */
class EducationController extends Controller
{
    /**
     * Candidate - Educations
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     */
    public function index(EducationRequest $request): EducationCollection
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return new EducationCollection($user->educations);
    }

    /**
     * Candidate - Education store (create)
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam education_level_id integer required The id of the Education level. Example: 1
     * @bodyParam education_area_id integer required The id of the Education area. Example: 1
     * @bodyParam title string required
     * @bodyParam institution string required
     * @bodyParam course string required
     * @bodyParam city string required
     * @bodyParam start_date date required
     * @bodyParam end_date date required_unless:ongoing,1
     * @bodyParam ongoing boolean
     */
    public function store(EducationRequest $request): EducationResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $education = $user->educations()->create($request->validated());

        return (new EducationResource($education))->additional(['message' => 'Education successfully added.']);
    }

    /**
     * Candidate - Education update
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @bodyParam education_level_id integer required The id of the Education level. Example: 1
     * @bodyParam education_area_id integer required The id of the Education area. Example: 1
     * @bodyParam title string required
     * @bodyParam institution string required
     * @bodyParam course string required
     * @bodyParam city string required
     * @bodyParam start_date date required
     * @bodyParam end_date date required_unless:ongoing,1
     * @bodyParam ongoing boolean
     * @urlParam id integer required
     */
    public function update(EducationRequest $request, $id): EducationResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        $education = $user->educations()->findOrFail($id);
        $education->update($request->validated());

        return (new EducationResource($education))->additional(['message' => 'Education successfully updated.']);
    }

    /**
     * Candidate - Education delete
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam id integer required
     */
    public function destroy(EducationRequest $request, $id): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $education = $user->educations()->findOrFail($id);
        $education->delete();

        return response()->json(['message' => 'Education successfully removed.']);
    }
}
