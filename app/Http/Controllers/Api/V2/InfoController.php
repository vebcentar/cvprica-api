<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\Candidate\CandidatePublicInfoResource;
use App\Http\Resources\Company\CompanyPublicInfoResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 */
class InfoController extends Controller
{
    /**
     * Candidate Info (company or admin roles)
     *
     * @urlParam candidate integer ID of candidate
     */
    public function candidate($id)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->isCompany() && !$user->isAdmin()) {
            abort(403, 'Forbidden.');
        }

        /** @var User $candidate */
        $candidate = User::findOrFail($id);

        if (!$candidate->isCandidate()) {
            return response()->json([ 'message' => 'User is not a candidate'], 400);
        }

        return new CandidatePublicInfoResource($candidate);
    }

    /**
     * Company Info - public route
     *
     * @urlParam companyId integer ID of company
     */
    public function company($companyId)
    {
        /** @var User $user */
        $user = User::findOrFail($companyId);

        if (!$user->isCompany()) {
            return response()->json([ 'message' => 'Not a company'], 400);
        }

        return new CompanyPublicInfoResource($user);
    }
}
