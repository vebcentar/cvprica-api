<?php

namespace App\Http\Controllers\Api\V2;

use App\Exceptions\BusinessLogicException;
use App\Http\Controllers\Controller;
use App\Models\AdSharedInfo;
use App\Models\User;
use App\Services\AdAnswerService;
use App\Services\ApplicationService;
use App\Services\NotificationService;
use Illuminate\Http\JsonResponse;
use App\Models\Ad;
use App\Models\Notification;
use App\Http\Requests\Candidate\ApplicationRequest;
use App\Http\Resources\Candidate\ApplicationResource;
use App\Http\Resources\Candidate\ApplicationCollection;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group V2
 * @authenticated
 */
class ApplicationController extends Controller
{
    /**
     * Candidate - Applications
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @urlParam type string Type (draft|send). Example: 1
     * @urlParam type_of_work_id integer The id of the Type of work. Example: 1
     * @urlParam city_id string  The id of the City. Example: 1
     * @urlParam term string Term.
     * @urlParam seen boolean Seen.
     */
    public function index(ApplicationRequest $request, ApplicationService $applicationService): ApplicationCollection
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        return $applicationService->filterApplicationsForCandidate($user->id, $request);
    }

    /**
     * Candidate - Application store (create)
     */
    public function store(ApplicationRequest $request, AdAnswerService $adAnswerService, NotificationService $notificationService)
    {
        //@todo Refactor
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        if (!$user->isPhoneAndEmailVerified() && $request->applied) {
            return response()->json(['message' => 'Must be a verified user.'],403);
        }

        $user->applications()->where('ad_id', $request->ad_id)->delete();

        $application = $user->applications()->where('ad_id', $request->ad_id)->first();
        if ($application) {
            return (new ApplicationResource($application))
                ->additional([
                    'message' => 'Application already created.',
                    'request_all' => $request->all(),
                    'request_validated' => $request->validated(),
                ])
                ->response()
                ->setStatusCode(Response::HTTP_CONFLICT);
        }
        /** @var Ad $ad */
        $ad = Ad::findOrFail($request->ad_id);
        $cv = $user->cv_document->first();
        $cvPath = $cv ? $cv->document_link : null;
        $data = $request->validated();
        $data['full_name'] = $user->full_name;
        $data['email'] = $user->email;
        $data['profile_image'] = $user->profile_image;
        $data['education'] = $user->educations;
        $data['languages'] = $user->languages;
        $data['computer_skills'] = $user->computer_skills;
        $data['additional_information'] = $user->aditional_info;
        $data['experience'] = $user->work_experiences;
        $data['gender_id'] = $user->gender_id;
        $data['country_id'] = $user->country_id;
        $data['city_id'] = $user->city_id;
        $data['address'] = $user->address;
        $data['cv'] = $cvPath;
        $data['video'] = $user->cv_video ? $user->cv_video->video : null;

        $adAnswers = $adAnswerService->handleAnswersFromRequest($user, $request->answers ?? []);
        $questionsCount = $ad->questions()->count();

        if ($request->applied && $questionsCount > count($adAnswers)) {
            throw new BusinessLogicException('Must answer all the questions to apply');
        }

        if ($request->applied && !$user->isProfileCompleted()) {
            throw new BusinessLogicException('Must complete profile in order to apply');
        }

        $application = $user->applications()->create($data);

        if ($application->applied) {
            $notificationService->sendNotificationForNewApplication($application);
        }

        return (new ApplicationResource($application))
            ->additional([
                'message' => 'Application successfully added.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_CREATED);

    }

    /**
     * Candidate - Application show
     *
     * @urlParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     */
    public function show(ApplicationRequest $request, $id): ApplicationResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $application = $user->applications()->findOrFail($id);

        return (new ApplicationResource($application));
    }

    /**
     * Candidate - Application update
     *
     * @urlParam id integer required
     */
    public function update(ApplicationRequest $request, $id, AdAnswerService $adAnswerService, NotificationService $notificationService)
    {
        //@todo Refactor
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        /** @var AdSharedInfo $application */
        $application = $user->applications()->findOrFail($id);

        if (!$user->isPhoneAndEmailVerified() && $request->applied) {
            return response()->json(['message' => 'Must be a verified user.'],403);
        }

        $cv = $user->cv_document->first();
        $cvPath = $cv ? $cv->document_link : null;

        $data = $request->validated();
        $data['full_name'] = $user->full_name;
        $data['email'] = $user->email;
        $data['profile_image'] = $user->profile_image;
        $data['education'] = $user->educations;
        $data['languages'] = $user->languages;
        $data['computer_skills'] = $user->computer_skills;
        $data['additional_information'] = $user->aditional_info;
        $data['experience'] = $user->work_experiences;
        $data['gender_id'] = $user->gender_id;
        $data['country_id'] = $user->country_id;
        $data['city_id'] = $user->city_id;
        $data['address'] = $user->address;
        $data['cv'] = $cvPath;
        if (!$application->video && !$application->applied) {
            $data['video'] = $user->cv_video ? $user->cv_video->video : null;
        }

        $adAnswers = $adAnswerService->handleAnswersFromRequest($user, $request->answers ?? []);
        $questionsCount = $application->ad()->questions()->count();

        if ($request->applied && $questionsCount > count($adAnswers)) {
            throw new BusinessLogicException('Must answer all the questions to apply');
        }

        if ($request->applied && !$user->isProfileCompleted()) {
            throw new BusinessLogicException('Must complete profile in order to apply');
        }

        $application->update($data);

        if ($application->applied) {
            $notificationService->sendNotificationForNewApplication($application);
        }

        return (new ApplicationResource($application))->additional(['message' => 'Application successfully updated.']);
    }

    /**
     * Candidate - Application delete
     */
    public function destroy(ApplicationRequest $request, $id): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $application = $user->applications()->findOrFail($id);
        $application->delete();

        return response()->json(['message' => 'Application successfully removed.']);
    }
}
