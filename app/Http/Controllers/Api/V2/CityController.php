<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Models
use App\Models\City;
// Requests
// use App\Http\Requests\Admin\UserRequest;
// Resources
use App\Http\Resources\CityResource;

/**
 * @group V2
 */

class CityController extends Controller
{
    /**
     * Cities
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validated = $request->validate([
            'country_id' => ['nullable', 'integer', 'exists:App\Models\Country,id'],
            'term' => ['nullable', 'string'],
        ]);
        $cities = City::when($request->country_id, function($query) use ($request) {
            return $query->where("country_id", $request->country_id);
        })
        ->when($request->term, function($query) use ($request) {
            return $query->where("name", "LIKE", "%{$request->term}%");
        })->get();
        return CityResource::collection($cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
