<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\MeResource;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group V2
 */
class MeController extends Controller
{
    /**
     * Logged in user info
     */
    public function me(): MeResource
    {
        return new MeResource(Auth::user());
    }

    /**
     * Profile completeness attribute
     */
    public function profileCompleted(): JsonResponse
    {
        return new JsonResponse([
            'profile_completed' => Auth::user()->profile_completely
        ]);
    }

    /**
     * Set firebase device token
     */
    public function setDeviceToken(Request $request, UserService $userService)
    {
        $request->validate([ 'device_token' => ['string', 'required']]);

        $userService->setDeviceToken(Auth::user(), (string) $request->device_token);

        return new JsonResponse([]);
    }

    /**
     * Delete firebase device token
     */
    public function deleteDeviceToken(UserService $userService)
    {
        $userService->deleteDeviceToken(Auth::user());

        return new JsonResponse([]);
    }
}
