<?php

namespace App\Http\Controllers\Api\V2;

use App\Exceptions\BusinessLogicException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Company\CompanyPublicInfoCollection;
use App\Http\Resources\MeResource;
use App\Services\CompanyService;
use App\Services\EmailVerificationService;
use App\Services\MediaService;
use App\Services\S3ClientService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\Company\DashboardRequest;
use App\Http\Requests\Company\UpdateRequest;
use App\Http\Requests\Company\UpdateProfileImageRequest;
use App\Http\Requests\Company\UpdateBackgroundImageRequest;
use App\Http\Resources\Company\DashboardResource;
use App\Http\Resources\Company\InfoResource;
use Illuminate\Support\Facades\Validator;

/**
 * @group V2
 * @authenticated
 */

class CompanyController extends Controller
{
    /**
     * Company - Dashboard
     */
    public function dashboard(DashboardRequest $request): DashboardResource
    {
        $user = $request->user();
        return new DashboardResource($user);
    }

    /**
     * Company - Info
     */
    public function info(Request $request): InfoResource
    {
        $user = $request->user();
        return new InfoResource($user);
    }

    /**
     * Company - Update
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     * @param UpdateRequest $request
     *
     * @return MeResource
     */
    public function update(UpdateRequest $request, EmailVerificationService $emailVerificationService, UserService $userService)
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }
        if (!$user->isCompany()) {
            return response()->json([ 'message' => 'Not a company.' ], 400);
        }
        $updateData = $request->validated();

        if ($request->contact_phone) {
            $newPhone = $updateData['contact_phone'] ?? null;
            $userService->changeUserPhone($user, $newPhone);
        }

        $oldEmail = $user->email;

        if ($request->email && $user->email !== $request->email && User::query()->where('email', $request->email)->where('id', '<>', $user->id)->exists()) {
            throw new BusinessLogicException('Email must be unique');
        }

        $user->update($updateData);

        if ($oldEmail && $oldEmail !== $user->email) {
            $emailVerificationService->sendVerificationEmailForUser($user);
            $user->email_verified_at = null;
            $user->save();
        }

        $company_users = $user->company_users()->firstOrNew();
        $company_users->fill($request->validated());
        $company_users->save();

        return (new MeResource($user))->additional(['message' => 'The company successfully updated.']);
    }


    /**
     * Company - Update profile image
     */
    public function fieldsProfileImageUpdate(UpdateProfileImageRequest $request, MediaService $mediaService): InfoResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $mediaService->updateUserProfileImage(
            $user,
            $request->file('image')
        );

        return (new InfoResource($user))->additional(['message' => 'The company "profile image" successfully updated.']);
    }

    /**
     * Company - Update background image
     */
    public function fieldsBackgroundImageUpdate(UpdateBackgroundImageRequest $request, MediaService $mediaService): InfoResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $mediaService->updateUserBackgroundImage(
            $user,
            $request->file('image')
        );

        return (new InfoResource($user))->additional(['message' => 'The company "background image" successfully updated.']);
    }



    /**
     * Company - Update profile video
     */
    public function updateProfileVideo(Request $request, MediaService $mediaService, S3ClientService $s3ClientService)
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $validator = Validator::make($request->all(), [
            'video' => 'required|string|regex:'.$s3ClientService->regex(),
        ]);

        if ($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->messages()]);
        }

        $mediaService->updateCompanyVideo($user, $request->video);

        return (new InfoResource($user))->additional(['message' => 'The company "profile video" successfully updated.']);
    }

    /**
     * Company - Deactivate
     *
     * @bodyParam user_id integer The id of the User (Parameter for admin roles). Example: 1
     */
    public function deactivate(Request $request): InfoResource
    {
        /** @var User $user */
        $user = $request->user();
        if ($user->isAdmin() && $request->user_id) {
            $user = User::findOrFail($request->user_id);
        }

        $user->update(["is_active" => 0]);

        return (new InfoResource($user))->additional(['message' => 'The company successfully deactivate.']);
    }

    /**
     * Public company info list
     *
     * @urlParam is_eminent boolean Filter by eminent status of teh company. (0|1) Example: 1
     * @urlParam term string Filter by company's full name. Example: 'web'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0, Example: 20
     * @urlParam limit integer Limit on the number of results. Default: 20, Min: 5, Max: 100 Example: 10
     */
    public function publicCompanies(Request $request, CompanyService $companyService): CompanyPublicInfoCollection
    {
        $request->validate([
            'is_eminent' =>  [ 'boolean' ],
            'offset' =>  [ 'integer', 'min:0' ],
            'limit' =>  [ 'integer', 'min:5', 'max:100' ],
            'term' => [ 'max:200' ]
        ]);

        $isEminent = $request->has('is_eminent') ? (bool) $request->is_eminent : null;

        return $companyService->publicCompanies(
            (int) $request->offset,
            (int) $request->limit,
            $request->term ?? null,
            $isEminent
        );
    }

    /**
     * Company info. Admin role required.
     *
     * @urlParam companyId integer Id of company. Example 1
     */
    public function company($companyId)
    {
        /** @var User $company */
        $company = User::query()->findOrFail($companyId);

        if (!$company->isCompany()) {
            return response()->json([ 'message' => 'Not a company.'], 400);
        }

        return new InfoResource($company);
    }
}
