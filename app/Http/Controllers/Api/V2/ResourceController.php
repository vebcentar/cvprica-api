<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Services\CommonDataService;
use App\Services\JobSearchFilterService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Language;
use App\Models\LanguageRead;
use App\Models\LanguageSpeak;
use App\Models\LanguageWrite;
use App\Models\UserLanguage;
use App\Models\ComputerSkillName;
use App\Models\DriversLicenceCategory;
use App\Models\EducationArea;
use App\Models\EducationLevel;
use App\Models\EducationTitle;
use App\Models\Gender;
use App\Models\JobCategory;
use App\Models\JobType;
use App\Models\Package;
use App\Models\TypeOfWork;
use App\Models\WorkTime;
use App\Http\Resources\BasicCollection;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\LanguageReadResource;
use App\Http\Resources\LanguageSpeakResource;
use App\Http\Resources\LanguageWriteResource;
use App\Http\Resources\UILanguageResource;
use App\Http\Resources\ComputerSkillResource;
use App\Http\Resources\DriversLicenceCategoryResource;
use App\Http\Resources\EducationAreaResource;
use App\Http\Resources\EducationLevelResource;
use App\Http\Resources\EducationTitleResource;
use App\Http\Resources\GenderResource;
use App\Http\Resources\JobCategoryResource;
use App\Http\Resources\JobTypeResource;
use App\Http\Resources\PackageResource;
use App\Http\Resources\TypeOfWorkResource;
use App\Http\Resources\WorkTimeResource;

/**
 * @group V2
 */

class ResourceController extends Controller
{
    /**
     * UI Languages
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function uiLanguages()
    {
        return UILanguageResource::collection(UserLanguage::all());
    }

    /**
     * Languages
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function languages()
    {
        return LanguageResource::collection(Language::all());
    }

    /**
     * Language reads
     *
     * @return App\Http\Resources\LanguageReadResource
     */
    public function languageReads()
    {
        return LanguageReadResource::collection(LanguageRead::all());
    }

    /**
     * Language speaks
     *
     * @return App\Http\Resources\LanguageSpeakResource
     */
    public function languageSpeaks()
    {
        return LanguageSpeakResource::collection(LanguageSpeak::all());
    }

    /**
     * Language writes
     *
     * @return App\Http\Resources\LanguageWriteResource
     */
    public function languageWrites()
    {
        return LanguageWriteResource::collection(LanguageWrite::all());
    }

    /**
     * Computer skill names
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function computerSkillNames()
    {
        return ComputerSkillResource::collection(ComputerSkillName::all());
    }

    /**
     * Drivers licence categories
     *
     * @return App\Http\Resources\DriversLicenceCategoryResource
     */
    public function driversLicenceCategories()
    {
        return DriversLicenceCategoryResource::collection(DriversLicenceCategory::all());
    }

    /**
     * Education areas
     *
     * @return App\Http\Resources\EducationAreaResource
     */
    public function educationAreas()
    {
        return EducationAreaResource::collection(EducationArea::all());
    }

    /**
     * Education levels
     *
     * @return App\Http\Resources\EducationLevelResource
     */
    public function educationLevels()
    {
        return EducationLevelResource::collection(EducationLevel::all());
    }

    /**
     * Education titles
     *
     * @return App\Http\Resources\EducationTitleResource
     */
    public function educationTitles()
    {
        return EducationTitleResource::collection(EducationTitle::all());
    }

    /**
     * Genders
     *
     * @return App\Http\Resources\GenderResource
     */
    public function genders()
    {
        return GenderResource::collection(Gender::all());
    }

    /**
     * Job categories
     *
     * @return App\Http\Resources\JobCategoryResource
     */
    public function jobCategories()
    {
        return JobCategoryResource::collection(JobCategory::all());
    }

    /**
     * Job types
     *
     * @return App\Http\Resources\JobTypeResource
     */
    public function jobTypes()
    {
        return JobTypeResource::collection(JobType::all());
    }

    /**
     * Packages
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function packages()
    {
        return PackageResource::collection(Package::all());
    }

    /**
     * Type of works
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function typeOfWorks()
    {
        return TypeOfWorkResource::collection(TypeOfWork::all());
    }

    /**
     * Work times
     *
     * @return App\Http\Resources\LanguageResource
     */
    public function workTimes()
    {
        return WorkTimeResource::collection(WorkTime::all());
    }

    /**
     * Companies
     *
     * @urlParam term string Term search parameter. Example: Web Centar
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     * @urlParam only_with_ads boolean Returns only companies that have active ads. Default: 1 Example: 0
     * @param  App\Http\Requests\Admin  $request
     * @return \App\Http\Resources\Admin\AdCollection
     */
    public function companies(Request $request)
    {
        $request->validate([
            'term' => ['nullable', 'string'],
            'offset' => ['nullable', 'integer', 'min:0'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'only_with_ads' => ['boolean'],
        ]);

        $onlyWithAds = $request->input('only_with_ads', true);

        $companies = User::query()->select(['id', 'full_name'])
                        ->withCount(['ads' => function($query) {
                            return $query->where('is_active', 1)
                            ->where('is_archived', 0)
                            ->where('is_hidden', 0)
                            ->where('end_date', '>=', date('Y-m-d'))
                            ;
                        }])
                        ->where('role', 'company')
                        ->where('is_archived', 0)
                        ->where('is_active', 1)
                        ->where('is_hidden', 0)
                        ->when($request->term, function ($query) use ($request) {
                            return $query->where('full_name', 'LIKE', "%{$request->term}%");
                        });

        if ($onlyWithAds) {
            $companies->having('ads_count', '>', 0);
        }
        $companies_count = $companies->count();
        $companies = $companies
            ->orderBy('full_name', 'ASC')
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? 20)
            ->get();

        return (new BasicCollection($companies))->additional(
            ['meta' => [
                    'total' => $companies_count,
                ]
            ]
            );
    }

    /**
     * Common data
     */
    public function commonData(CommonDataService $commonDataService): JsonResponse
    {
        return response()->json(
            $commonDataService->fetchCommonData()
        );
    }

    /**
     * Job search all filters
     */
    public function jobSearchFilters(JobSearchFilterService $filterService): JsonResponse
    {
        return response()->json(
            $filterService->allFilters()
        );
    }

    /**
     * Job search company filter
     *
     * @urlParam term string Term to search for
     */
    public function jobSearchCompanyFilter(Request $request, JobSearchFilterService $filterService): JsonResponse
    {
        return response()->json(
            $filterService->companiesFilter($request->term ?? null)
        );
    }

    /**
     * Job search type of work filter
     *
     * @urlParam term string Term to search for
     */
    public function jobSearchTypeOfWorkFilter(Request $request, JobSearchFilterService $filterService): JsonResponse
    {
        return response()->json(
            $filterService->typeOfWorkFilter($request->term ?? null)
        );
    }

    /**
     * Job search location filter
     *
     * @urlParam term string Term to search for
     */
    public function jobSearchLocationFilter(Request $request, JobSearchFilterService $filterService): JsonResponse
    {
        return response()->json(
            $filterService->locationFilter($request->term ?? null)
        );
    }

    /**
     * Job search terms filter
     *
     * @urlParam term string Term to search for
     */
    public function jobSearchTermFilter(Request $request, JobSearchFilterService $filterService): JsonResponse
    {
        $request->validate([
            'term' => 'required|string|min:1'
        ]);

        return response()->json(
            $filterService->termFilter($request->term)
        );
    }
}
