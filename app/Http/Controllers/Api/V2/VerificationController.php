<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Http\Resources\MeResource;
use App\Models\User;
use App\Services\EmailVerificationService;
use App\Services\OTP\OTPService;
use Illuminate\Http\JsonResponse;
use App\Services\IpificationService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * @group V2
 */
class VerificationController extends Controller
{
    /**
     * Generate phone verification state
     */
    public function generatePhoneState(IpificationService $service): JsonResponse
    {
        $state = $service->generateStateForUser(Auth::user());

        return new JsonResponse([ 'state' => $state ]);
    }

    /**
     * Generate phone verification url
     */
    public function generatePhoneVerificationUrl(IpificationService $ipificationService): JsonResponse
    {
        return new JsonResponse([
            'url' => $ipificationService->generateVerificationUrl(Auth::user())
        ]);
    }

    /**
     * Generate phone verification QR code image
     */
    public function generatePhoneQRCode(IpificationService $ipificationService)
    {
        return QrCode::size(500)->generate($ipificationService->generateVerificationUrl(Auth::user()));
    }

    /**
     * Start process of phone verification
     */
    public function startIpification(IpificationService $ipificationService)
    {
        $url = $ipificationService->generateVerificationUrl(Auth::user());

        return new RedirectResponse($url);
    }

    /**
     * Ipification callback
     */
    public function ipificationCallback(Request $request, IpificationService $ipificationService)
    {
        $request->validate([
            'code' =>  ['required', 'string'],
        ]);

        $ipificationService->validateCode($request->code);
    }

    /**
     * Send verification OTP via SMS
     */
    public function sendOTP(OTPService $otpService): JsonResponse
    {
        $otpService->sendSMSForUser(Auth::user());

        return new JsonResponse([ 'message' => 'Sent successfully']);
    }

    /**
     * Confirm SMS verification OTP
     *
     * @bodyParam code string required
     */
    public function confirmOTP(Request $request, OTPService $otpService): MeResource
    {
        $request->validate([
            'code' =>  ['required', 'string'],
        ]);

        /** @var User $user */
        $user = Auth::user();

        $otpService->confirmPhoneForUser($user, $request->code);

        $user->refresh();

        return new MeResource($user);
    }

    /**
     * Send verification email
     */
    public function sendVerificationEmail(EmailVerificationService $emailVerificationService): JsonResponse
    {
        $emailVerificationService->sendVerificationEmailForUser(Auth::user());

        return new JsonResponse([ 'message' => 'Sent successfully' ]);
    }
}
