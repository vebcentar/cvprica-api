<?php

namespace App\Http\Controllers\Api\V2;

use App\Exceptions\SafeException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\AdListRequest;
use App\Http\Requests\Company\AdRequest;
use App\Http\Requests\Company\AdSharedInfoListRequest;
use App\Http\Resources\AdCollection;
use App\Http\Resources\AdResource;
use App\Models\Ad;
use App\Models\AdSharedInfo;
use App\Models\User;
use App\Repositories\AdRepository;
use App\Repositories\AdSharedInfoRepository;
use App\Services\AdService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Candidate\ApplicationResource;

/**
 * @group V2
 * @authenticated
 */
class CompanyAdController extends Controller
{
    /**
     * Company Ads list
     *
     * @urlParam is_active boolean Filter by ad is active status (0|1). Example: 1
     * @urlParam is_archived boolean Filter by ad is archived status (0|1). Example: 1
     * @urlParam status integer Filter by ad status (from 0 to 6). Example: 1
     * @urlParam is_ready_to_publish boolean Filter by application is ready to publish status (0|1). Example: 1
     * @urlParam city_id integer Filter by ad city id. Example: 22
     * @urlParam country_id integer Filter by ad country id. Example: 1
     * @urlParam type_of_work_id integer Filter by ad type of work. Example: 16
     * @urlParam term string Filter by ad title or description. Example: 'react'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     */
    public function jobsList(AdListRequest $request, AdRepository $adRepository): AdCollection
    {
        return $adRepository->filterListForCompanyUser(
            Auth::id(),
            $request
        );
    }

    /**
     * Archive company ad(s)
     *
     * @bodyParam ids.* integer Array of ids to archive
     */
    public function archive(Request $request, AdRepository $adRepository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $adRepository->archive(Auth::id(), $payload['ids']);

        return response()->json([]);
    }

    /**
     * Delete company ad(s)
     *
     * @bodyParam ids.* integer Array of ids to delete
     */
    public function delete(Request $request, AdRepository $adRepository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $adRepository->delete(Auth::id(), $payload['ids']);

        return response()->json([]);
    }

    /**
     * Unarchive company ad(s)
     *
     * @bodyParam ids.* integer Array of ids to return from archive
     */
    public function unarchive(Request $request, AdRepository $adRepository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $adRepository->unarchive(Auth::id(), $payload['ids']);

        return response()->json([]);
    }

    /**
     * Company Ad applications list
     *
     * @urlParam is_selected boolean Filter by application is selected status (0|1). Example:
     * @urlParam is_eminent boolean Filter by application is eminent (0|1). Example: 1
     * @urlParam city_id integer Filter by applicant's city id. Example: 22
     * @urlParam born_before_year integer Filter by applicant's year of birth (inclusive). Example: 1985
     * @urlParam born_after_year integer Filter by applicant's year of birth (inclusive). Example: 1980
     * @urlParam gender_id integer Filter by applicant's gender (1|2). Example: 1
     * @urlParam education_level_id integer Filter by applicant's education level. Example: 3
     * @urlParam experience boolean Filter by applicant's experience. Example: 1
     * @urlParam term string Filter by applicant's full name, email or phone. Example: 'nikola'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     */
    public function applications(int $adId, AdSharedInfoListRequest $request, AdSharedInfoRepository $repository)
    {
        if (!$ad = Ad::find($adId)) {
            return response()->json([ 'message' => 'Not Found.'], 404);
        }

        if (Auth::id() !== $ad->user_id) {
            return response()->json([ 'message' => 'Forbidden.'], 403);
        }
        return $repository->filterApplicationsForAd($adId, $request);
    }

    /**
     * Company Ad single application (w/o ad id)
     *
     * @urlParam applicationId integer Id of an application. Example: 1
     *
     * NOTE: Needed to remove adId because of structure od notifications
     */
    public function singleApplication($applicationId)
    {
        if (!$application = AdSharedInfo::find($applicationId)) {
            return response()->json([ 'message' => 'Application not Found.'], 404);
        }

        if (Auth::id() !== $application->ad->user_id) {
            return response()->json([ 'message' => 'Forbidden.'], 403);
        }

        return new ApplicationResource($application);
    }

    /**
     * Company Ad single application
     *
     * @urlParam adId integer Id of Ad. Example: 1
     * @urlParam applicationId integer Id of an application. Example: 1
     */
    public function application(int $adId, int $applicationId)
    {
        if (!$ad = Ad::find($adId)) {
            return response()->json([ 'message' => 'Ad not Found.'], 404);
        }

        if (!$application = AdSharedInfo::find($applicationId)) {
            return response()->json([ 'message' => 'Application not Found.'], 404);
        }

        if (Auth::id() !== $ad->user_id) {
            return response()->json([ 'message' => 'Forbidden.'], 403);
        }

        if ($application->ad_id !== $ad->id) {
            return response()->json([ 'message' => 'Application is not for this Ad.'], 400);
        }

        return new ApplicationResource($application);
    }

    /**
     * Select application(s).
     *
     * @bodyParam ids.* integer Array of ids of ad_shared_info to select
     */
    public function selectApplications(int $adId, Request $request, AdSharedInfoRepository $repository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\AdSharedInfo,id'],
        ]);

        if (!$ad = Ad::find($adId)) {
            return response()->json([ 'message' => 'Not Found.'], 404);
        }

        if (Auth::id() !== $ad->user_id) {
            return response()->json([ 'message' => 'Forbidden.'], 403);
        }

        $repository->selectApplications($adId, $payload['ids']);

        return response()->json([]);
    }

    /**
     * Deselect application(s).
     *
     * @bodyParam ids.* integer Array of ids of ad_shared_info to deselect
     */
    public function deselectApplications(Request $request, AdSharedInfoRepository $repository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\AdSharedInfo,id'],
        ]);

        $applications = AdSharedInfo::whereIn('id', $payload['ids'])->with('ad')->get();

        if (0 === count($applications)) {
            return response()->json([ 'message' => 'Not Found.'], 404);
        }

        foreach ($applications as $application) {
            if ($application->ad->user_id !== Auth::id()) {
                return response()->json([ 'message' => 'Forbidden. You have no rights for ad '.$application->ad_id ], 403);
            }
        }

        $repository->deselectApplications($payload['ids']);

        return response()->json([]);
    }

    /**
     * Company selected applications list
     *
     * @urlParam is_eminent boolean Filter by application is eminent (0|1). Example: 1
     * @urlParam city_id integer Filter by applicant's city id. Example: 22
     * @urlParam born_before_year integer Filter by applicant's year of birth (inclusive). Example: 1985
     * @urlParam born_after_year integer Filter by applicant's year of birth (inclusive). Example: 1980
     * @urlParam gender_id integer Filter by applicant's gender (1|2). Example: 1
     * @urlParam education_level_id integer Filter by applicant's education level. Example: 3
     * @urlParam experience boolean Filter by applicant's experience. Example: 1
     * @urlParam term string Filter by applicant's full name, email or phone. Example: 'nikola'
     * @urlParam offset integer Offset specifies the number of rows to skip before starting to return rows from the query. Default: 0 Example: 1
     * @urlParam limit integer Limit on the number of results. Default: 20 Example: 10
     */
    public function selectedApplications(AdSharedInfoListRequest $request, AdSharedInfoRepository $repository)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->isCompany()) {
            return response()->json([ 'message' => 'Forbidden.'], 403);
        }

        $request->merge(['is_selected' => 1]);

        return $repository->filterApplicationsForCompany($user->id, $request);
    }

    /**
     * Show application(s).
     *
     * @bodyParam ids.* integer Array of ids to show
     */
    public function showJobs(Request $request, AdRepository $adRepository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $adRepository->show(Auth::id(), $payload['ids']);

        return response()->json([]);
    }

    /**
     * Hide application(s).
     *
     * @bodyParam ids.* integer Array of ids to hide
     */
    public function hideJobs(Request $request, AdRepository $adRepository): JsonResponse
    {
        $payload = $request->validate([
            'ids'    =>  ['required', 'array'],
            'ids.*'  =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
        ]);

        $adRepository->hide(Auth::id(), $payload['ids']);

        return response()->json([]);
    }

    /**
     * Company - Application set eminent
     *
     * @return App\Http\Resources\Candidate\ApplicationResource
     */

    public function applicationSetEminent($adId, $id, Request $request)
    {
        $validated = $request->validate([
            'is_eminent'    =>  ['nullable', 'boolean'],
        ]);
        $ad = Ad::findOrFail($adId);
        $application = $ad->applications()->findOrFail($id);
        $application->setEminent($request->is_eminent);
        return (new ApplicationResource($application))
            ->additional([
                'message' => 'The application successfully updated.',
            ])
            ->response()
            ->setStatusCode(\Illuminate\Http\Response::HTTP_ACCEPTED);
    }

    /**
     * Create ad
     *
     * @bodyParam title string required Title of the ad. Maximum 190 characters
     * @bodyParam country_id integer Country id for the ad. Example: 1
     * @bodyParam city_id integer City id for the ad. Example: 22
     * @bodyParam type_of_work_id integer Type of work id for the ad. Example: 2
     * @bodyParam description string Description for teh ad. Example: 'Opis'
     * @bodyParam start_date string Start date of the ad. Example: 2021-01-01
     * @bodyParam end_date string Date when ad expires. Example: 2021-01-01
     * @bodyParam ads_questions string Stringified json of an array of string questions. Example: "[\"question 1?\",\"question 2?\",\"question 3?\"]"
     * @bodyParam short_description string Short description of an ad.
     * @bodyParam is_active boolean Active status for the ad. Can be set to active only if all the necessary fields are filled. Default: 0 Example: 1
     * @bodyParam job_type_id integer Job type id of the ad. Example: 1
     * @bodyParam work_time_id integer Work time id of the ad. Example: 1
     * @bodyParam education_level_id integer Education level id asked of candidates. Example: 6
     */
    public function store(AdRequest $request, AdService $adService)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->isCompany()) {
            abort(403, 'Forbidden.');
        }

        if (!$user->isPhoneAndEmailVerified() && $request->is_active) {
            abort(403, 'Must be a verified user.');
        }

        try {
            $ad = $adService->create($user, $request);

            return new AdResource($ad);
        } catch (SafeException $ex) {
            return response()->json([ 'message' => $ex->getMessage()], 400);
        }
    }

    /**
     * Update ad
     *
     * @urlParam adId integer required Id of the ad to update
     * @bodyParam title string required Title of the ad. Maximum 190 characters
     * @bodyParam country_id integer Country id for the ad. Example: 1
     * @bodyParam city_id integer City id for the ad. Example: 22
     * @bodyParam type_of_work_id integer Type of work id for the ad. Example: 2
     * @bodyParam description string Description for teh ad. Example: 'Opis'
     * @bodyParam start_date string Start date of the ad. Example: 2021-01-01
     * @bodyParam end_date string Date when ad expires. Example: 2021-01-01
     * @bodyParam ads_questions string Stringified json of an array of string questions. Example: "[\"question 1?\",\"question 2?\",\"question 3?\"]"
     * @bodyParam short_description string Short description of an ad.
     * @bodyParam is_active boolean Active status for the ad. Can be set to active only if all the necessary fields are filled. Default: 0 Example: 1
     * @bodyParam job_type_id integer Job type id of the ad. Example: 1
     * @bodyParam work_time_id integer Work time id of the ad. Example: 1
     * @bodyParam education_level_id integer Education level id asked of candidates. Example: 6
     */
    public function update($adId, AdRequest $request, AdService $adService)
    {
        $ad = Ad::findOrFail($adId);

        /** @var User $company */
        $company = Auth::user();

        if ($company->id !== $ad->company->id) {
            abort(403, 'Forbidden.');
        }

        if (!$company->isPhoneAndEmailVerified() && $request->is_active) {
            abort(403, 'Must have a verified email and phone.');
        }

        try {
            $adService->update($ad, $request);

            return new AdResource($ad);
        } catch (SafeException $ex) {
            return response()->json([ 'message' => $ex->getMessage()], 400);
        }
    }
}
