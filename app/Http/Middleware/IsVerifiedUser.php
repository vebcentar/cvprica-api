<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsVerifiedUser
{
    /**
     * Is verified user
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        if (!$user = Auth::user()) {
            return response()->json(['message' => 'Unauthenticated.'],401);
        }

        if ($user->isAdmin() || $user->isPhoneAndEmailVerified()) {
            return $next($request);
        }

        if (request()->wantsJson()){
            return response()->json(['message' => 'Forbidden.'],403);
        }

        Auth::logout();
        return redirect()->back();
    }
}
