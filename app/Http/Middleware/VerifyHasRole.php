<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyHasRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        /** @var User $user */
        $user = Auth::user();

        foreach ($roles as $role) {
            if ($user->hasRole(trim($role))) {
                return $next($request);
            }
        }

        if (request()->wantsJson()){
            return response()->json(['message' => 'Forbidden.'],403);
        }

        Auth::logout();
        return redirect()->back();
    }
}
