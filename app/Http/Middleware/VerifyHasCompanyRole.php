<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyHasCompanyRole
{
    /**
     * Verify has company role
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->isCompany()) {
            return $next($request);
        }

        if (request()->wantsJson()){
            return response()->json(['message' => 'Forbidden.'],403);
        }

        Auth::logout();
        return redirect()->back();
    }
}
