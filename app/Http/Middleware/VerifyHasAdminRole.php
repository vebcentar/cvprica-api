<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyHasAdminRole
{
    /**
     * Verify has admin role
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->isAdmin()) {
            return $next($request);
        }

        if (request()->wantsJson()){
            return response()->json(['message' => 'Forbidden.'],403);
        }

        Auth::logout();
        return redirect()->back();
    }
}
