<?php

namespace App\Http\Resources;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'ad_question_id' => $this->ad_question->id,
            'ad_question_text'  =>  $this->ad_question->text_question,
            'text_answer' => $this->text_answer,
            'video_answer' => new VideoResource($this->video),
        ];
    }
}
