<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ForeignLanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'user_id'   => $this->user_id,
            'languages_id'   => $this->languages_id,
            'languages_name'   => $this->language->name,
            'language_reads_id'   => $this->language_reads_id,
            'language_reads_name'   => $this->language_read->name,
            'language_writes_id'   => $this->language_writes_id,
            'language_writes_name'   => $this->language_write->name,
            'language_speaks_id'   => $this->language_speaks_id,
            'language_speaks_name'   => $this->language_speak->name,
        ];
        return parent::toArray($request);
    }
}
