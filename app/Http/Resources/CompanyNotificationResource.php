<?php

namespace App\Http\Resources;

use App\Http\Resources\Company\CompanyPublicInfoResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyNotificationResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'    =>  $this->id,
            'user_id'    =>  $this->user_id,
            'company_id'    => $this->company->id,
            'company_name'    => $this->company->full_name,
            'created_at'    =>  $this->created_at,
            'updated_at'    =>  $this->updated_at,
        ];
    }
}
