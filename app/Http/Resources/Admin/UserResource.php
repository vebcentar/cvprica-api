<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\CvVideoResource;
use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;


class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'role'    =>  $this->role,
            'full_name'    =>  $this->full_name,
            'email'    =>  $this->email,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'phone'    =>  $this->phone,
            'birth_year'    =>  $this->birth_year,
            'aditional_info'    =>  $this->aditional_info,
            'gender_id'    =>  $this->gender_id,
            'is_active'    =>  $this->is_active,
            'company_activity'    =>  $this->company_activity,
            'company_description'    =>  $this->company_description,
            'company_video' => MediaService::buildUrlForMedia($this->company_video),
            'video' => new CvVideoResource($this->cv_video),
            'number_of_employees_from' =>  $this->number_of_employees_from,
            'number_of_employees_to' =>  $this->number_of_employees_to,
            'pib'    =>  $this->pib,
            'pdv'    =>  $this->pdv,
            'package_id'    =>  $this->package_id,
            'country_id'    =>  $this->country_id,
            'country'    =>  new CountryResource($this->country),
            'city_id'    =>  $this->city_id,
            'city'    =>  new CityResource($this->city),
            'address'    =>  $this->address,
            'zip_code'    =>  $this->zip_code,
            'created_at'    =>  $this->created_at,
            'updated_at'    =>  $this->updated_at,
            'created_by'    =>  $this->created_by,
            'is_archived'    =>  $this->is_archived,
            'is_hidden'    =>  $this->is_hidden,
            'education_level_id'    =>  $this->education_level_id,
            'turn_notification'    =>  $this->turn_notification,
            'website'    =>  $this->website,
            'facebook'    =>  $this->facebook,
            'language_id'    =>  $this->language_id,
            'eminent'    =>  $this->eminent,
            'linkedin'    =>  $this->linkedin,
            'background_image' =>  MediaService::buildUrlForMedia($this->background_image),
            'google_id'    =>  $this->google_id,
            'apple_id'    =>  $this->apple_id,
            'facebook_id'    =>  $this->facebook_id,
            'linkedin_id'    =>  $this->linkedin_id,
            'contact_email'    =>  $this->contact_email,
            'selection_time'    =>  $this->selection_time,
            'selection_type'    =>  $this->selection_type,
            'selection_circles'    =>  $this->selection_circles,
            'priority'    =>  $this->priority,
        ];
    }
}
