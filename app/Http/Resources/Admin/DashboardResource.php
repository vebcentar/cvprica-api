<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
// Models
use App\Models\User;
use App\Models\Ad;
use App\Http\Resources\Company\InfoResource as CompanyResource;

class DashboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $new_companies = User::where('role', 'company')->orderByDesc('id')->limit(5)->get();

        $top_companies = User::where('role', 'company')->orderByDesc('id')->limit(5)->get();

        $users_count = User::count();

        $companies_count = User::where('role', 'company')->where('is_archived', 0)->count();

        $candidates_count = User::where('role', 'employee')->where('is_archived', 0)->count();

        $ads_count = Ad::count();

        for($i=1; $i<=12; $i++)
        {
            $ads_statistics_per_mouth[] = Ad::whereYear('created_at', '=', $request->year)->whereMonth('created_at', '=', $i)->count();
        }

        return [
            'user'   =>  [
                'full_name' =>  $this->full_name,
            ],
            'users_count'   =>  $users_count,
            'companies_count'   =>  $companies_count,
            'candidates_count'   =>  $candidates_count,
            'ads_count'   =>  $ads_count,
            'ads_statistics_per_mouth'  =>  $ads_statistics_per_mouth,
            'new_companies'    =>  CompanyResource::collection($new_companies),
            'top_companies'    =>  CompanyResource::collection($top_companies),
        ];
    }
}
