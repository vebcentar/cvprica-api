<?php

namespace App\Http\Resources;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;

class CvVideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        $data['video'] = MediaService::buildUrlForMedia($data['video']);

        return $data;
    }
}
