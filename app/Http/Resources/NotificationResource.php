<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'user_id'    => $this->user_id,
            'title'    =>   $this->title,
            'text'    =>    $this->text,
            'seen'    =>    $this->seen,
            'type'    =>    $this->type,
            'particular_id'    =>   $this->particular_id,
            'created_at'    =>   $this->created_at,
        ];
    }
}
