<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CityNotificationCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => CityNotificationResource::collection($this->collection),
        ];
    }
}
