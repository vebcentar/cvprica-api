<?php

namespace App\Http\Resources;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        if ($data['document_link']) {
            $data['document_link'] = MediaService::buildUrlForMedia($data['document_link']);
        }

        return $data;
    }
}
