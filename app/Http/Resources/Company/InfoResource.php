<?php

namespace App\Http\Resources\Company;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\TypeOfWorkNotificationResource;
use App\Http\Resources\CityNotificationResource;
use App\Http\Resources\WorkExperienceResource;
use App\Http\Resources\EducationResource;
use App\Http\Resources\EducationLevelResource;
use App\Http\Resources\CvVideoResource;
use App\Http\Resources\DriversLicenceResource;
use App\Http\Resources\UserDocumentResource;
use App\Http\Resources\ForeignLanguageResource;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\MessageResource;

class InfoResource extends JsonResource
{
    public function toArray($request): array
    {
        $data = [
            'id'    => $this->id,
            'full_name' =>  $this->full_name,
            'gender'    =>  $this->gender,
            'birth_year'    =>  $this->birth_year,
            'email' => $this->email,
            'contact_email' => $this->contact_email,
            'country'   =>   new CountryResource($this->country),
            'city'   =>  new CityResource($this->city),
            'address'   =>  $this->address,
            'phone'   =>  $this->phone,
            'zip_code'   =>  $this->zip_code,
            'ui_language'  => new LanguageResource($this->uiLanguage),
            'turn_notification' => $this->turn_notification,
            'city_notifications'    => CityNotificationResource::collection($this->city_notifications),
            'company_activities'    =>  $this->company_activities,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'video'  => new CvVideoResource($this->cv_video),
            'aditional_info' => $this->aditional_info,
            'gender_id' =>  $this->gender_id,
            'is_active' =>  $this->is_active,
            'is_eminent' =>  $this->eminent,
            'company_activity' =>  $this->company_activity,
            'company_description' =>  $this->company_description,
            'company_video' => MediaService::buildUrlForMedia($this->company_video),
            'number_of_employees_from' =>  $this->number_of_employees_from,
            'number_of_employees_to' =>  $this->number_of_employees_to,
            'pib' =>  $this->pib,
            'pdv' =>  $this->pdv,
            'package' =>  $this->package,
            'is_archived' =>  $this->is_archived,
            'facebook' =>  $this->facebook,
            'instagram' =>  $this->instagram,
            'linkedin' =>  $this->linkedin,
            'website' =>  $this->website,
            'background_image' =>  MediaService::buildUrlForMedia($this->background_image),
            'selection_time' =>  $this->selection_time,
            'selection_type' =>  $this->selection_type,
            'selection_circles' =>  $this->selection_circles,
            'profile_completely' => $this->profile_completely,
            'created_at'    =>  $this->created_at,
            'company_users' => $this->company_users,
        ];

        if ($request->user() && $request->user()->id === $this->id) {
            $data['notifications'] =  NotificationResource::collection($this->notifications->take(5));
            $data['messages']  = MessageResource::collection($this->messages->take(5));
        }

        return $data;
    }
}
