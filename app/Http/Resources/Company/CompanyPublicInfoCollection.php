<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyPublicInfoCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => CompanyPublicInfoResource::collection($this->collection),
        ];
    }
}
