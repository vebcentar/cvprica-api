<?php

namespace App\Http\Resources\Company;

use App\Repositories\AdSharedInfoRepository;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\AdSharedInfo;
use App\Http\Resources\AdResource;
use App\Http\Resources\Candidate\InfoResource as CandidateResource;
use Illuminate\Support\Facades\App;

class DashboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var User $user */
        $user = $request->user();
        $all_active_ads = $this->ads()
            ->where('is_active', 1)
            ->where('is_archived', 0)
            ->where('end_date', '>=', date('Y-m-d'));

        $active_ads = $this->ads()
            ->where('is_active', 1)
            ->where('is_archived', 0)
            ->where('end_date', '>=', date('Y-m-d'))
            ->orderByDesc('id')
            ->limit(5)->get();

        $ads_applications_count = AdSharedInfo::whereIn('ad_id',  $all_active_ads->pluck('id')->toArray())->where('applied', 1)->count();

        $ads_count = $this->ads()->count();

        $selectedApplicationsCount = AdSharedInfo::query()
            ->join('ads', 'ads.id', '=', 'ad_shared_infos.ad_id')
            ->where('ads.user_id', $user->id)
            ->where('ads.end_date', '>=', date('Y-m-d'))
            ->where('ad_shared_infos.applied', 1)
            ->where('ad_shared_infos.selected', 1)
            ->count()
        ;

        for($i=1; $i<=12; $i++)
        {
            $applications_statistics_per_mouth[] = AdSharedInfo::whereIn('ad_id', $all_active_ads->pluck('id')->toArray())->where('applied', 1)->whereYear('created_at', '=', $request->year)->whereMonth('created_at', '=', $i)->count();
        }

        $new_cadidates = User::where('role', 'employee')->limit(5)->get();

        /** @var AdSharedInfoRepository $applicationsRepository */
        $applicationsRepository = App::make(AdSharedInfoRepository::class);

        $candidatesEminentCount = $applicationsRepository->applicationsForCompanyQueryBuilder($this->id)->where('selected', 1)->count();

        $ads_applications_eminent_count = AdSharedInfo::whereIn('ad_id',  $all_active_ads->pluck('id')->toArray())->where('applied', 1)->where('eminent', 1)->count();

        $lastAd = $active_ads->first();

        $lastAdApplicationsCount = $lastAd ? $lastAd->shared_adds()->where('applied', 1)->count() : 0;

        return [
            'company'   =>  [
                'full_name' =>  $this->full_name,
                'active_ads'    =>  AdResource::collection($active_ads),
                'ads_applications_count' =>  $ads_applications_count,
                'last_ad_applications_count' => $lastAdApplicationsCount,
                'selected_applications_count' => $selectedApplicationsCount,
                'candidates_eminent_count' => $candidatesEminentCount,
                'ads_applications_eminent_count' => $ads_applications_eminent_count,
                'applications_statistics_per_mouth' =>  $applications_statistics_per_mouth,
                'ads_count' =>  $ads_count,
                'profile_completely' => $user->profile_completely,
            ],
            'new_candidates'    =>  CandidateResource::collection($new_cadidates),
        ];
    }
}
