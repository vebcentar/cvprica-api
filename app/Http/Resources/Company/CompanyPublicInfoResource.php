<?php

namespace App\Http\Resources\Company;

use App\Http\Resources\PublicAdResourceWithoutCompany;
use App\Models\User;
use App\Services\MediaService;
use App\Services\NotificationService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\CvVideoResource;
use Illuminate\Support\Str;

class CompanyPublicInfoResource extends JsonResource
{
    public function toArray($request): array
    {
        $data = [
            'id' => $this->id,
            'full_name' =>  $this->full_name,
            'slug' => Str::slug($this->full_name),
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'background_image' =>  MediaService::buildUrlForMedia($this->background_image),
            'company_description' =>  $this->company_description,
            'video' => new CvVideoResource($this->cv_video),
            'company_video' => MediaService::buildUrlForMedia($this->company_video),
            'country'   =>   new CountryResource($this->country),
            'city'   =>  new CityResource($this->city),
            'address'   =>  $this->address,
            'zip_code'   =>  $this->zip_code,
            'contact_email'   =>  $this->contact_email,
            'facebook' =>  $this->facebook,
            'instagram' =>  $this->instagram,
            'linkedin' =>  $this->linkedin,
            'website' =>  $this->website,
            'pib' =>  $this->pib,
            'pdv' =>  $this->pdv,
            'number_of_employees_from' =>  $this->number_of_employees_from,
            'number_of_employees_to' =>  $this->number_of_employees_to,
            'active_ads_count' => $this->activeAds()->count(),
            'latest_active_ad' => new PublicAdResourceWithoutCompany($this->latestActiveAd)
        ];

        /** @var User $user */
        $user = $request->user('api');

        if ($user && $user->isCandidate()) {
            $data['subscribed'] = app()->make(NotificationService::class)->isUserSubscribedToCompany($user->id, $this->id) ? 1 : 0;
        }

        return $data;
    }
}
