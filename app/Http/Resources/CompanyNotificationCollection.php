<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyNotificationCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => CompanyNotificationResource::collection($this->collection),
        ];
    }
}
