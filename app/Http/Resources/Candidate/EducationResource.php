<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'user_id'    =>  $this->user_id,
            'education_level_id'    =>  $this->education_level_id,
            'education_level'    =>  $this->education_level,
            'education_area_id'    =>  $this->education_area_id,
            'education_area'    =>  $this->education_area,
            'title'    =>  $this->title,
            'institution'    =>  $this->institution,
            'course'    =>  $this->course,
            'city'    =>  $this->city,
            'start_date'    =>  $this->start_date,
            'end_date'    =>  $this->end_date,
            'ongoing'    =>  $this->ongoing
        ];
    }
}
