<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'user_id'    =>  $this->user_id,
            'languages_id'    =>  $this->languages_id,
            'languages'    =>  $this->languages,
            'language_reads_id'    =>  $this->language_reads_id,
            'language_reads'    =>  $this->language_reads,
            'language_writes_id'    =>  $this->language_writes_id,
            'language_writes'    =>  $this->language_writes,
            'language_speaks_id'    =>  $this->language_speaks_id,
            'language_speaks'    =>  $this->language_speaks,
        ];
        return parent::toArray($request);
    }
}
