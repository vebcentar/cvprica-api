<?php

namespace App\Http\Resources\Candidate;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Candidate\AdResource;

class AplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'ad_id'    =>  $this->ad_id,
            'ad_title'    =>  $this->ad->title,
            'ad_company_full_name'    =>  $this->ad->type_of_work ? $this->ad->company->full_name : null,
            //'ad_position'    =>  $this->ad->position,
            'ad_type_of_work_name'   =>  $this->ad->type_of_work ? $this->ad->type_of_work->name : null,
            'user_id'    =>  $this->user_id,
            'reminder'    =>  $this->reminder,
            'favourite'    =>  $this->favourite,
            'applied'    =>  $this->applied,
            'selected'    =>  $this->selected,
            'created_at'    =>  $this->created_at,
            'updated_at'    =>  $this->updated_at,
            'video'    =>  $this->video,
            'viewed'    =>  $this->viewed,
            'cv'    =>  $this->cv,
            'full_name'    =>  $this->full_name,
            'email'    =>  $this->email,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'phone'    =>  $this->phone,
            'seen'      =>  $this->seen,
            'eminent'    =>  $this->eminent,
        ];
    }
}
