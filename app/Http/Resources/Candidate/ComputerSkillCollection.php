<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ComputerSkillCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => ComputerSkillResource::collection($this->collection),
        ];
    }
}
