<?php

namespace App\Http\Resources\Candidate;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AdResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\AnswerResource;
use App\Models\AdAnswer;

class ApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $answer = AdAnswer::whereIn('ad_question_id', $this->ad->questions()->pluck('id')->toArray())->where('user_id', $this->user_id)->get();
        return [
            'id'    =>  $this->id,
            'ad_id'    =>  $this->ad_id,
            'ad'    =>  new AdResource($this->ad),
            'user_id'    =>  $this->user_id,
            'user'    =>  new UserResource($this->user),
            'reminder'    =>  $this->reminder,
            'favourite'    =>  $this->favourite,
            'applied'    =>  $this->applied,
            'selected'    =>  $this->selected,
            'created_at'    =>  $this->created_at,
            'updated_at'    =>  $this->updated_at,
            'created_by'    =>  $this->created_by,
            'modified_by'    =>  $this->modified_by,
            'deleted_at'    =>  $this->deleted_at,
            'deleted_by'    =>  $this->deleted_by,
            'seen'    =>  $this->seen,
            'viewed'    =>  $this->viewed,
            'cv_video'    =>  MediaService::buildUrlForMedia($this->video),
            'cv_document'    =>  MediaService::buildUrlForMedia($this->cv),
            'full_name'    =>  $this->full_name,
            'email'    =>  $this->email,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'phone'    =>  $this->phone,
            'education'    =>  $this->education,
            'languages'    =>  $this->languages,
            'computer_skills'    =>  $this->computer_skills,
            'experience'    =>  $this->experience,
            'additional_information'    =>  $this->additional_information,
            'gender_id'    =>  $this->gender_id,
            'country_id'    =>  $this->country_id,
            'city_id'    =>  $this->city_id,
            'city'    =>  $this->city,
            'status'    =>  $this->status,
            'eminent'    =>  $this->eminent,
            'answers'   =>  AnswerResource::collection($answer),
        ];
    }
}
