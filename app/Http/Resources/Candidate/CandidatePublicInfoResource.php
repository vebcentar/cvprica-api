<?php

namespace App\Http\Resources\Candidate;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\WorkExperienceResource;
use App\Http\Resources\EducationResource;
use App\Http\Resources\EducationLevelResource;
use App\Http\Resources\DriversLicenceResource;
use App\Http\Resources\ForeignLanguageResource;

class CandidatePublicInfoResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'    => $this->id,
            'full_name' =>  $this->full_name,
            'gender'    =>  $this->gender,
            'birth_year' =>  $this->birth_year,
            'country' =>   new CountryResource($this->country),
            'city' =>  new CityResource($this->city),
            'address' =>  $this->address,
            'phone' =>  $this->phone,
            'zip_code' =>  $this->zip_code,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'computer_skills' => ComputerSkillResource::collection($this->computer_skills),
            'work_experiences' => WorkExperienceResource::collection($this->work_experiences),
            'aditional_info' => $this->aditional_info,
            'educations' =>  EducationResource::collection($this->educations),
            'education_level' => new EducationLevelResource($this->education_level),
            'languages' =>  ForeignLanguageResource::collection($this->languages),
            'driver_licences' => DriversLicenceResource::collection($this->driver_licences),
            'created_at' =>  $this->created_at
        ];
    }
}
