<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Candidate\ApplicationResource;

class ApplicationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => ApplicationResource::collection($this->collection),
        ];
    }
}
