<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WorkExperienceCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => WorkExperienceResource::collection($this->collection),
        ];
    }
}
