<?php

namespace App\Http\Resources\Candidate;

use App\Http\Resources\CvVideoResource;
use App\Http\Resources\PublicAdResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\Ad;
use DB;

class DashboardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $user */
        $user = User::withCount(['documents', 'applications' => function ($query) {
            $query->where('applied', 1);
        }])->with(['cv_video', 'applications'])
        ->findOrFail($this->user()->id);

        $adsForMe = Ad::query()->with('city', 'country', 'company')->where('is_active', true)->where('is_archived', false)->where('end_date', '>=', date('Y-m-d'))
        ->when($request->city_id, function($query) use ($request) {
            return $query->where('city_id', $request->city_id);
        })->when($request->type_of_work_id, function($query) use ($request) {
            return $query->where('type_of_work_id', $request->type_of_work_id);
        })->when($request->company_name, function($query) use ($request) {
            return $query->whereHas('creator', function($query) use ($request) {
                return $query->where('full_name', 'like',  "%{$request->company_name}%");
            });
        })->when($request->country_id, function($query) use ($request) {
            return $query->where('country_id', $request->country_id);
        })->orderByDesc('created_at')->limit(5)->get();

        $applications_statistics_per_mouth = [];

        for($i=1; $i<=12; $i++)
        {
            $applications_statistics_per_mouth[] = $user->applications()->where('applied', 1)->whereYear('created_at', '=', $request->year)->whereMonth('created_at', '=', $i)->count();
        }

        $adsForMeArray = [];

        foreach ($adsForMe as $adForMe) {
            $adsForMeArray[] = new PublicAdResource($adForMe);
        }

        $applications = $user->applications()->where('applied', 1)->get();

        return [
            'user'    =>  [
                'full_name' =>  $user->full_name,
                'email' => $user->email,
                'cv_video'  => new CvVideoResource($user->cv_video),
                'documents_count' =>  $user->documents_count,
                'applications' => AplicationResource::collection($applications),
                'applications_count' => count($applications),
                'applications_statistics_per_mouth' => $applications_statistics_per_mouth,
                'profile_completely' => $user->profile_completely,
                'is_email_and_phone_verified' => $user->isPhoneAndEmailVerified() ? 1 : 0
            ],
            'ads_for_me' => $adsForMeArray,
        ];
    }
}
