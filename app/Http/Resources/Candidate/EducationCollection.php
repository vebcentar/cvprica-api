<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EducationCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => EducationResource::collection($this->collection),
        ];
    }
}
