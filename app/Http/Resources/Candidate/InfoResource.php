<?php

namespace App\Http\Resources\Candidate;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\TypeOfWorkNotificationResource;
use App\Http\Resources\CityNotificationResource;
use App\Http\Resources\Candidate\ComputerSkillResource;
use App\Http\Resources\WorkExperienceResource;
use App\Http\Resources\EducationResource;
use App\Http\Resources\EducationLevelResource;
use App\Http\Resources\CvVideoResource;
use App\Http\Resources\DriversLicenceResource;
use App\Http\Resources\Candidate\AplicationResource;
use App\Http\Resources\UserDocumentResource;
use App\Http\Resources\ForeignLanguageResource;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\MessageResource;

class InfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'full_name' =>  $this->full_name,
            'gender'    =>  $this->gender,
            'birth_year'    =>  $this->birth_year,
            'email' => $this->email,
            'contact_email' => $this->contact_email,
            'country'   =>   new CountryResource($this->country),
            'city'   =>  new CityResource($this->city),
            'address'   =>  $this->address,
            'phone'   =>  $this->phone,
            'zip_code'   =>  $this->zip_code,
            'ui_language'  => new LanguageResource($this->uiLanguage),
            'turn_notification' => $this->turn_notification,
            'city_notifications'    => CityNotificationResource::collection($this->city_notifications),
            'type_of_work_notifications'    =>  TypeOfWorkNotificationResource::collection($this->type_of_work_notifications),
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'computer_skills'   => ComputerSkillResource::collection($this->computer_skills),
            'work_experiences'  => WorkExperienceResource::collection($this->work_experiences),
            'cv_video'  => new CvVideoResource($this->cv_video),
            'cv_document'  => new UserDocumentResource($this->cv_document->first()),
            'aditional_info' => $this->aditional_info,
            'documents'  => UserDocumentResource::collection($this->documents),
            'documents_count' =>  $this->documents_count,
            'educations'    =>  EducationResource::collection($this->educations),
            'education_level'  => new EducationLevelResource($this->education_level),
            'languages' =>  ForeignLanguageResource::collection($this->languages),
            'applications' => AplicationResource::collection($this->applications),
            'applications_count' =>  $this->applications_count,
            'profile_completely' => $this->profile_completely,
            'driver_licences' => DriversLicenceResource::collection($this->driver_licences),
            'created_at'    =>  $this->created_at,
            'notifications' => NotificationResource::collection($this->notifications->take(5)),
            'messages'  => MessageResource::collection($this->messages->take(5)),
        ];
    }
}
