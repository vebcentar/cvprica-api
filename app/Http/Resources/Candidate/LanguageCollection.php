<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LanguageCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => LanguageResource::collection($this->collection),
        ];
    }
}
