<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\TypeOfWorkResource;
use App\Http\Resources\WorkTimeResource;
use App\Http\Resources\EducationLevelResource;
use Illuminate\Support\Str;

class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  =>  $this->id,
            'user_id'   =>  $this->user_id,
            'company'   =>  $this->user,
            'title' =>  $this->title,
            'slug' => Str::slug($this->title),
            'description' =>  $this->description,
            'location' =>  $this->location,
            'video' =>  $this->video,
            'document' =>  $this->document,
            'number_of_applications' =>  $this->number_of_applications,
            'start_date' =>  $this->start_date,
            'end_date' =>  $this->end_date,
            'is_active' =>  $this->is_active,
            'is_archived' =>  $this->is_archived,
            'city' =>  new CityResource($this->city),
            'country' =>  new CountryResource($this->country),
            'position'  =>  $this->position,
            'type_of_work' =>  new TypeOfWorkResource($this->type_of_work),
            'work_time' =>  new WorkTimeResource($this->work_time),
            'education_level' =>  new EducationLevelResource($this->education_level),
            'short_description' =>  $this->short_description,
            'visible' =>  $this->visible,
            'published' =>  $this->published,
            'status' =>  $this->status,
            'ready_to_publish' =>  $this->ready_to_publish,
            'is_hidden' =>  $this->is_hidden,
        ];
    }
}
