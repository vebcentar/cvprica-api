<?php

namespace App\Http\Resources\Candidate;

use Illuminate\Http\Resources\Json\JsonResource;

class ComputerSkillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'user_id'    =>  $this->user_id,
            'computer_skill_name_id'    =>  $this->computer_skill_name_id,
            'computer_skill_name'    =>  $this->computer_skill_name,
            'computer_skill_knowledge_level_id'    =>  $this->computer_skill_knowledge_level_id,
            'computer_skill_knowledge_level'    =>  $this->computer_skill_knowledge_level,
        ];
    }
}
