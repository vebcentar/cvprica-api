<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DriversLicenceCategoryResource;

class DriversLicenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'drivers_licence_category_id'   =>  $this->drivers_licence_category_id,
            'drivers_licence_category'  => new DriversLicenceCategoryResource($this->driversLicenceCategory),
            'own_vehicle'   =>  $this->own_vehicle,
            'additional_info'   =>  $this->additional_info,
        ];
    }
}
