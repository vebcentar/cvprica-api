<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PublicAdCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => PublicAdResource::collection($this->collection),
        ];
    }
}
