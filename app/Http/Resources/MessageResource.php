<?php

namespace App\Http\Resources;

use App\Http\Resources\Candidate\CandidatePublicInfoResource;
use App\Http\Resources\Company\CompanyPublicInfoResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'type' => $request->user()->id === $this->user_id ? 'incoming' : 'outgoing',
            'from' => $this->buildUserResource($this->from),
            'to' => $this->buildUserResource($this->to),
            'title' => $this->title,
            'text' => $this->text,
            'seen' => $this->seen,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ];
    }

    private function buildUserResource($resource): JsonResource
    {
        return $resource->isCompany() ?
            new CompanyPublicInfoResource($resource) :
            new CandidatePublicInfoResource($resource);
    }
}
