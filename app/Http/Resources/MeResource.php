<?php

namespace App\Http\Resources;

use App\Models\Message;

class MeResource extends UserResource
{
    public function toArray($request): array
    {
        $data = parent::toArray($request);

        $unreadMessagesCount = Message::query()->where('user_id', $this->id)->whereNull('seen')->count();

        $additionalData = [
            'is_email_verified' => \is_null($this->email_verified_at) ? 0 : 1,
            'is_phone_verified' => \is_null($this->phone_verified_at) ? 0 : 1,
            'notifications' => NotificationResource::collection($this->notifications->take(5)),
            'messages' => MessageResource::collection($this->messages->take(5)),
            'unread_messages_count' => $unreadMessagesCount
        ];

        return array_merge($data, $additionalData);
    }
}
