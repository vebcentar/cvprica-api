<?php

namespace App\Http\Resources;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\TypeOfWorkNotificationResource;
use App\Http\Resources\CityNotificationResource;
use App\Http\Resources\Candidate\ComputerSkillResource;
use App\Http\Resources\WorkExperienceResource;
use App\Http\Resources\EducationResource;
use App\Http\Resources\EducationLevelResource;
use App\Http\Resources\CvVideoResource;
use App\Http\Resources\DriversLicenceResource;
use App\Http\Resources\Candidate\AplicationResource;
use App\Http\Resources\UserDocumentResource;
use App\Http\Resources\ForeignLanguageResource;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\MessageResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [];

        if ($this->role === 'admin') {
            $data = [
                'id'    => $this->id,
                'full_name' =>  $this->full_name,
                'gender'    =>  $this->gender,
                'birth_year'    =>  $this->birth_year,
                'email' => $this->email,
                'contact_email' => $this->contact_email,
                'country'   =>   new CountryResource($this->country),
                'city'   =>  new CityResource($this->city),
                'address'   =>  $this->address,
                'phone'   =>  $this->phone,
                'zip_code'   =>  $this->zip_code,
            ];
        } elseif ($this->role === 'company') {
            $data = [
            'id'    => $this->id,
            'full_name' =>  $this->full_name,
            'gender'    =>  $this->gender,
            'birth_year'    =>  $this->birth_year,
            'email' => $this->email,
            'contact_email' => $this->contact_email,
            'company_users' => $this->company_users,
            'country'   =>   new CountryResource($this->country),
            'city'   =>  new CityResource($this->city),
            'address'   =>  $this->address,
            'phone'   =>  $this->phone,
            'zip_code'   =>  $this->zip_code,
            'ui_language'  => new LanguageResource($this->uiLanguage),
            'turn_notification' => $this->turn_notification,
            'city_notifications'    => CityNotificationResource::collection($this->city_notifications),
            'company_activities'    =>  $this->company_activities,
            'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
            'video'  => new CvVideoResource($this->cv_video),
            'aditional_info' => $this->aditional_info,
            'gender_id' =>  $this->gender_id,
            'is_active' =>  $this->is_active,
            'company_activity' =>  $this->company_activity,
            'company_description' =>  $this->company_description,
            'company_video' => MediaService::buildUrlForMedia($this->company_video),
            'number_of_employees_from' =>  $this->number_of_employees_from,
            'number_of_employees_to' =>  $this->number_of_employees_to,
            'pib' =>  $this->pib,
            'pdv' =>  $this->pdv,
            'package' =>  $this->package,
            'current_package' => 'Besplatan paket',
            'is_archived' =>  $this->is_archived,
            'facebook' =>  $this->facebook,
            'instagram' =>  $this->instagram,
            'linkedin' =>  $this->linkedin,
            'website' =>  $this->website,
            'background_image' =>  MediaService::buildUrlForMedia($this->background_image),
            'selection_time' =>  $this->selection_time,
            'selection_type' =>  $this->selection_type,
            'selection_circles' =>  $this->selection_circles,
            'profile_completely' => $this->profile_completely,
            'created_at'    =>  $this->created_at,
            'notifications' => NotificationResource::collection($this->notifications->take(5)),
            'messages'  => MessageResource::collection($this->messages->take(5)),
            ];
        } elseif ($this->role === 'employee') {
            $data = [
                'id'    => $this->id,
                'full_name' =>  $this->full_name,
                'gender'    =>  $this->gender,
                'birth_year'    =>  $this->birth_year,
                'email' => $this->email,
                'contact_email' => $this->contact_email,
                'country'   =>   new CountryResource($this->country),
                'city'   =>  new CityResource($this->city),
                'address'   =>  $this->address,
                'phone'   =>  $this->phone,
                'zip_code'   =>  $this->zip_code,
                'ui_language'  => new LanguageResource($this->uiLanguage),
                'turn_notification' => $this->turn_notification,
                'city_notifications'    => CityNotificationResource::collection($this->city_notifications),
                'type_of_work_notifications'    =>  TypeOfWorkNotificationResource::collection($this->type_of_work_notifications),
                'profile_image' => MediaService::buildUrlForMedia($this->profile_image),
                'computer_skills'   => ComputerSkillResource::collection($this->computer_skills),
                'work_experiences'  => WorkExperienceResource::collection($this->work_experiences),
                'cv_video'  => new CvVideoResource($this->cv_video),
                'cv_document'  => new UserDocumentResource($this->cv_document->first()),
                'aditional_info' => $this->aditional_info,
                'documents'  => UserDocumentResource::collection($this->documents),
                'documents_count' =>  $this->documents_count,
                'educations'    =>  EducationResource::collection($this->educations),
                'education_level'  => new EducationLevelResource($this->education_level),
                'languages' =>  ForeignLanguageResource::collection($this->languages),
                'profile_completely' => $this->profile_completely,
                'driver_licences' => DriversLicenceResource::collection($this->driver_licences),
                'created_at'    =>  $this->created_at,
            ];
        }

        if ($this->role) {
            $data['role'] = $this->role;
        }

        return $data;
    }
}
