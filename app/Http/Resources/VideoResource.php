<?php

namespace App\Http\Resources;

use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => MediaService::buildUrlForMedia($this->path),
            'video_length' => $this->video_length,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ];
    }
}
