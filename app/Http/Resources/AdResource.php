<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Services\MediaService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Company\InfoResource;
use Illuminate\Support\Str;

class AdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id'    => $this->id,
            'user_id'    => $this->user_id,
            'company'    => new InfoResource($this->user),
            'title'    => $this->title,
            'slug' => Str::slug($this->title),
            'description'    => $this->description,
            'location'    => $this->location,
            'video'    => MediaService::buildUrlForMedia($this->video),
            'document'    => $this->document,
            'number_of_applications'    => $this->number_of_applications,
            'start_date'    => $this->start_date,
            'end_date'    => $this->end_date,
            'is_active'    => $this->is_active,
            'is_archived'    => $this->is_archived,
            'city_id'    => $this->city_id,
            'city'    => $this->city,
            'country_id'    => $this->country_id,
            'country'    => $this->country,
            'type_of_work_id'    => $this->type_of_work_id,
            'type_of_work'  =>  $this->type_of_work,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'created_by'    => $this->created_by,
            'modified_by'    => $this->modified_by,
            'deleted_at'    => $this->deleted_at,
            'deleted_by'    => $this->deleted_by,
            'position'    => $this->position,
            'job_type_id'    => $this->job_type_id,
            'work_time_id'    => $this->work_time_id,
            'education_level_id'    => $this->education_level_id,
            'short_description'    => $this->short_description,
            'visible'    => $this->visible,
            'published'    => $this->published,
            'status'    => $this->status,
            'ready_to_publish'    => $this->ready_to_publish,
            'is_hidden' =>  $this->is_hidden,
            'questions' =>  $this->questions,
        ];

        /** @var User $user */
        $user = $request->user('api');

        if ($user && $user->isCandidate()) {
            $data['favorite'] = $this->favorites()->where('user_id', $user->id)->exists() ? 1 : 0;

            $application = $this->applications()->where('user_id', $user->id)->first();
            $data['application_id'] = $application ? $application->id : null;
            $data['applied'] = $application ? $application->applied : null;
        }

        return $data;
    }
}
