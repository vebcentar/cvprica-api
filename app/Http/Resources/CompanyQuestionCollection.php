<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyQuestionCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => CompanyQuestionResource::collection($this->collection),
        ];
    }
}
