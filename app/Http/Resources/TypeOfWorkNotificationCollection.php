<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TypeOfWorkNotificationCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'data' => TypeOfWorkNotificationResource::collection($this->collection),
        ];
    }
}
