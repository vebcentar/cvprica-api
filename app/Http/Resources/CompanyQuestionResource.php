<?php

namespace App\Http\Resources;

use App\Http\Resources\Company\InfoResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyQuestionResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'company' => new InfoResource($this->company),
            'question' => $this->question,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'modified_by' => $this->modified_by,
        ];
    }
}
