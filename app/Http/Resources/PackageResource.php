<?php

namespace App\Http\Resources;

use App\Models\Package;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'benefits' => $this->benefits,
            'pricing' => $this->pricing,
            'discounts' => Package::buildDiscounts(),
            'active' =>  $this->active
        ];
    }
}
