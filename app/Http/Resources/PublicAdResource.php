<?php

namespace App\Http\Resources;

use App\Http\Resources\Company\CompanyPublicInfoResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PublicAdResource extends JsonResource
{
    public function toArray($request): array
    {
        $data = [
            'id'    => $this->id,
            'user_id'    => $this->user_id,
            'company'    => new CompanyPublicInfoResource($this->user),
            'title'    => $this->title,
            'slug' => Str::slug($this->title),
            'description'    => $this->description,
            'location'    => $this->location,
            'video'    => $this->video,
            'document'    => $this->document,
            'number_of_applications'    => $this->number_of_applications,
            'start_date'    => $this->start_date,
            'end_date'    => $this->end_date,
            'is_active'    => $this->is_active,
            'is_archived'    => $this->is_archived,
            'city_id'    => $this->city_id,
            'city'    => $this->city,
            'country_id'    => $this->country_id,
            'country'    => $this->country,
            'type_of_work_id'    => $this->type_of_work_id,
            'type_of_work'  =>  $this->type_of_work,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'created_by'    => $this->created_by,
            'modified_by'    => $this->modified_by,
            'deleted_at'    => $this->deleted_at,
            'deleted_by'    => $this->deleted_by,
            'position'    => $this->position,
            'job_type_id'    => $this->job_type_id,
            'work_time_id'    => $this->work_time_id,
            'education_level_id'    => $this->education_level_id,
            'short_description'    => $this->short_description,
            'visible'    => $this->visible,
            'published'    => $this->published,
            'status'    => $this->status,
            'ready_to_publish'    => $this->ready_to_publish,
            'is_hidden' =>  $this->is_hidden,
            'questions' =>  $this->questions,
            'favorite' => 0,
            'applied' => 0
        ];

        /** @var User $user */
        $user = $request->user('api');

        if ($user && $user->isCandidate()) {
            $data['favorite'] = $this->favorites()->where('user_id', $user->id)->exists() ? 1 : 0;
            $data['application_id'] = $this->applications()->where('user_id', $user->id)->pluck('id')->first();
        }

        return $data;
    }
}
