<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'confirmed'], // Password must be confirmed (password_confirmation)
            'role' => ['required', 'string', 'in:employee,company'],
            'full_name' => ['required', 'string'],
            'country_id' => ['required', 'integer', 'exists:App\Models\Country,id'],
            'language_id' => ['nullable', 'integer', 'exists:App\Models\UserLanguage,id'],
        ];
    }
}
