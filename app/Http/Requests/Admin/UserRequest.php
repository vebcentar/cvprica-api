<?php

namespace App\Http\Requests\Admin;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->user() && $this->user()->role === 'admin')
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            {
                return [
                    'role'  =>  ['nullable', 'string'],
                    'is_active'  =>  ['nullable', 'boolean'],
                    'is_archived'  =>  ['nullable', 'boolean'],
                    'term'  =>  ['nullable', 'string'],
                    'birth_year_min'  =>  ['nullable', 'date_format:Y'],
                    'birth_year_max'  =>  ['nullable', 'date_format:Y'],
                    'country_id'  =>  ['nullable', 'integer', 'exists:App\Models\Country,id'],
                    'city_id'  =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
                    'education_level_id'  =>  ['nullable', 'integer', 'exists:App\Models\EducationLevel,id'],
                    'gender_id'  =>  ['nullable', 'integer', 'exists:App\Models\Gender,id'],
                    'eminent'  =>  ['nullable', 'boolean'],
                    'order_by' => [ 'string', 'in:id,full_name,created_at,email' ],
                    'order_dir' => [ 'string', 'in:asc,desc' ],
                ];
            }
            case 'DELETE':
            case 'POST':
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'role'  =>  ['nullable', 'string'],
                    'full_name'  =>  ['nullable', 'string'],
                    'email '  =>  ['nullable', 'string', 'email'],
                    'password'  =>  ['nullable', 'string', 'confirmed', 'min:8'],
                    'phone' =>  User::phoneRulesArray(true),
                    'aditional_info'  =>  ['nullable', 'string'],
                    'gender_id' =>  ['required', 'integer', 'exists:App\Models\Gender,id'],
                    'birth_year' =>  ['required', 'date'],
                    'country_id'  =>  ['nullable', 'integer', 'exists:App\Models\Country,id'],
                    'city_id'  =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
                    'education_level_id'  =>  ['nullable', 'integer', 'exists:App\Models\EducationLevel,id'],
                    'eminent'  =>  ['nullable', 'boolean'],
                ];
            }
            default:break;
        }
    }
}
