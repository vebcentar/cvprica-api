<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->user() && $this->user()->role === 'admin')
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            {
                return [
                    'user_id'    =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'is_active' =>  ['nullable', 'boolean'],
                    'is_archived'   =>  ['nullable', 'boolean'],
                ];
            }
            case 'DELETE':
            case 'POST':
            {
                return [
                    'user_id'    =>  ['required', 'integer', 'exists:App\Models\User,id'],
                    'title' =>  ['required', 'string'],
                    'description'   =>  ['nullable', 'string'],
                    // 'location'   =>  ['required', 'string'], POTREBNO IZBRISATI POLJE
                    // 'video'    =>  ['required', 'string'],   POTREBNO IZBRISATI POLJE
                    // 'document'   =>  ['required', 'string'], POTREBNO IZBRISATI POLJE
                    'start_date'    =>  ['nullable', 'date'],
                    'end_date'  =>  ['required_unless:ongoing,1', 'date'],
                    'is_active'   =>  ['nullable', 'boolean'],  
                    'is_archived'   =>  ['nullable', 'boolean'],
                    'city_id'   =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
                    'country_id'   =>  ['nullable', 'integer', 'exists:App\Models\Country,id'],
                    'type_of_work_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'position'   =>  ['nullable', 'string'],
                    'job_type_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'work_time_id'   =>  ['nullable', 'integer', 'exists:App\Models\WorkTime,id'],
                    // 'education_level_id'   =>  ['required', 'integer', 'exists:App\Models\EducationLevel,id'],  POTREBNO IZBRISATI POLJE
                    'short_description'   =>  ['nullable', 'string'],
                    'visible'   =>  ['nullable', 'boolean'],
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'user_id'    =>  ['required', 'integer', 'exists:App\Models\User,id'],
                    'title' =>  ['required', 'string'],
                    'description'   =>  ['nullable', 'string'],
                    // 'location'   =>  ['required', 'string'], POTREBNO IZBRISATI POLJE
                    // 'video'    =>  ['required', 'string'],   POTREBNO IZBRISATI POLJE
                    // 'document'   =>  ['required', 'string'], POTREBNO IZBRISATI POLJE
                    'start_date'    =>  ['nullable', 'date'],
                    'end_date'  =>  ['required_unless:ongoing,1', 'date'],
                    'is_active'   =>  ['nullable', 'boolean'],  
                    'is_archived'   =>  ['nullable', 'boolean'],
                    'city_id'   =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
                    'country_id'   =>  ['nullable', 'integer', 'exists:App\Models\Country,id'],
                    'type_of_work_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'position'   =>  ['nullable', 'string'],
                    'job_type_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'work_time_id'   =>  ['nullable', 'integer', 'exists:App\Models\WorkTime,id'],
                    // 'education_level_id'   =>  ['required', 'integer', 'exists:App\Models\EducationLevel,id'],  POTREBNO IZBRISATI POLJE
                    'short_description'   =>  ['nullable', 'string'],
                    'visible'   =>  ['nullable', 'boolean'],
                ];
            }
            default:break;
        }
    }
}
