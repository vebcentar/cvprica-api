<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id'    =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'is_active' =>  ['nullable', 'boolean'],
            'is_archived'   =>  ['nullable', 'boolean'],
            'is_expired'   =>  ['nullable', 'boolean'],
            'city_id'   =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
            'type_of_work_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
            'term' => [ 'nullable', 'string', 'max:200' ],
            'limit' => ['numeric', 'max:100', 'min:5'],
            'offset' => ['numeric', 'min:0'],
            'status' => ['numeric', 'min:0', 'max:6'],
            'order_by' => [ 'string', 'in:id,title,created_at' ],
            'order_dir' => [ 'string', 'in:asc,desc' ],
        ];
    }
}
