<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Company\AdRequest as CompanyAdRequest;
use App\Models\User;

class CreateAdRequest extends CompanyAdRequest
{
    public function authorize(): bool
    {
        /** @var User $user */
        if (!$user = $this->user()) {
            return false;
        }

        return $user->isAdmin();
    }

    public function rules(): array
    {
        $rules = parent::rules();
        $rules['user_id'] = 'required|integer|exists:App\Models\User,id';

        return $rules;
    }
}
