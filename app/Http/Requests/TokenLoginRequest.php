<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class TokenLoginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => [ 'required', 'string' ],
            'role'  =>  [ 'required', 'in:'.User::ROLE_CANDIDATE.','.User::ROLE_COMPANY ],
        ];
    }
}
