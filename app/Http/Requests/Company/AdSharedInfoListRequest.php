<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class AdSharedInfoListRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'is_selected' => [ 'boolean' ],
            'limit' => [ 'numeric', 'max:1000', 'min:5' ],
            'offset' => [ 'numeric', 'min:0' ],
            'term' => [ 'string', 'max:100' ],
            'born_before_year' => [ 'numeric' ],
            'born_after_year' => [ 'numeric' ],
            'city_id' => [ 'numeric', 'exists:App\Models\City,id' ],
            'education_level_id' => [ 'numeric', 'exists:App\Models\EducationLevel,id'],
            'experience' => [ 'boolean'],
            'gender_id' => [ 'numeric', 'exists:App\Models\Gender,id' ],
            'is_eminent' => ['nullable', 'boolean'],
        ];
    }
}
