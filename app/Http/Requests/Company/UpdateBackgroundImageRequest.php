<?php

namespace App\Http\Requests\Company;

use App\Services\MediaService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBackgroundImageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['nullable', 'integer', 'exists:App\Models\User,id'],
            'image' => MediaService::imageRequestRuleArray(),
        ];
    }
}
