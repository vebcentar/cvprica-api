<?php

namespace App\Http\Requests\Company;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        /** @var User $user */
        if (!$user = $this->user()) {
            return false;
        }

        return $user->isCompany() || $user->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'   =>  ['sometimes', 'integer', 'exists:App\Models\User,id'],
            'full_name' =>  ['sometimes', 'string'],
            'email' =>  ['required', 'email'],
            'password'  =>  ['sometimes', 'string'],
            'company_activity' =>  ['sometimes', 'string'],
            'company_description' =>  ['sometimes', 'string'],
            'facebook' =>  ['sometimes', 'url'],
            'website' =>  ['sometimes', 'url'],
            'instagram' =>  ['sometimes', 'url'],
            'country_id' =>  ['sometimes', 'integer', 'exists:App\Models\Country,id'],
            'city_id' =>  ['sometimes', 'integer', 'exists:App\Models\City,id'],
            'address' =>  ['nullable', 'sometimes', 'string'],
            'pib' =>  ['sometimes', 'string'],
            'pdv' =>  ['sometimes', 'string'],
            'contact_mail' =>  ['sometimes', 'email'],
            'contact_person' =>  ['sometimes', 'string'],
            'contact_person_position' =>  ['sometimes', 'string'],
            'contact_phone' =>  'sometimes|'.implode('|', User::phoneRulesArray()),
            'contact_website' =>  ['sometimes', 'string',  'url'],
            'owner' =>  ['sometimes', 'boolean'],
            'number_of_employees_from' =>  ['sometimes', 'integer'],
            'number_of_employees_to' =>  ['sometimes', 'integer', 'gte:number_of_employees_from'],
            'turn_notification' => ['sometimes', 'boolean'],
            'language_id'   => ['sometimes', 'integer', 'exists:App\Models\Language,id'],
        ];
    }

    protected function prepareForValidation()
    {
        foreach (self::urlFields() as $urlField) {
            if ($this->{$urlField} && false === strpos($this->{$urlField}, 'http')) {
                $this->merge([
                    $urlField => 'https://'.$this->{$urlField}
                ]);
            }
        }
    }

    private static function urlFields(): array
    {
        return [
            'website',
            'facebook',
            'instagram'
        ];
    }
}
