<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class DashboardRequest extends FormRequest
{
    public function rules()
    {
        return [
            'year'  =>  ['required', 'date_format:Y'],
        ];
    }
}
