<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:190',
            'country_id' => 'nullable|int|exists:App\Models\Country,id',
            'city_id' => 'nullable|int|exists:App\Models\City,id',
            'type_of_work_id' => 'nullable|int|exists:App\Models\TypeOfWork,id',
            'description' => 'nullable|string|max:4000',
            'start_date' => 'nullable',
            'end_date' => 'nullable',
            'ads_questions' => 'nullable|json',
            'short_description' => 'nullable|string|max:150',
            'is_active' => 'nullable|boolean',
            'job_type_id' => 'nullable|integer|exists:App\Models\JobType,id',
            'work_time_id' => 'nullable|integer|exists:App\Models\WorkTime,id',
            'education_level_id' => 'nullable|integer|exists:App\Models\EducationLevel,id'
        ];
    }
}
