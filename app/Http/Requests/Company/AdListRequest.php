<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class AdListRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'is_active' => ['nullable', 'boolean'],
            'is_ready_to_publish' => [ 'boolean' ],
            'is_archived' => ['nullable', 'boolean'],
            'limit' => ['numeric', 'max:1000', 'min:5'],
            'offset' => ['numeric', 'min:0'],
            'term' => ['string', 'max:100'],
            'city_id' => ['numeric', 'exists:App\Models\City,id'],
            'country_id' => ['numeric', 'exists:App\Models\Country,id'],
            'type_of_work_id' => ['numeric', 'exists:App\Models\TypeOfWork,id'],
            'status' => ['numeric', 'min:0', 'max:6'],
        ];
    }
}
