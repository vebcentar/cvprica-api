<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class JobSearchRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'limit' => [ 'numeric', 'max:100', 'min:5' ],
            'offset' => [ 'numeric', 'min:0' ],
            'term' => [ 'string', 'max:100' ],
            'city_id' => [ 'numeric', 'exists:App\Models\City,id' ],
            'country_id' => [ 'numeric', 'exists:App\Models\Country,id' ],
            'types_of_work' => [ 'array' ],
            'types_of_work.*' => [ 'numeric', 'exists:App\Models\TypeOfWork,id' ],
            'job_type_id' => [ 'numeric', 'exists:App\Models\JobType,id' ],
            'education_level_id' => [ 'numeric', 'exists:App\Models\EducationLevel,id' ],
            'work_time_id' => [ 'numeric', 'exists:App\Models\WorkTime,id' ],
            'companies' =>  [ 'array' ],
            'companies.*'  =>  [ 'integer', 'exists:App\Models\User,id' ],
            'created_before'  =>  [ 'integer', 'min:0' ],
            'status'  =>  [ 'integer', 'in:1,6' ],
        ];
    }
}
