<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password'  =>  ['required', 'string'],
            // 'password' => ['required_without_all:google_id,facebook_id,apple_id,linkedin_id', 'string'],
            // 'google_id' => ['nullable', 'string'],
            // 'facebook_id' => ['nullable', 'string'],
            // 'apple_id' => ['nullable', 'string'],
            // 'linkedin_id' => ['nullable', 'string'],
        ];
    }
}
