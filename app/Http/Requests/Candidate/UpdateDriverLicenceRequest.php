<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDriverLicenceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'drivers_licence_category_id' =>  ['required', 'integer', 'exists:App\Models\DriversLicenceCategory,id'],
            'own_vehicle' =>  ['nullable', 'boolean'],
            'additional_info' =>  ['nullable', 'string'],
        ];
    }
}
