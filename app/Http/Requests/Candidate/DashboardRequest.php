<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @queryParam year integer required Number year. Example: 2021
 * @queryParam city_id integer The id of the city. Example: 1
 * @queryParam type_of_work_id integer The id of the Type of work. Example: 1
 * @queryParam company_name string Company name. Example: Web Centar
 */
class DashboardRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'year'  =>  ['required', 'date_format:Y'],
            'city_id'   =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
            'type_of_work_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
            'country_id'   =>  ['nullable', 'integer', 'exists:App\Models\Country,id'],
            'company_name'   =>  ['nullable', 'string']
        ];
    }
}
