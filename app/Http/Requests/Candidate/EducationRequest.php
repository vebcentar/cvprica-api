<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    public function rules(): array
    {
        switch ($this->method()) {
            case 'DELETE':
            case 'PUT':
            case 'GET':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                ];
            }
            case 'PATCH':
            case 'POST':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'education_level_id'    =>  ['required', 'integer', 'exists:App\Models\EducationLevel,id'],
                    'education_area_id' =>  ['required', 'integer', 'exists:App\Models\EducationArea,id'],
                    'title'   =>  ['required', 'string'],
                    'institution'   =>  ['required', 'string'],
                    'course'    =>  ['required', 'string'],
                    'city'   =>  ['required', 'string'],
                    'start_date'    =>  ['required', 'date'],
                    'end_date'  =>  ['required_unless:ongoing,1', 'date'],
                    'ongoing'   =>  ['nullable', 'boolean'],
                ];
            }
        }

        return [];
    }
}
