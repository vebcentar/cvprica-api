<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateFieldsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'email' =>  ['nullable', 'string', 'email', "unique:users,email,".Auth::id() ?? 0],
            'full_name' =>  ['nullable', 'string'],
            'aditional_info' =>  ['nullable', 'string'],
            'password' => ['nullable', 'string'],
            'old_password' => ['nullable', 'string'],
            'language_id' => ['required', 'integer', 'exists:App\Models\UserLanguage,id'],
            'turn_notification' => ['nullable', 'boolean'],
            'city_notifications'    =>  ['nullable', 'array'],
            'city_notifications_ids.*'  =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
            'type_of_work_notifications_ids'    =>  ['nullable', 'array'],
            'type_of_work_notifications_ids.*'  =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
        ];
    }
}
