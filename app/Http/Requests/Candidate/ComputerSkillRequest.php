<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class ComputerSkillRequest extends FormRequest
{
    public function rules(): array
    {
        switch ($this->method()) {
            case 'DELETE':
            case 'PUT':
            case 'GET':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                ];
            }
            case 'PATCH':
            case 'POST':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'computer_skill_name_id'    =>  ['required', 'integer', 'exists:App\Models\ComputerSkillName,id'],
                    'computer_skill_knowledge_level_id'    =>  ['required', 'integer', 'exists:App\Models\ComputerSkillKnowledgeLevel,id'],
                ];
            }
        }

        return [];
    }
}
