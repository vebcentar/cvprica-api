<?php

namespace App\Http\Requests\Candidate;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
            'full_name' =>  ['sometimes', 'string'],
            'gender_id' =>  ['sometimes', 'integer', 'exists:App\Models\Gender,id'],
            'birth_year' =>  ['sometimes', 'date'],
            'education_level_id' =>  ['sometimes', 'integer', 'exists:App\Models\EducationLevel,id'],
            'email' =>  ['required', 'email'],
            'country_id' =>  ['sometimes', 'integer', 'exists:App\Models\Country,id'],
            'city_id' =>  ['sometimes', 'integer', 'exists:App\Models\City,id'],
            'address' =>  ['nullable', 'sometimes', 'string'],
            'phone' => 'sometimes|'.implode('|', User::phoneRulesArray()),
            'zip_code' =>  ['nullable', 'sometimes', 'integer', 'regex:/\b\d{5,8}\b/'],
        ];
    }
}
