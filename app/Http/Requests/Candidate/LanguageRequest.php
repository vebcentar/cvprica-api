<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class LanguageRequest extends FormRequest
{
    public function rules(): array
    {
        switch ($this->method()) {
            case 'DELETE':
            case 'PUT':
            case 'GET':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                ];
            }
            case 'PATCH':
            case 'POST':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'languages_id'    =>  ['required', 'integer', 'exists:App\Models\Language,id'],
                    'language_reads_id' =>  ['required', 'integer', 'exists:App\Models\LanguageRead,id'],
                    'language_writes_id'   =>  ['required', 'integer', 'exists:App\Models\LanguageWrite,id'],
                    'language_speaks_id'   =>  ['required', 'integer', 'exists:App\Models\LanguageSpeak,id'],
                ];
            }
        }

        return [];
    }
}
