<?php

namespace App\Http\Requests\Candidate;

use App\Services\MediaService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileImageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'image' =>  MediaService::imageRequestRuleArray(),
        ];
    }
}
