<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCvDocumentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'document' =>  [ 'required', 'file', 'mimes:pdf,doc,dot,docx,odt', 'max:8192' ],
        ];
    }
}
