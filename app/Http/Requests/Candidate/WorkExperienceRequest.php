<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class WorkExperienceRequest extends FormRequest
{
    public function rules(): array
    {
        switch ($this->method()) {
            case 'DELETE':
            case 'PUT':
            case 'GET':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                ];
            }
            case 'PATCH':
            case 'POST':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'company_name'    =>  ['required', 'string'],
                    'location' =>  ['required', 'string'],
                    'position'   =>  ['required', 'string'],
                    'type_of_work_id'    =>  ['required', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'start_date'    =>  ['required', 'date'],
                    'end_date'  =>  ['required_unless:ongoing,1', 'date'],
                    'ongoing'   =>  ['nullable', 'boolean'],
                    'description'   =>  ['nullable', 'string'],
                ];
            }
        }

        return [];
    }
}
