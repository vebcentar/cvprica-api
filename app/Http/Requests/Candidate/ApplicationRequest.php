<?php

namespace App\Http\Requests\Candidate;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationRequest extends FormRequest
{
    public function rules(): array
    {
        switch ($this->method()) {
            case 'GET':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'type'      =>  ['nullable', 'string', 'in:draft,send'],
                    'type_of_work_id'   =>  ['nullable', 'integer', 'exists:App\Models\TypeOfWork,id'],
                    'city_id'   =>  ['nullable', 'integer', 'exists:App\Models\City,id'],
                    'term'   =>  ['nullable', 'string'],
                    'seen'  =>  ['nullable', 'boolean'],
                ];
            }
            case 'PUT':
            case 'DELETE':
            {
                return [
                    'user_id'   =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                ];
            }
            case 'POST':
            {
                return [
                    'user_id'    =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'ad_id'   =>  ['required', 'integer', 'exists:App\Models\Ad,id'],
                    'applied'   =>  ['required', 'boolean'],
                    'video'   =>  ['nullable', 'string', 'url'],
                    'answers'   =>  ['nullable', 'array'],
                    'answers.*'   =>  ['nullable', 'array'],
                    'answers.*.ad_question_id'   =>  ['nullable', 'integer', 'exists:App\Models\AdQuestion,id'],
                    'answers.*.video_id'   =>  ['nullable', 'string', 'exists:App\Models\Video,id'],
                ];
            }
            case 'PATCH':
            {
                return [
                    'user_id'    =>  ['nullable', 'integer', 'exists:App\Models\User,id'],
                    'applied'   =>  ['required', 'boolean'],
                    'video'   =>  ['nullable', 'string', 'url'],
                    'answers'   =>  ['nullable', 'array'],
                    'answers.*'   =>  ['nullable', 'array'],
                    'answers.*.ad_question_id'   =>  ['nullable', 'integer', 'exists:App\Models\AdQuestion,id'],
                    'answers.*.text_answer'   =>  ['nullable', 'string'],
                    'answers.*.video_answer'   =>  ['nullable', 'string'],
                ];
            }
        }

        return [];
    }
}
