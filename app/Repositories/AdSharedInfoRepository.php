<?php

namespace App\Repositories;

use App\Http\Requests\Candidate\ApplicationRequest;
use App\Http\Requests\Company\AdSharedInfoListRequest;
use App\Http\Resources\Candidate\ApplicationCollection;
use App\Models\AdSharedInfo;
use Illuminate\Database\Eloquent\Builder;

class AdSharedInfoRepository
{
    const DEFAULT_LIST_LIMIT = 20;

    public function filterApplicationsForAd(int $adId, AdSharedInfoListRequest $request): ApplicationCollection
    {
        $qb = AdSharedInfo::query()
            ->select('ad_shared_infos.*')
            ->where('ad_shared_infos.ad_id', $adId)
            ->join('users AS applicant', 'applicant.id', '=', 'ad_shared_infos.user_id')
            ->where('ad_shared_infos.applied', 1)
        ;

        $this->buildConditionsFromAdSharedInfoListRequest($qb, $request);

        $total = $qb->count();

        $applications = $qb
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? self::DEFAULT_LIST_LIMIT)
            ->orderBy('ad_shared_infos.id', 'DESC')
            ->get()
        ;

        return (new ApplicationCollection($applications))->additional(['meta' => [ 'total' => $total ]]);
    }

    public function applicationsForCompanyQueryBuilder(int $companyId): Builder
    {
        return AdSharedInfo::query()
            ->select('company.*', 'ads.*', 'ad_shared_infos.*')
            ->with('user')
            ->join('users AS applicant', 'applicant.id', '=', 'ad_shared_infos.user_id')
            ->join('ads', 'ads.id', '=', 'ad_shared_infos.ad_id')
            ->join('users AS company', 'company.id', '=', 'ads.user_id')
            ->where('company.id', $companyId)
            ->where('ad_shared_infos.applied', 1)
        ;
    }

    public function filterApplicationsForCompany(int $companyId, AdSharedInfoListRequest $request): ApplicationCollection
    {
        $qb = $this->applicationsForCompanyQueryBuilder($companyId);

        $this->buildConditionsFromAdSharedInfoListRequest($qb, $request);

        $total = $qb->count();

        $applications = $qb
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? self::DEFAULT_LIST_LIMIT)
            ->orderBy('ad_shared_infos.id', 'DESC')
            ->get()
        ;

        return (new ApplicationCollection($applications))->additional(['meta' => [ 'total' => $total ]]);
    }

    public function filterApplicationsForCandidate(int $candidateId, ApplicationRequest $request): array
    {
        $qb = AdSharedInfo::query()
            ->select('company.*', 'ads.*', 'ad_shared_infos.*', 'applicant.*')
            ->with('user')
            ->join('users AS applicant', 'applicant.id', '=', 'ad_shared_infos.user_id')
            ->join('ads', 'ads.id', '=', 'ad_shared_infos.ad_id')
            ->join('users AS company', 'company.id', '=', 'ads.user_id')
            ->where('ad_shared_infos.user_id', $candidateId)
        ;

        if ($request->has('type')) {
            $qb->where('ad_shared_infos.applied', 'send' === $request->type ? 1 : 0);
        }

        if ($request->has('term')) {
            $qb->where(function (Builder $qb) use ($request) {
                $qb
                    ->where('company.full_name', 'like', "%{$request->term}%")
                    ->orWhere('ads.title', 'like', "%{$request->term}%")
                    ->orWhere('ads.description', 'like', "%{$request->term}%");
            });
        }

        if ($request->has('seen')) {
            $qb->where('ad_shared_infos.seen', $request->seen);
        }

        if ($request->has('city_id')) {
            $qb->where('ads.city_id', $request->city_id);
        }

        if ($request->has('type_of_work_id')) {
            $qb->where('ads.type_of_work_id', $request->type_of_work_id);
        }

        $total = $qb->count();

        $applications = $qb
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? self::DEFAULT_LIST_LIMIT)
            ->orderBy('ad_shared_infos.id', 'DESC')
            ->get()
        ;

        return [
            'applications' => $applications,
            'total' => $total
        ];
    }

    /**
     * Select applications; Returns affected rows;
     */
    public function selectApplications(int $adId, array $ids): int
    {
        return AdSharedInfo::query()
            ->with('user')
            ->where('applied', 1)
            ->whereIn('id', $ids)
            ->update([
                'selected' => 1
            ])
        ;
    }

    /**
     * Deselect applications; Returns affected rows;
     */
    public function deselectApplications(array $ids): int
    {
        return AdSharedInfo::query()
            ->with('user')
            ->where('applied', 1)
            ->whereIn('id', $ids)
            ->update([
                'selected' => 0
            ])
        ;
    }

    private function buildConditionsFromAdSharedInfoListRequest(Builder $qb, AdSharedInfoListRequest $request): void
    {
        if ($request->has('is_eminent')) {
            $qb->where('ad_shared_infos.eminent', $request->is_eminent);
        }

        if ($request->has('is_selected')) {
            $qb->where('ad_shared_infos.selected', $request->is_selected);
        }

        if ($request->has('term')) {
            $qb->where(function (Builder $qb) use ($request) {
                $qb
                    ->where('ad_shared_infos.full_name', 'like', "%{$request->term}%")
                    ->orWhere('ad_shared_infos.email', 'like', "%{$request->term}%")
                    ->orWhere('ad_shared_infos.phone', 'like', "%{$request->term}%");
            });
        }

        if ($request->has('born_before_year')) {
            $qb->whereYear('applicant.birth_year', '<=', $request->born_before_year);
        }

        if ($request->has('born_after_year')) {
            $qb->whereYear('applicant.birth_year', '>=', $request->born_after_year);
        }

        if ($request->has('city_id')) {
            $qb->where('ad_shared_infos.city_id', $request->city_id);
        }

        if ($request->has('gender_id')) {
            $qb->where('ad_shared_infos.gender_id', $request->gender_id);
        }

        if ($request->has('education_level_id')) {
            $qb->where('ad_shared_infos.education', 'like', "%\"education_level_id\":{$request->education_level_id}%");
        }

        if ($request->has('experience')) {
            $experienceOperator = $request->experience ? '<>' : '=';
            $qb->where('ad_shared_infos.experience', $experienceOperator, '[]');
        }
    }
}
