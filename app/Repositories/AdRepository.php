<?php

namespace App\Repositories;

use App\Http\Requests\Admin\AdListRequest as AdminAdListRequest;
use App\Http\Requests\Company\AdListRequest;
use App\Http\Requests\Company\JobSearchRequest;
use App\Http\Resources\AdCollection;
use App\Models\Ad;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class AdRepository
{
    const DEFAULT_LIST_LIMIT = 20;

    public function filterListForCompanyUser(int $userId, AdListRequest $request): AdCollection
    {
        $qb = Ad::query();

        $qb->where('user_id', $userId);

        if ($request->has('is_ready_to_publish')) {
            $this->buildReadyToPublishCondition($qb, (bool) $request->is_ready_to_publish);
        }

        if ($request->has('is_archived')) {
            $qb->where('is_archived', $request->is_archived);
        }

        if ($request->has('is_active')) {
            $qb->where('is_active', $request->is_active);
        }

        if ($request->has('term')) {
            $qb->where(function (Builder $qb) use ($request) {
                $qb
                    ->where('title', 'like', "%{$request->term}%")
                    ->orWhere('description', 'like', "%{$request->term}%");
            });
        }

        if ($request->has('type_of_work_id')) {
            $qb->where('type_of_work_id', $request->type_of_work_id);
        }

        if ($request->has('city_id')) {
            $qb->where('city_id', $request->city_id);
        }

        if ($request->has('country_id')) {
            $qb->where('country_id', $request->country_id);
        }

        if ($request->has('status')) {
            $this->buildStatusCondition($qb, $request->status);
        }

        $total = $qb->count();

        $ads = $qb
            ->offset($request->offset ?? 0)
            ->limit($request->limit ?? self::DEFAULT_LIST_LIMIT)
            ->orderBy('id', 'DESC')
            ->get()
        ;

        return (new AdCollection($ads))->additional(['meta' => [ 'total' => $total ]]);
    }

    /**
     * Archives ads; Returns affected rows;
     */
    public function archive(int $userId, array $adIds): int
    {
        return Ad::query()
            ->whereIn('id', $adIds)
            ->where('user_id', $userId)
            ->update([
                'is_archived' => 1,
                'is_active' => 0
            ])
        ;
    }

    /**
     * Deletes ads; Returns affected rows;
     */
    public function delete(int $userId, array $adIds): int
    {
        return Ad::query()
            ->whereIn('id', $adIds)
            ->where('user_id', $userId)
            ->delete()
        ;
    }

    /**
     * Unarchives ads; Returns affected rows;
     */
    public function unarchive(int $userId, array $adIds): int
    {
        return Ad::query()
            ->whereIn('id', $adIds)
            ->where('user_id', $userId)
            ->update([
                'is_archived' => 0
            ])
        ;
    }

    public function show(int $userId, array $adIds): int
    {
        return $this->setAdsVisibility($userId, $adIds, true);
    }

    public function hide(int $userId, array $adIds): int
    {
        return $this->setAdsVisibility($userId, $adIds, false);
    }

    public function publicCompanyAds(int $companyId, int $offset, int $limit): array
    {
        $qb = $this->publicAdQueryBuilder();
        $qb->where('user_id', $companyId);

        $total = $qb->count();

        $ads = $qb
            ->with('company')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'DESC')
            ->get()
       ;

        return [
            'ads' => $ads,
            'total' => $total
        ];
    }

    public function publicCompanyAd(int $adId)
    {
        return $this
            ->publicAdQueryBuilder()
            ->where('id', $adId)
            ->with('company')
            ->first()
        ;
    }

    public function jobSearch(JobSearchRequest $request): array
    {
        $qb = $this->publicAdQueryBuilder();

        $qb
            ->select('creator.*', 'ads.*')
            ->orderBy('ads.created_at', 'DESC')
        ;

        $this->buildReadyToPublishCondition($qb, true);
        $this->buildSearchConditions($request, $qb);
        if ($request->has('status')) {
            $this->buildStatusCondition($qb, (int) $request->status);
        }

        return $this->buildResponse(
            $qb,
            $request->offset ?? 0,
            $request->limit ?? self::DEFAULT_LIST_LIMIT,
            'ads.id',
            'DESC'
        );
    }

    public function adminAdSearch(AdminAdListRequest $request): array
    {
        $qb = Ad::query();

        $qb->select('creator.*', 'ads.*');

        $this->buildSearchConditions($request, $qb);

        if ($request->has('is_active')) {
            $qb->where('ads.is_active', $request->is_active);
        }

        if ($request->has('is_archived')) {
            $qb->where('ads.is_archived', $request->is_archived);
        }

        if ($request->has('is_expired')) {
            $qb->where(
                'ads.end_date',
                $request->is_expired ? '<' : '>=',
                date('Y-m-d H:i:s')
            );
        }

        if ($request->has('status')) {
            $this->buildStatusCondition($qb, $request->status);
        }

        return $this->buildResponse(
            $qb,
            $request->offset ?? 0,
            $request->limit ?? self::DEFAULT_LIST_LIMIT,
            $request->has('order_by') ? 'ads.'.$request->order_by : 'ads.id',
            $request->order_dir === 'asc'
        );
    }

    public function activeAdCount(): int
    {
        return $this->publicAdQueryBuilder()->count();
    }


    public function activeAdsTermFilterTitles(string $term, int $limit): array
    {
        $qb = $this->publicAdQueryBuilder();

        $qb
            ->select('ads.title')
            ->orderBy('ads.title', 'ASC')
            ->limit($limit)
            ->distinct()
        ;
        if ($term) {
            $qb->where('ads.title', 'like', "%{$term}%");
        }

        return $qb->getQuery()->get()->pluck('ads.title')->toArray();
    }

    private function buildSearchConditions($request, Builder $qb): void
    {
        $qb->join('users AS creator', 'ads.user_id', '=', 'creator.id');

        if ($request->has('user_id')) {
            $qb->where('ads.user_id', $request->user_id);
        }

        if ($request->has('term') && $request->term) {
            $qb->where(function (Builder $qb) use ($request) {
                $qb
                    ->where('ads.title', 'like', "%{$request->term}%")
                    ->orWhere('ads.description', 'like', "%{$request->term}%")
                    ->orWhere('creator.full_name', 'like', "%{$request->term}%")
                ;
            });
        }

        if ($request->has('types_of_work') && count($request->types_of_work) > 0) {
            $qb->whereIn('ads.type_of_work_id', $request->types_of_work);
        }

        if ($request->has('type_of_work_id')) {
            $qb->where('ads.type_of_work_id', $request->type_of_work_id);
        }

        if ($request->has('job_type_id')) {
            $qb->where('ads.job_type_id', $request->job_type_id);
        }

        if ($request->has('city_id')) {
            $qb->where('ads.city_id', $request->city_id);
        }

        if ($request->has('country_id')) {
            $qb->where('ads.country_id', $request->country_id);
        }

        if ($request->has('education_level_id')) {
            $qb->where('ads.education_level_id', $request->education_level_id);
        }

        if ($request->has('work_time_id')) {
            $qb->where('ads.work_time_id', $request->work_time_id);
        }

        if ($request->has('companies')) {
            $qb->whereIn('ads.user_id', $request->companies);
        }

        if ($request->has('created_before')) {
            $date = Carbon::now()->subDays((int) $request->created_before)->startOfDay();
            $qb->where('ads.created_at', '>=', $date);
        }
    }

    private function buildResponse(Builder $qb, int $offset, int $limit, string $orderBy, bool $ascending): array
    {
        $total = $qb->count();

        $ads = $qb
            ->offset($offset)
            ->limit($limit)
            ->orderBy($orderBy, $ascending ? 'ASC' : 'DESC')
            ->get()
        ;

        return [
            'ads' => $ads,
            'total' => $total
        ];
    }

    private function publicAdQueryBuilder(): Builder
    {
        return Ad::query()
            ->where('ads.is_active', 1)
            ->where('ads.is_archived', 0)
            ->where('ads.visible', 1)
            ->where('ads.end_date', '>=', date('Y-m-d H:i:s'))
            ->where('ads.start_date', '<=', date('Y-m-d H:i:s'))
        ;
    }

    private function setAdsVisibility(int $userId, array $adIds, bool $isVisible): int
    {
        return Ad::query()
            ->whereIn('id', $adIds)
            ->where('user_id', $userId)
            ->update([
                'visible' => $isVisible
            ])
        ;
    }

    private function buildStatusCondition(Builder $qb, int $status): void
    {
        if (!Ad::isStatusValid($status)) {
            return;
        }

        if (Ad::STATUS_ACTIVE === $status) {
            $qb
                ->where('ads.is_active', 1)
                ->where('ads.is_archived', 0)
                ->where('ads.visible', 1)
                ->where('ads.end_date', '>=', date('Y-m-d H:i:s'))
            ;
            return;
        }

        if (Ad::STATUS_DEACTIVATED === $status) {
            $qb
                ->where('ads.is_active', 1)
                ->where('ads.is_archived', 0)
                ->where('ads.visible', 0)
            ;
            return;
        }

        if (Ad::STATUS_ARCHIVED === $status) {
            $qb
                ->where('ads.is_active', 1)
                ->where('ads.is_archived', 1)
            ;
            return;
        }

        if (Ad::STATUS_UNPUBLISHED === $status) {
            $qb->where('ads.is_active', 0);
            $this->buildReadyToPublishCondition($qb, true);

            return;
        }

        if (Ad::STATUS_DRAFT === $status) {
            $qb->where('ads.is_active', 0);
            $this->buildReadyToPublishCondition($qb, false);

            return;
        }

        if (Ad::STATUS_EXPIRED === $status) {
            $qb
                ->where('ads.is_active', 1)
                ->where('ads.is_archived', 0)
                ->where('ads.end_date', '<', date('Y-m-d H:i:s'))
            ;

            return;
        }
    }

    private function buildReadyToPublishCondition(Builder $qb, bool $isReadyToPublish): void
    {
        $columns = [
            'ads.title',
            'ads.country_id',
            'ads.city_id',
            'ads.type_of_work_id',
            'ads.job_type_id',
            'ads.work_time_id',
            'ads.description',
            'ads.start_date',
            'ads.end_date'
        ];

        $qb->where(function (Builder $qb) use ($columns, $isReadyToPublish) {
            $isReadyToPublish ?
                $qb->whereNotNull($columns) :
                $qb->whereNull($columns, 'OR')
            ;
        });
    }
}
