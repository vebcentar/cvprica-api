<?php

namespace App\Repositories;

use App\Models\CompanyQuestion;

class CompanyQuestionRepository
{
    public function countCompanyQuestions(int $companyId): int
    {
        return CompanyQuestion::where('created_by', $companyId)->count();
    }

    public function findByCompanyId(int $companyId)
    {
        return CompanyQuestion::where('created_by', $companyId)
            ->orderBy('updated_at', 'DESC')
            ->get()
        ;
    }

    public function delete(int $companyId, array $questionIds): int
    {
        return CompanyQuestion::query()
            ->where('created_by', $companyId)
            ->whereIn('id', $questionIds)
            ->delete()
        ;
    }
}
