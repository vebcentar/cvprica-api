<?php

namespace App\Repositories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class MessageRepository
{
    public function conversationsForUser(int $userId, int $limit, int $offset = 0, string $term = null): array
    {
        $qb = Message::query();

        $qb
            ->select(DB::raw('MAX(messages.id) as ids'))
            ->where(function (Builder $qb) use ($userId) {
                $qb->where('messages.user_id', $userId)
                   ->orWhere('messages.created_by', $userId)
                ;
            })
            ->groupBy(DB::raw("CASE WHEN messages.user_id = {$userId} THEN messages.created_by ELSE messages.user_id END"))
        ;

        if ($term) {
            $qb
                ->join('users AS from', 'from.id', '=', 'messages.created_by')
                ->join('users AS to', 'to.id', '=', 'messages.user_id')
                ->where(function (Builder $qb) use ($userId, $term) {
                    $qb->where(function (Builder $qb) use ($userId, $term) {
                        $qb
                            ->where('messages.user_id', $userId)
                            ->where('from.full_name', 'like', "%{$term}%")
                        ;
                    })
                    ->orWhere(function (Builder $qb) use ($userId, $term) {
                        $qb
                            ->where('messages.created_by', $userId)
                            ->where('to.full_name', 'like', "%{$term}%")
                        ;
                    });
                })
            ;
        }

        $messageIds = $qb
            ->orderBy('ids', 'DESC')
            ->pluck('ids')
        ;

        $total = count($messageIds);

        $messages = Message::query()
            ->whereIn('id', $messageIds)
            ->limit($limit)
            ->offset($offset)
            ->orderBy('id', 'DESC')
            ->get()
        ;

        return [
            'messages' => $messages,
            'total' => $total
        ];
    }

    public function conversationBetweenTwoUsers(int $firstUserId, int $secondUserId, int $limit, int $fromMessageId = null): array
    {
        $qb = Message::query();

        $qb
            ->where(function (Builder $qb) use ($firstUserId, $secondUserId) {
                $qb
                    ->where(function (Builder $qb) use ($firstUserId, $secondUserId) {
                        $qb
                            ->where('messages.user_id', $firstUserId)
                            ->where('messages.created_by', $secondUserId);
                    })
                    ->orWhere(function (Builder $qb) use ($firstUserId, $secondUserId) {
                        $qb
                            ->where('messages.user_id', $secondUserId)
                            ->where('messages.created_by', $firstUserId);
                    })
                ;
            })
            ->orderBy('id', 'DESC')
        ;

        if ($fromMessageId) {
            $qb->where('id', '<', $fromMessageId);
        }

        $total = $qb->count();

        $messages = $qb->limit($limit)->get();

        return [
            'messages' => $messages,
            'total' => $total
        ];
    }
}
