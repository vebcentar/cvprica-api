<?php

namespace App\Repositories;

use App\Models\User;

class CompanyRepository
{
    public function publicCompanies(int $offset, int $limit, string $term = null, bool $isEminent = null): array
    {
        $qb = User::query();

        $qb
            ->where('role', User::ROLE_COMPANY)
            ->where('is_archived', 0)
            ->where('is_active', 1)
            ->where('is_hidden', 0)
        ;

        if ($term) {
            $qb->where('full_name', 'like', "%{$term}%");
        }

        if (!is_null($isEminent)) {
            $qb->where('eminent', $isEminent);
        }

        $total = $qb->count();

        $companies = $qb
            ->offset($offset)
            ->limit($limit)
            ->get()
        ;

        return [
            'companies' => $companies,
            'total' => $total
        ];
    }
}
