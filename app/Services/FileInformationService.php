<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;

class FileInformationService
{
    public function getDuration(string $path): int
    {
        if (!\file_exists($path)) {
            throw new BusinessLogicException('File not found. Unable to determine duration');
        }

        $getID3 = new \getID3;
        $fileInfo = $getID3->analyze($path);
        if (!\array_key_exists('playtime_seconds', $fileInfo)) {
            throw new \RuntimeException('Unable to determine duration');
        }

        return (int) $fileInfo['playtime_seconds'];
    }
}
