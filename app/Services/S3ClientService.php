<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use Aws\S3\Exception\S3Exception;
use Aws\S3\PostObjectV4;
use Aws\S3\S3ClientInterface;
use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;

class S3ClientService
{
    const UPLOAD_FILE_LIMIT_1GB = 1073741824;

    private S3ClientInterface $client;

    private string $bucketName;

    public function __construct(S3ClientInterface $client, string $bucketName)
    {
        $this->client = $client;
        $this->bucketName = $bucketName;
    }

    public function createVideoSignedPost(): array
    {
        $key = $this->videoFileKey();

        $formInputs = [
            'key' => $key,
            'acl' => 'public-read'
        ];

        $options = [
            [ 'acl' => 'public-read' ],
            [ 'bucket' => $this->bucketName ],
            [ 'eq', '$key', $key ],
            [ 'content-length-range', 100, self::UPLOAD_FILE_LIMIT_1GB ]
        ];

        $expires = '+2 hours';

        $postObject = new PostObjectV4(
            $this->client,
            $this->bucketName,
            $formInputs,
            $options,
            $expires
        );

        return [
            'attributes' => $postObject->getFormAttributes(),
            'inputs' => $postObject->getFormInputs()
        ];
    }

    public function deleteObject(string $path): void
    {
        $fileKey = $this->getKeyFromPath($path);

        $this->client->deleteObject([
            'Bucket' => $this->bucketName,
            'Key' => $fileKey,
        ]);
    }

    public function videoFileKey(): string
    {
       return $this->fileKey($this->baseVideoPath());
    }

    private function baseVideoPath(): string
    {
        return 'media/video';
    }

    public function fileKey(string $basePath): string
    {
        $now = new \DateTimeImmutable();
        $uuid = Uuid::uuid4()->toString();

        return "{$basePath}/{$now->format('Y')}/{$now->format('m')}/{$now->format('d')}/{$uuid}";
    }

    public function baseS3Url(): string
    {
        return "https://{$this->bucketName}.s3.{$this->client->getRegion()}.amazonaws.com";
    }

    public function regex(): string
    {
        $path = $this->baseS3Url().'/'.$this->baseVideoPath();

        $regex = str_replace('/', '\/', $path);
        $regex = str_replace('.', '\.', $regex);

        return '/'.$regex.'\/\d{4}\/\d{2}\/\d{2}\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/';
    }

    public function isS3Url(string $video): bool
    {
        return str_starts_with($this->baseS3Url(), $video);
    }

    public function uploadVideo(UploadedFile $video): string
    {
        $fileKey = $this->videoFileKey();

        try {
            $this->client->putObject([
                'Bucket' => $this->bucketName,
                'Key' => $fileKey,
                'Body' => fopen($video->getRealPath(), 'r'),
                'ACL' => 'public-read',
            ]);
        } catch (S3Exception $e) {
            throw new \RuntimeException("Error processing video: {$fileKey}");
        }

        return $this->baseS3Url().'/'.$fileKey;
    }

    private function getKeyFromPath(string $path): string
    {
        if (!\str_contains($path, $this->baseS3Url())) {
            throw new \RuntimeException("Path is not valid S3 bucket url: {$path}");
        }

        return str_replace($this->baseS3Url().'/', '', $path);
    }
}
