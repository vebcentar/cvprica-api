<?php

namespace App\Services\OTP;

use App\Exceptions\BusinessLogicException;
use App\Models\OneTimePassword;
use App\Models\User;
use App\Services\SMSClientService;
use Psr\Log\LoggerInterface;

class OTPService implements OTPServiceInterface
{
    private SMSClientService $smsService;

    private LoggerInterface $logger;

    public function __construct(SMSClientService $smsService, LoggerInterface $logger)
    {
        $this->smsService = $smsService;
        $this->logger = $logger;
    }

    public function sendSMSForUser(User $user): void
    {
        $user->validateUserCanVerifyPhone();

        try {
            $otp = $this->createForPhoneNumber($user->phone);
            $this->smsService->sendSMS($user->phone, 'Verifikacioni kod: '.$otp->code);
        } catch (\Throwable $ex) {
            $this->logger->error("Error sending OTP {$otp->code} to phone number $otp->phone: {$ex->getMessage()} {$ex->getTraceAsString()}");

            throw new BusinessLogicException('Error sending OTP');
        }
    }

    public function confirmPhoneForUser(User $user, string $code): void
    {
        $user->validateUserCanVerifyPhone();

        /** @var OneTimePassword $otp */
        if (!$otp = OneTimePassword::query()->where('identifier', $user->phone)->where('type', OneTimePassword::TYPE_PHONE)->first()) {
            throw new BusinessLogicException('OTP not found');
        }

        if ($otp->isUsed()) {
            throw new BusinessLogicException('OTP already used');
        }

        if ($otp->isExpired()) {
            throw new BusinessLogicException('OTP expired');
        }

        if (!$otp->isCodeValid($code)) {
            throw new BusinessLogicException('OTP not correct');
        }

        $otp->used_on = new \DateTimeImmutable();
        $otp->save();

        $user->phone_verified_at = new \DateTime();
        if (0 === $user->is_active && $user->isPhoneAndEmailVerified()) {
            $user->is_active = 1;
        }
        $user->save();
    }

    private function createForPhoneNumber(string $phoneNumber): OneTimePassword
    {
        OneTimePassword::query()->where('identifier', $phoneNumber)->where('type', OneTimePassword::TYPE_PHONE)->delete();

        return OneTimePassword::create([
            'code' => self::generateNumbersOtp(),
            'type' => OneTimePassword::TYPE_PHONE,
            'identifier' => $phoneNumber,
            'valid_until' => (new \DateTimeImmutable('now + 1 hour'))
        ]);
    }

    public function createForEmail(string $email): OneTimePassword
    {
        OneTimePassword::query()->where('identifier', $email)->where('type', OneTimePassword::TYPE_EMAIL)->delete();

        return OneTimePassword::create([
            'code' => self::generateNumbersOtp(),
            'type' => OneTimePassword::TYPE_EMAIL,
            'identifier' => $email,
            'valid_until' => (new \DateTimeImmutable('now + 1 week'))
        ]);
    }

    public static function generateNumbersOtp(): string
    {
        return sprintf('%06d', random_int(1000, 999999));
    }
}
