<?php

namespace App\Services\OTP;

use App\Models\OneTimePassword;
use App\Models\User;

class NullOTPService implements OTPServiceInterface
{
    public function sendSMSForUser(User $user): void
    {
    }

    public function confirmPhoneForUser(User $user, string $code): void
    {
    }
}
