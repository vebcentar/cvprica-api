<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Http\Resources\CompanyQuestionCollection;
use App\Models\CompanyQuestion;
use App\Models\User;
use App\Repositories\CompanyQuestionRepository;

class CompanyQuestionService
{
    const MAX_NUMBER_OF_QUESTIONS_PER_COMPANY = 10;

    private CompanyQuestionRepository $companyQuestionRepository;

    public function __construct(CompanyQuestionRepository $companyQuestionRepository)
    {
        $this->companyQuestionRepository = $companyQuestionRepository;
    }

    /**
     * @throws BusinessLogicException
     */
    public function create(string $question, User $creator): CompanyQuestion
    {
        if (!$creator->isCompany()) {
            throw new BusinessLogicException('Only companies can create company questions');
        }

        if (self::MAX_NUMBER_OF_QUESTIONS_PER_COMPANY <= $this->companyQuestionRepository->countCompanyQuestions($creator->id)) {
            throw new BusinessLogicException(sprintf('Company can create a maximum of %d questions', self::MAX_NUMBER_OF_QUESTIONS_PER_COMPANY));
        }

        return CompanyQuestion::create([
            'question' => $question,
            'created_by' => $creator->id
        ]);
    }

    public function findByCompanyId(int $companyId): CompanyQuestionCollection
    {
        $questions = $this->companyQuestionRepository->findByCompanyId($companyId);

        return (new CompanyQuestionCollection($questions))->additional(['meta' => [ 'total' => count($questions) ]]);
    }
}
