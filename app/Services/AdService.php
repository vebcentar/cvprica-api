<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Exceptions\SafeException;
use App\Http\Requests\Admin\AdListRequest;
use App\Http\Requests\Company\AdRequest;
use App\Http\Requests\Company\JobSearchRequest;
use App\Http\Resources\Admin\AdCollection;
use App\Http\Resources\PublicAdCollection;
use App\Http\Resources\PublicAdResource;
use App\Models\Ad;
use App\Models\AdQuestion;
use App\Models\User;
use App\Repositories\AdRepository;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;

class AdService
{
    private LoggerInterface $logger;

    private AdRepository $adRepository;

    private NotificationService $notificationService;

    public function __construct(LoggerInterface $logger, AdRepository $adRepository, NotificationService $notificationService)
    {
        $this->logger = $logger;
        $this->adRepository = $adRepository;
        $this->notificationService = $notificationService;
    }

    /**
     * @throws SafeException
     */
    public function create(User $company, AdRequest $request): Ad
    {
        $validatedData = $request->validated();

        $validatedData['user_id'] = $company->id;
        $validatedData['created_by'] = $company->id;
        $validatedData['is_archived'] = 0;
        $validatedData['visible'] = 1;

        try {
            DB::beginTransaction();

            /** @var Ad $ad */
            $ad = Ad::create($validatedData)->refresh();

            if (!$ad->ready_to_publish && $ad->isActive()) {
                $ad->deactivate();
                $ad->save();
            }

            $this->handleQuestionsForAd($ad, self::parseQuestions($request));

            if ($ad->isPublished()) {
                $this->notificationService->sendNotificationForAd($ad);
            }
            DB::commit();

            return $ad->load('questions');
        } catch (SafeException $ex) {
            DB::rollBack();
            throw $ex;
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->logger->error("Error creating add: {$ex->getMessage()} {$ex->getTraceAsString()}");
            throw new SafeException('Error creating add');
        }
    }

    public function update(Ad $ad, AdRequest $request): void
    {
        $validatedData = $request->validated();

        try {
            DB::beginTransaction();

            $ad->update($validatedData);

            if (!$ad->ready_to_publish && $ad->isActive()) {
                $ad->deactivate();
                $ad->save();
            }

            $this->handleQuestionsForAd($ad, self::parseQuestions($request));
            DB::commit();
        } catch (SafeException $ex) {
            DB::rollBack();
            throw $ex;
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->logger->error("Error updating add: {$ex->getMessage()} {$ex->getTraceAsString()}");
            throw new SafeException('Error updating add');
        }
    }

    public function publicCompanyAds(int $companyId, int $offset, int $limit): PublicAdCollection
    {
        if ($limit < 5) {
            $limit = 20;
        }

        $responseArray = $this->adRepository->publicCompanyAds($companyId, $offset, $limit);

        return (new PublicAdCollection($responseArray['ads']))
            ->additional(['meta' => [ 'total' => $responseArray['total'] ]])
        ;
    }

    public function publicCompanyAd(int $adId): ?PublicAdResource
    {
        if (!$ad = $this->adRepository->publicCompanyAd($adId)) {
            return null;
        }

        return new PublicAdResource($ad);
    }

    public function jobSearch(JobSearchRequest $request): PublicAdCollection
    {
        $adsArray = $this->adRepository->jobSearch($request);

        return (new PublicAdCollection($adsArray['ads']))->additional(['meta' => [ 'total' => $adsArray['total'] ]]);
    }

    private static function parseQuestions(AdRequest $request): array
    {
        if (!$request->ads_questions) {
            return [];
        }

        $adQuestions = \json_decode($request->ads_questions, true);

        $adQuestions = array_map(
            function($question) {
                return \is_array($question) && isset($question['text_question']) ?
                    $question['text_question'] :
                    $question
                ;
            },
            $adQuestions
        );

        return array_filter($adQuestions, function ($question) {
            if (!is_string($question)) {
                throw new BusinessLogicException('Question is not a string!');
            }
            return mb_strlen(trim($question)) > 0;
        });
    }

    private function handleQuestionsForAd(Ad $ad, array $questions): void
    {
        if (count($questions) > Ad::MAX_NUMBER_OF_QUESTIONS) {
            throw new BusinessLogicException(sprintf('Cannot have more then %d questions', Ad::MAX_NUMBER_OF_QUESTIONS));
        }

        AdQuestion::where('ad_id', $ad->id)->delete();
        foreach ($questions as $question) {
            AdQuestion::create([
                'ad_id' => $ad->id,
                'text_question' => $question,
                'created_by' => $ad->company->id
            ]);
        }
    }

    public function adminAdSearch(AdListRequest $request): AdCollection
    {
        $adsArray = $this->adRepository->adminAdSearch($request);

        return (new AdCollection($adsArray['ads']))->additional(['meta' => [ 'total' => $adsArray['total'] ]]);
    }
}
