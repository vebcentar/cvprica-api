<?php

namespace App\Services;

use App\Models\ComputerSkillKnowledgeLevel;
use App\Models\ComputerSkillName;
use App\Models\DriversLicenceCategory;
use App\Models\EducationArea;
use App\Models\EducationLevel;
use App\Models\EducationTitle;
use App\Models\JobCategory;
use App\Models\JobType;
use App\Models\Language;
use App\Models\LanguageRead;
use App\Models\LanguageSpeak;
use App\Models\LanguageWrite;
use App\Models\TypeOfWork;
use App\Models\WorkTime;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CommonDataService
{
    const COMMON_DATA_CACHE_KEY = 'common-data-cache-key';
    const COMMON_DATA_JOB_FILTER_CACHE_KEY = 'common-data-job-filter-cache-key';

    public function fetchCommonData(): array
    {
        if (!$commonData = Cache::get(self::COMMON_DATA_CACHE_KEY)) {
            $commonData = $this->buildCommonData();
            Cache::put(self::COMMON_DATA_CACHE_KEY, $commonData, new \DateTimeImmutable('now + 1 week'));
        }

        return $commonData;
    }

    public function fetchJobFilterCommonData(): array
    {
        if (!$commonData = Cache::get(self::COMMON_DATA_JOB_FILTER_CACHE_KEY)) {
            $commonData = $this->buildJobFilterCommonData();
            Cache::put(self::COMMON_DATA_JOB_FILTER_CACHE_KEY, $commonData, new \DateTimeImmutable('now + 1 week'));
        }

        return $commonData;
    }

    private function buildJobFilterCommonData(): array
    {
        return [
            'education_levels' => EducationLevel::all([ 'id', 'name' ]),
            'work_times' => WorkTime::all([ 'id', 'name' ]),
            'job_types' => JobType::all([ 'id', 'name' ])
        ];
    }

    private function buildCommonData(): array
    {
        return [
            'education_levels' => EducationLevel::all([ 'id', 'name' ]),
            'education_areas' => EducationArea::all([ 'id', 'name' ]),
            'education_titles' => EducationTitle::all([ 'id', 'name' ]),
            'job_categories' => JobCategory::all([ 'id', 'name' ]),
            'languages' => Language::all([ 'id', 'name' ]),
            'languages_speak' => LanguageSpeak::all([ 'id', 'name' ]),
            'languages_read' => LanguageRead::all([ 'id', 'name' ]),
            'languages_write' => LanguageWrite::all([ 'id', 'name' ]),
            'computer_skills_names' => ComputerSkillName::all([ 'id', 'name' ]),
            'computer_skills_levels' => ComputerSkillKnowledgeLevel::all([ 'id', 'name' ]),
            'drivers_licence_categories' => DriversLicenceCategory::all([ 'id', 'name' ]),
            'job_types' => JobType::all([ 'id', 'name', 'type_of_work_id' ]),
            'type_of_works' => $this->buildTypeOfWorks(),
            'work_times' => WorkTime::all([ 'id', 'name' ])
        ];
    }

    private function buildTypeOfWorks(): Collection
    {
        $typeOfWorks = TypeOfWork::withCount('activeAds')->orderBy('name', 'asc')->get();

        return collect($typeOfWorks)->map(function ($item) {
            $item['field'] = 'job_category';
            return $item;
        });
    }
}
