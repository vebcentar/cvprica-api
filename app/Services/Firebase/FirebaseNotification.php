<?php

namespace App\Services\Firebase;

use App\Models\ValueObject\Priority;

class FirebaseNotification
{
    const TYPE_EMAIL_VERIFIED = 'email_verified';
    const TYPE_NULL = 'null';

    private string $type;

    private array $data;

    private array $deviceTokens;

    private Priority $priority;

    private array $apnsConfigData;

    public function __construct(string $type, array $data, array $deviceTokens, Priority $priority, array $apnsConfigData = [])
    {
        $this->type = $type;
        $this->data = $data;
        $this->deviceTokens = $deviceTokens;
        $this->apnsConfigData = $apnsConfigData;
        $this->priority = $priority;
    }

    public static function createNull(): FirebaseNotification
    {
        return new self(self::TYPE_NULL, [], [], Priority::low());
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isNull(): bool
    {
        return self::TYPE_NULL === $this->type;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getDeviceTokens(): array
    {
        return $this->deviceTokens;
    }

    public function hasDeviceTokens(): bool
    {
        return count($this->deviceTokens) > 0;
    }

    public function getPriority(): Priority
    {
        return $this->priority;
    }

    public function isHighPriority(): bool
    {
        return $this->priority->isHigh();
    }

    public function isLowPriority(): bool
    {
        return $this->priority->isLow();
    }

    public function getApnsConfigData(): array
    {
        return $this->apnsConfigData;
    }
}
