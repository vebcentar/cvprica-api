<?php

namespace App\Services\Firebase;

use App\Models\User;
use App\Services\Firebase\Client\FirebaseClientInterface;

class FirebaseService
{
    private FirebaseClientInterface $firebaseClient;

    private FirebaseNotificationFactory $notificationFactory;

    public function __construct(FirebaseClientInterface $firebaseClient, FirebaseNotificationFactory $notificationFactory)
    {
        $this->firebaseClient = $firebaseClient;
        $this->notificationFactory = $notificationFactory;
    }

    public function sendForEmailVerified(User $user): void
    {
        $notification = $this->notificationFactory->createForEmailVerified($user);

        $this->firebaseClient->sendNotification($notification);
    }

    public function isTokenValid(string $token): bool
    {
        return $this->firebaseClient->isTokenValid($token);
    }
}
