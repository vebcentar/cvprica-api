<?php

namespace App\Services\Firebase\Client;

use App\Services\Firebase\FirebaseNotification;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\AndroidConfig;
use Kreait\Firebase\Messaging\ApnsConfig;
use Kreait\Firebase\Messaging\CloudMessage;
use Psr\Log\LoggerInterface;

class FirebaseClient implements FirebaseClientInterface
{
    private Messaging $messaging;

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->messaging = app('firebase.messaging');
        $this->logger = $logger;
    }

    public function sendNotification(FirebaseNotification $notification): void
    {
        if (!$notification->hasDeviceTokens() || $notification->isNull()) {
            return;
        }
        $message = CloudMessage::new()->withData($notification->getData());
        $message = $message
            ->withApnsConfig(ApnsConfig::fromArray($notification->getApnsConfigData()))
            ->withAndroidConfig(AndroidConfig::fromArray(['data' => self::androidDataArray($notification)]))
        ;

        if ($notification->isHighPriority()) {
            $message = $message->withHighestPossiblePriority();
        }

        if ($notification->isLowPriority()) {
            $message = $message->withLowestPossiblePriority();
        }

        $sendReport = $this->messaging->sendMulticast($message, $notification->getDeviceTokens());

        if (!$sendReport->hasFailures()) {
            return;
        }

        foreach ($sendReport->failures()->getItems() as $failure) {
            $errorMessage = $failure->error() ? $failure->error()->getMessage() : 'Unknown';
            $this->logger->error("Error sending notification to device: {$errorMessage}");
        }
    }

    public function isTokenValid(string $token): bool
    {
        try {
            $validationResult = $this->messaging->validateRegistrationTokens([$token]);
        } catch (\Throwable $e) {
            return false;
        }

        return in_array($token, $validationResult['valid']);
    }

    private static function androidDataArray(FirebaseNotification $notification): array
    {
        return array_merge(
            $notification->getData(),
            [
                'type' => $notification->getType(),
                'priority' => $notification->getPriority()->getValue(),
            ]
        );
    }
}
