<?php

namespace App\Services\Firebase\Client;

use App\Services\Firebase\FirebaseNotification;

class NullFirebaseClient implements FirebaseClientInterface
{
    public function sendNotification(FirebaseNotification $notification): void
    {
    }

    public function isTokenValid(string $token): bool
    {
        return true;
    }
}
