<?php

namespace App\Services\Firebase;

use App\Models\ValueObject\Priority;
use App\Models\User;

class FirebaseNotificationFactory
{
    public function createForEmailVerified(User $user): FirebaseNotification
    {
        if (!$user->hasDeviceToken()) {
            return FirebaseNotification::createNull();
        }

        $type = FirebaseNotification::TYPE_EMAIL_VERIFIED;

        $data = [
            'user_id' => $user->getId()->toString(),
            'email' => $user->getEmail(),
            'title' => 'Email verifikovan',
            'body' => "Uspešno ste verifikovali email {$user->email}",
            'type' => $type,
        ];

        $apnsData = $this->defaultSilentApnsData($type);

        return new FirebaseNotification(
            $type,
            $data,
            [$user->getDeviceToken()],
            Priority::normal(),
            $apnsData
        );
    }

    private function defaultSilentApnsData(string $category): array
    {
        return [
            'payload' => [
                'aps' => [
                    'category' => $category,
                    'content-available' => 1,
                ],
            ],
        ];
    }
}
