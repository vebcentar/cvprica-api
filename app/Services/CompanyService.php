<?php

namespace App\Services;

use App\Http\Resources\Company\CompanyPublicInfoCollection;
use App\Repositories\CompanyRepository;

class CompanyService
{
    const MIN_PAGINATION_LIMIT = 5;
    const DEFAULT_PAGINATION_LIMIT = 20;

    private CompanyRepository $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function publicCompanies(int $offset, int $limit, string $term = null, bool $isEminent = null): CompanyPublicInfoCollection
    {
        if ($limit < self::MIN_PAGINATION_LIMIT) {
            $limit = self::DEFAULT_PAGINATION_LIMIT;
        }
        $result = $this->companyRepository->publicCompanies($offset, $limit, $term, $isEminent);

        return (new CompanyPublicInfoCollection($result['companies']))->additional(['meta' => [ 'total' => $result['total'] ]]);
    }
}
