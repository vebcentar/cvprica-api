<?php

namespace App\Services;

use App\Http\Requests\Candidate\ApplicationRequest;
use App\Http\Resources\Candidate\ApplicationCollection;
use App\Repositories\AdSharedInfoRepository;

class ApplicationService
{
    private AdSharedInfoRepository $adSharedInfoRepository;

    public function __construct(AdSharedInfoRepository $adSharedInfoRepository)
    {
        $this->adSharedInfoRepository = $adSharedInfoRepository;
    }

    public function filterApplicationsForCandidate(int $candidateId, ApplicationRequest $request): ApplicationCollection
    {
        $applicationsArray = $this->adSharedInfoRepository->filterApplicationsForCandidate($candidateId, $request);

        $collection = new ApplicationCollection($applicationsArray['applications']);
        $collection->additional(['meta' => [ 'total' => $applicationsArray['total'] ]]);

        return $collection;
    }
}
