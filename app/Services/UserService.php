<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Exceptions\ValidationException;
use App\Models\User;
use App\Services\Firebase\FirebaseService;
use Illuminate\Support\Str;

class UserService
{
    private NotificationService $notificationService;

    private FirebaseService $firebaseService;

    public function __construct(NotificationService $notificationService, FirebaseService $firebaseService)
    {
        $this->notificationService = $notificationService;
        $this->firebaseService = $firebaseService;
    }

    public function createFromGoogleIdToken(array $idToken, string $role): User
    {
        $this->validateIdToken($idToken);

        /** @var User $user */
        if (!$user = $this->findByEmailAndRole($idToken['email'], $role)) {
            $user = $this->createUser($idToken['email'], $role, $idToken['name']);
        }

        if (!$user->isEmailVerified()) {
            $user->email_verified_at = new \DateTimeImmutable();
            $user->save();
        }

        return $user;
    }

    private function createUser(string $email, string $role, ?string $fullName, string $password = null): User
    {
        $this->validateUserCreateRole($role);

        if (!$password) {
            $password = Str::random();
        }

        /** @var User $user */
        $user = User::query()->create([
            'role' => $role,
            'email' => $email,
            'full_name' => $fullName ?? '',
            'is_active' => 0,
            'password' => $password
        ]);

        $user->refresh();

        if ($user->isCandidate()) {
            $this->notificationService->sendNotificationForNewUser($user);
        }

        return $user;
    }

    public function generateAccessTokenForUser(User $user): string
    {
        return $user->createToken('LaravelAuthApp')->accessToken;
    }

    public function findByEmailAndRole(string $email, string $role): ?User
    {
        /** @var User $user */
        if (!$user = User::query()->where('email', $email)->first()) {
            return null;
        }

        if (!$user->hasRole($role)) {
            throw new BusinessLogicException("User doesn't have a role {$role}");
        }

        return $user;
    }

    public function validateIdToken(array $idToken): void
    {
        foreach (self::requiredFields() as $fieldName) {
            if (!isset($idToken[$fieldName])) {
                throw new BusinessLogicException(sprintf('Required fields missing: %s', $fieldName));
            }
        }

        if (!$idToken['email_verified']) {
            throw new BusinessLogicException('Email not verified');
        }
    }

    public function setDeviceToken(User $user, string $token): void
    {
        if (!$this->firebaseService->isTokenValid($token)) {
            throw new ValidationException('Token is not valid');
        }

        if ($user->device_token === $token) {
            return;
        }

        User::query()->where('device_token', $token)->update([ 'device_token' => null ]);

        $user->setDeviceToken($token);
        $user->save();
    }

    public function deleteDeviceToken(User $user): void
    {
        if (!$user->hasDeviceToken()) {
            return;
        }

        $user->eraseDeviceToken();
        $user->save();
    }

    public function changeUserPhone(User $user, ?string $newPhone, bool $save = false): void
    {
        if ($user->phone === $newPhone) {
            return;
        }

        if ($newPhone && User::query()
            ->where('phone', $newPhone)
            ->whereNotNull('phone_verified_at')
            ->where('id', '<>', $user->id)
            ->where('role', $user->role)
            ->exists()
        ) {
            throw new BusinessLogicException('Phone must be unique');
        }

        $user->phone = $newPhone;
        $user->phone_verified_at = null;


        if ($save) {
            $user->save();
        }
    }

    public static function requiredFields(): array
    {
        return [
            'email',
            'email_verified',
            'name',
        ];
    }

    private function validateUserCreateRole(string $role)
    {
        $validRoles = [ User::ROLE_CANDIDATE, User::ROLE_COMPANY ];
        if (!in_array($role, $validRoles)) {
            throw new BusinessLogicException(sprintf('Role must be in: %s', implode(', ', $validRoles)));
        }
    }
}
