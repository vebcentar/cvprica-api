<?php

namespace App\Services;

use App\Models\Ad;
use App\Models\City;
use App\Models\Country;
use App\Models\TypeOfWork;
use App\Models\User;
use App\Repositories\AdRepository;
use Illuminate\Support\Facades\DB;

class JobSearchFilterService
{
    const DEFAULT_LIMIT = 5;

    private CommonDataService $commonDataService;

    private AdRepository $adRepository;

    public function __construct(CommonDataService $commonDataService, AdRepository $adRepository)
    {
        $this->commonDataService = $commonDataService;
        $this->adRepository = $adRepository;
    }

    public function allFilters(): array
    {
        $jobFilters = [
            'locations' => $this->countryFilter(),
            'cities' => $this->cityFilter(),
            'types_of_work' => $this->typeOfWorkFilter(),
            'companies' => $this->companiesFilter(),
            'totals' => [
                'companies' => $this->activeCompaniesCount(),
                'ads' => $this->activeAdsCount(),
            ]
        ];

        $commonDataFilters = $this->commonDataService->fetchJobFilterCommonData();

        return array_merge(
            $commonDataFilters,
            $jobFilters
        );
    }

    public function companiesFilter(string $term = null): array
    {
        $qb = User::query();

        $qb
            ->select('id AS company_id', 'full_name')
            ->where('role', User::ROLE_COMPANY)
            ->withCount('activeAds')
            ->orderBy('full_name', 'ASC')
            ->limit(self::DEFAULT_LIMIT)
        ;
        if ($term) {
            $qb->where('full_name', 'like', "%{$term}%");
        }

        return $qb->getQuery()->get()->toArray();
    }

    public function typeOfWorkFilter(string $term = null): array
    {
        $qb = TypeOfWork::query();

        $qb
            ->select('id AS type_of_work_id', 'name')
            ->withCount('activeAds')
            ->orderBy('name', 'ASC')
        ;
        if ($term) {
            $qb->where('name', 'like', "%{$term}%");
        }

        return $qb->getQuery()->get()->toArray();
    }

    public function termFilter(string $term): array
    {
        return [
            'companies' => $this->companiesTitlesFilter($term),
            'ads' => $this->adsTermFilter($term),
        ];
    }

    public function locationFilter(string $term = null): array
    {
        $countries = $this->countryFilter($term);
        $cities = $this->cityFilter($term, self::DEFAULT_LIMIT);

        return array_slice(
            array_merge($countries, $cities),
            0,
            self::DEFAULT_LIMIT
        );
    }

    private function activeAdsCount(): int
    {
        return $this->adRepository->activeAdCount();
    }

    private function activeCompaniesCount()
    {
        return User::where('role', User::ROLE_COMPANY)->count();
    }

    private function cityFilter(string $term = null, int $limit = null): array
    {
        $qb = City::query();

        $qb
            ->select('id AS city_id', 'name', DB::raw('CONCAT(\'city_id_\', id) AS list_key'))
            ->withCount('activeAds')
            ->orderBy('name', 'ASC')
        ;

        if ($term) {
            $qb->where('name', 'like', "%{$term}%");
        }

        if ($limit) {
            $qb->limit($limit);
        }

        return $qb->getQuery()->get()->toArray();
    }

    private function countryFilter(string $term = null): array
    {
        $qb = Country::query();

        $qb
            ->select('id AS country_id', 'name', DB::raw('CONCAT(\'country_id_\', id) AS list_key'))
            ->withCount('activeAds')
            ->orderBy('name', 'ASC')
            ->limit(self::DEFAULT_LIMIT)
        ;
        if ($term) {
            $qb->where('name', 'like', "%{$term}%");
        }

        return $qb->getQuery()->get()->toArray();
    }

    private function adsTermFilter(string $term): array
    {
        return $this->adRepository->activeAdsTermFilterTitles($term, self::DEFAULT_LIMIT);
    }

    private function companiesTitlesFilter(string $term): array
    {
        $qb = User::query();

        $qb
            ->select('full_name')
            ->where('role', User::ROLE_COMPANY)
            ->where('is_archived', 0)
            ->where('is_active', 1)
            ->where('is_hidden', 0)
            ->orderBy('full_name', 'ASC')
            ->limit(self::DEFAULT_LIMIT)
            ->distinct()
        ;
        if ($term) {
            $qb->where('full_name', 'like', "%{$term}%");
        }

        return $qb->getQuery()->get()->pluck('full_name')->toArray();
    }
}
