<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Mail\EmailVerification;
use App\Models\OneTimePassword;
use App\Models\User;
use App\Services\Firebase\FirebaseService;
use App\Services\OTP\OTPService;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class EmailVerificationService
{
    private OTPService $otpService;

    private FirebaseService $firebaseService;

    public function __construct(OTPService $otpService, FirebaseService $firebaseService)
    {
        $this->otpService = $otpService;
        $this->firebaseService = $firebaseService;
    }

    public function sendVerificationEmailForUser(User $user)
    {
        $url = route('v2.callback.verify_email', [ 'code' => $this->generateVerificationCodeForUser($user) ]);

        Mail::to($user->email)->send(new EmailVerification($url));
    }

    public function verifyEmail(string $code): void
    {
        $data = $this->decryptCode($code);

        /** @var User $user */
        $user = $data['user'];

        /** @var OneTimePassword $otp */
        $otp = $data['otp'];

        if ($otp->isUsed()) {
            return;
        }

        if ($otp->isExpired()) {
            throw new BadRequestHttpException('Code expired');
        }

        $otp->used_on = new \DateTimeImmutable();
        $otp->save();

        if ($user->isEmailVerified()) {
            return;
        }

        if ($user->email !== $data['email']) {
            throw new BadRequestHttpException('User has different email');
        }

        $user->email_verified_at = new \DateTimeImmutable();
        if (0 === $user->is_active && $user->isPhoneAndEmailVerified()) {
            $user->is_active = 1;
        }
        $user->save();

        $this->firebaseService->sendForEmailVerified($user);
    }

    public function generateVerificationCodeForUser(User $user): string
    {
        if (!$user->email) {
            throw new BusinessLogicException('User has no email');
        }

        return $this->generateVerificationCode($user->id, $user->email);
    }

    public function generateVerificationCode(int $id, string $email): string
    {
        $otp = $this->otpService->createForEmail($email);

        $verificationData = [
            $id,
            $email,
            $otp->code
        ];

        return Crypt::encryptString(\json_encode($verificationData));
    }

    private function decryptCode(string $code): array
    {
        $decryptedData = Crypt::decryptString($code);

        $array = \json_decode($decryptedData, true);

        if (count($array) != 3) {
            throw new BadRequestHttpException('Code not valid');
        }

        /** @var User $user */
        if (!$user = User::query()->find($array[0])) {
            throw new BadRequestHttpException('User not found');
        }

        /** @var OneTimePassword $otp */
        if (!$otp = OneTimePassword::query()->where('type', OneTimePassword::TYPE_EMAIL)->where('code', $array[2])->first()) {
            throw new BadRequestHttpException('Code not found');
        }

        return [
            'user' => $user,
            'email' => $array[1],
            'otp' => $otp
        ];
    }
}
