<?php

namespace App\Services;

use App\Models\AdAnswer;
use App\Models\User;
use App\Models\Video;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdAnswerService
{
    private MediaService $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    public function handleAnswersFromRequest(User $user, array $answers): array
    {
        $adAnswers = [];

        foreach ($answers as $answer) {
            $alreadySavedAnswer = AdAnswer::query()
                ->where('ad_question_id', $answer['ad_question_id'])
                ->where('user_id', $user->id)
                ->with('video')
                ->first()
            ;

            if (!isset($answer['video_id'])) {
                if ($alreadySavedAnswer) {
                    if ($alreadySavedAnswer->video) {
                        $this->mediaService->deleteVideo($alreadySavedAnswer->video);
                    }
                    $alreadySavedAnswer->delete();
                }

                continue;
            }

            if ($alreadySavedAnswer && $answer['video_id'] === $alreadySavedAnswer->video_id) {
                $adAnswers[] = $alreadySavedAnswer;
                continue;
            }

            if ($alreadySavedAnswer) {
                if ($alreadySavedAnswer->video) {
                    $this->mediaService->deleteVideo($alreadySavedAnswer->video);
                }
                $alreadySavedAnswer->delete();
            }

            if (isset($answer['video_id'])) {
                $video = Video::query()->findOrFail($answer['video_id']);
                if ($video->created_by !== $user->id) {
                    throw new AccessDeniedHttpException('You are not the owner of this video');
                }
            }

            $adAnswers[] = AdAnswer::create([
                'video_id' => $answer['video_id'],
                'ad_question_id' => $answer['ad_question_id'],
                'created_by' => $user->id,
                'user_id' => $user->id
            ]);
        }

        return $adAnswers;
    }
}
