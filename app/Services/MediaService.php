<?php

namespace App\Services;

use App\Models\Ad;
use App\Models\AdSharedInfo;
use App\Models\CvVideo;
use App\Models\User;
use App\Models\UserDocument;
use App\Models\Video;
use Illuminate\Http\UploadedFile;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class MediaService
{
    const IMAGE_MAX_FILE_SIZE_IN_KB = 8192;

    private LoggerInterface $logger;

    private FileInformationService $fileInformationService;

    private S3ClientService $s3ClientService;

    public function __construct(LoggerInterface $logger, FileInformationService $fileInformationService, S3ClientService $s3ClientService)
    {
        $this->logger = $logger;
        $this->fileInformationService = $fileInformationService;
        $this->s3ClientService = $s3ClientService;
    }

    public function updateUserProfileImage(User $user, UploadedFile $file): void
    {
        $name = $this->nameForUploadedFile($file);
        $profileImageDirectory = $this->profileImageDirectoryForUser($user);
        $file->move($profileImageDirectory, $name);
        if ($user->profile_image) {
            $this->deleteFile($user->profile_image);
        }
        $user->profile_image = $profileImageDirectory.'/'.$name;

        $user->save();
    }

    public function updateUserBackgroundImage(User $user, UploadedFile $file): void
    {
        $name = $this->nameForUploadedFile($file);
        $imageDirectory = $this->backgroundImageDirectoryForUser($user);
        $file->move($imageDirectory, $name);
        if ($user->background_image) {
            $this->deleteFile($user->background_image);
        }
        $user->background_image = $imageDirectory.'/'.$name;

        $user->save();
    }

    public function updateAdVideo(Ad $ad, string $s3Url)
    {
        if ($ad->video) {
            $this->deleteFile($ad->video);
        }

        $ad->video = $s3Url;

        $ad->save();
    }

    public function updateUserCVVideo(User $user, string $s3Url): void
    {
        //@todo calculate duration

        $this->deleteUserProfileVideo($user->id);

        if (!$video = CvVideo::query()->where('user_id', $user->id)->first()) {
            $video = CvVideo::query()->create([
                'user_id' => $user->id,
                'created_by' => $user->id,
                'video_length' => 0
            ]);
        }

        $video->update([
            'video' => $s3Url,
            'modified_by' => $user->id
        ]);
    }

    public function updateCompanyVideo(User $company, string $s3Url): void
    {
        if ($company->company_video) {
            $this->deleteFile($company->company_video);
        }

        $company->company_video = $s3Url;

        $company->save();
    }

    public function deleteUserProfileVideo(int $userId): void
    {
        /** @var CvVideo $video */
        if (!$video = CvVideo::where('user_id', $userId)->first()) {
            return;
        }

        // User applied to a job with this video (don't delete the file)
        if (!AdSharedInfo::where('video', $video->video)->first()) {
            $this->deleteCvVideoFile($video);
        }

        $video->delete();
    }

    public function deleteUserCvDocument(int $userId): void
    {
        $document = UserDocument::query()
            ->where(['user_id' => $userId])
            ->orderBy('created_at', 'DESC')
            ->first()
        ;

        if (!$document) {
            return;
        }

        $applicationExists = AdSharedInfo::query()->where('cv', $document->document_link)->exists();
        if ($document->document_link && !$applicationExists) {
            $this->deleteFile($document->document_link);
        }

        $document->delete();
    }

    public function deleteFile(string $path): void
    {
        if ($this->s3ClientService->isS3Url($path)) {
            try {
                $this->s3ClientService->deleteObject($path);
            } catch (\Exception $ex) {
                $this->logger->error("Error deleting S3 object {$path}: {$ex->getMessage()}");
            }

            return;
        }

        $filePath = public_path().'/'.$path;
        if (!file_exists($filePath)) {
            $this->logger->error("Could not delete! File not found - path: {$filePath}");

            return;
        }

        try {
            unlink($filePath);
        } catch (\Throwable $ex) {
            $this->logger->error("Error removing file - path: {$filePath} error: {$ex->getMessage()} {$ex->getTraceAsString()}");
        }
    }

    public function deleteCvVideo(CvVideo $video): void
    {
        $this->deleteFile($video->video);
        $video->delete();
    }

    private function deleteCvVideoFile(CvVideo $video): void
    {
        if ($this->s3ClientService->isS3Url($video->video)) {
            try {
                $this->s3ClientService->deleteObject($video->video);
            } catch (\Exception $ex) {
                $this->logger->error("Error deleting S3 object {$video->video}: {$ex->getMessage()}");
            }

            return;
        }

        $filePath = $this->filePathForCvVideo($video);

        if (!file_exists($filePath)) {
            $this->logger->error("CV Video present in database but file not found - id: {$video->id} path: {$filePath}");

            return;
        }

        try {
            unlink($filePath);
        } catch (\Throwable $ex) {
            $this->logger->error("Error removing CV Video present - id: {$video->id} path: {$filePath} error: {$ex->getMessage()} {$ex->getTraceAsString()}");
        }
    }

    public function createVideo(User $user, UploadedFile $file): Video
    {
        $duration = $this->getSafeDuration($file);

        $s3Url = $this->s3ClientService->uploadVideo($file);

        return Video::query()->create([
            'path' => $s3Url,
            'created_by' => $user->id,
            'video_length' => $duration
        ]);
    }

    public function deleteVideo(Video $video): void
    {
        $this->deleteFile($video->path);
        $video->delete();
    }

    public function filePathForCvVideo(CvVideo $video): string
    {
        return sprintf(
            '%s/%s',
            public_path(),
            $video->video
        );
    }

    public function filePathForUserDocument(UserDocument $document): string
    {
        return sprintf(
            '%s/%s',
            public_path(),
            $document->document_link
        );
    }

    private function getSafeDuration(UploadedFile $file): int
    {
        try {
            return $this->fileInformationService->getDuration($file->getRealPath());
        } catch (\Exception $ex) {
            return 0;
        }
    }

    public function nameForUploadedFile(UploadedFile $file): string
    {
        return Uuid::uuid4()->toString().'.'.$file->getClientOriginalExtension();
    }

    private function profileImageDirectoryForUser(User $user): string
    {
        //@todo Should be in config
        return "user-data/images/profile/{$user->id}";
    }

    private function backgroundImageDirectoryForUser(User $user): string
    {
        //@todo Should be in config
        return "user-data/images/background/{$user->id}";
    }

    private function videoDirectoryForUser(User $user): string
    {
        //@todo Should be in config
        return "user-data/videos/{$user->id}";
    }

    private function adVideoDirectoryForUser(User $user): string
    {
        //@todo Should be in config
        return "user-data/ads/video/{$user->id}";
    }

    public static function buildUrlForCvVideo(?CvVideo $video): ?string
    {
        if (is_null($video)) {
            return null;
        }

        return self::buildUrlForMedia($video->video);
    }

    public static function buildUrlForMedia(?string $url): ?string
    {
        if (is_null($url)) {
            return null;
        }

        if (filter_var($url, FILTER_VALIDATE_URL)) {
            return $url;
        }

        return config('app.media_base_url').$url;
    }

    public static function imageRequestRuleArray(bool $isRequired = true): array
    {
        $data = [ 'file', 'image', 'mimes:'.implode(',', self::imageAllowedMimeTypes()), 'max:'.self::IMAGE_MAX_FILE_SIZE_IN_KB ];

        if ($isRequired) {
            $data[] = 'required';
        }

        return $data;
    }

    public static function imageAllowedMimeTypes(): array
    {
        return [ 'jpeg', 'png' ];
    }
}
