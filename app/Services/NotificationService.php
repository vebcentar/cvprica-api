<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Http\Resources\CityNotificationCollection;
use App\Http\Resources\CompanyNotificationCollection;
use App\Http\Resources\TypeOfWorkNotificationCollection;
use App\Models\Ad;
use App\Models\AdSharedInfo;
use App\Models\CityNotification;
use App\Models\CompanyNotification;
use App\Models\Notification;
use App\Models\TypeOfWorkNotification;
use App\Models\User;

class NotificationService
{
    public function sendNotificationForAd(Ad $ad): void
    {
        $userIds = $this->findUserIdsForNotifyingForAd($ad);

        if (0 === count($userIds)) {
            return;
        }

        $turnOnUserIds = User::whereIn('id', $userIds)->where('turn_notification', 1)->pluck('id')->toArray();

        if (0 === count($turnOnUserIds)) {
            return;
        }

        $notifications = [];
        foreach ($turnOnUserIds as $recipientId) {
            $notificationArray = [
                'title' => 'Novi oglas',
                'text' => 'Imamo novi oglas za Vas',
                'created_by' => $ad->company->id,
                'user_id' => $recipientId,
                'type' => Notification::TYPE_SINGLE_AD,
                'particular_id' => $ad->id,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $notifications[] = $notificationArray;
        }

        Notification::insert($notifications);
    }

    public function findUserIdsForNotifyingForAd(Ad $ad): array
    {
        $ids = array_merge(
            $this->findUserIdsForNotifyingForCityId($ad->city_id),
            $this->findUserIdsForNotifyingForCompanyId($ad->company->id),
            $this->findUserIdsForNotifyingForTypeOfWorkId($ad->type_of_work->id),
        );

        return array_unique($ids);
    }

    public function findUserIdsForNotifyingForTypeOfWorkId(int $typeOfWorkId): array
    {
        return TypeOfWorkNotification::query()
            ->where('type_of_work_id', $typeOfWorkId)
            ->pluck('user_id')
            ->toArray()
        ;
    }

    public function findUserIdsForNotifyingForCityId(int $cityId): array
    {
        return CityNotification::query()
            ->where('city_id', $cityId)
            ->pluck('user_id')
            ->toArray()
        ;
    }

    public function findUserIdsForNotifyingForCompanyId(int $cityId): array
    {
        return CompanyNotification::query()
            ->where('company_id', $cityId)
            ->pluck('user_id')
            ->toArray()
        ;
    }

    public function allNotificationsForUser(User $user): array
    {
        $this->validateUser($user);

        return [
            'cities' => $this->getCityNotificationsForUser($user),
            'companies' => $this->getCompanyNotificationsForUser($user),
            'types_of_work' => $this->getTypeOfWorkNotificationsForUser($user),
        ];
    }

    public function getCityNotificationsForUser(User $user): CityNotificationCollection
    {
        $this->validateUser($user);

        $cityNotifications = CityNotification::query()
            ->with([ 'city', 'user' ])
            ->where('user_id', $user->id)
            ->get()
        ;

        return new CityNotificationCollection($cityNotifications);
    }

    public function getTypeOfWorkNotificationsForUser(User $user): TypeOfWorkNotificationCollection
    {
        $this->validateUser($user);

        $typeOfWorkNotifications = TypeOfWorkNotification::query()
            ->with([ 'type_of_work', 'user' ])
            ->where('user_id', $user->id)
            ->get()
        ;

        return new TypeOfWorkNotificationCollection($typeOfWorkNotifications);
    }

    public function getCompanyNotificationsForUser(User $user): CompanyNotificationCollection
    {
        $this->validateUser($user);

        $companyNotifications = CompanyNotification::query()
            ->with([ 'company', 'user' ])
            ->where('user_id', $user->id)
            ->get()
        ;

        return new CompanyNotificationCollection($companyNotifications);
    }

    public function isUserSubscribedToCompany(int $userId, int $companyId): bool
    {
        return CompanyNotification::query()
            ->where('user_id', $userId)
            ->where('company_id', $companyId)
            ->exists()
        ;
    }

    public function subscribeUserToCompany(User $user, User $company): CompanyNotification
    {
        $this->validateUser($user);

        if (!$company->isCompany()) {
            throw new BusinessLogicException('Must subscribe to a company');
        }

        $subscription = CompanyNotification::query()
            ->where('user_id', '=', $user->id)
            ->where('company_id', '=', $company->id)
            ->first()
        ;

        if ($subscription) {
            throw new BusinessLogicException('Already subscribed');
        }

        return CompanyNotification::create([
            'user_id' => $user->id,
            'company_id' => $company->id
        ]);
    }

    public function unsubscribeUserFromCompany(User $user, User $company): void
    {
        $this->validateUser($user);

        if (!$company->isCompany()) {
            throw new BusinessLogicException('Must subscribe to a company');
        }

        $subscription = CompanyNotification::query()
            ->where('user_id', '=', $user->id)
            ->where('company_id', '=', $company->id)
            ->first()
        ;

        if (!$subscription) {
            throw new BusinessLogicException('Subscription not found');
        }

        $subscription->delete();
    }

    public function sendNotificationForNewApplication(AdSharedInfo $application): void
    {
        $ad = $application->ad;

        Notification::query()->create([
            'user_id'   =>  $ad->user_id,
            'title'   =>  $ad->title.' - Nova prijava',
            'text'   =>  'Imate novu prijavu na oglas posla: '.$ad->title,
            'seen'   =>  0,
            'type'  =>  'new_candidate_application',
            'particular_id'   =>  $application->id,
        ]);
    }

    public function sendNotificationForNewUser(User $user): void
    {
        $title = $user->isCandidate() ? 'Popunite svoj profil prije konkurisanja!' : 'Popunite svoj profil!';

        Notification::query()->create([
            'title' => $title,
            'text' => '',
            'created_by' => null,
            'user_id' => $user->id,
            'type' => Notification::TYPE_EDIT_PROFILE_NOTIFICATION,
            'particular_id' => $user->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    private function validateUser(User $user): void
    {
        if (!$user->isCandidate()) {
            throw new BusinessLogicException('Must be a candidate');
        }
    }
}
