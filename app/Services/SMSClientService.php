<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use Psr\Log\LoggerInterface;

class SMSClientService
{
    private string $username;

    private string $password;

    private string $baseUrl;

    private int $port;

    private string $sender;

    private LoggerInterface $logger;

    public function __construct(string $username, string $password, string $baseUrl, int $port, string $sender, LoggerInterface $logger)
    {
        $this->username = $username;
        $this->password = $password;
        $this->baseUrl = $baseUrl;
        $this->port = $port;
        $this->sender = $sender;
        $this->logger = $logger;
    }

    public function sendSMS(string $phoneNumber, string $text): void
    {
        if (0 === strpos($phoneNumber, '+')) {
            $phoneNumber = '00'.substr($phoneNumber, 1);
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => sprintf('%s:%d/api/message', $this->baseUrl, $this->port),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "sender": "'.$this->sender.'",
                "phoneNumber": "'.$phoneNumber.'",
                "text":"'.$text.'",
                "priority":"HIGH",
                "validity":60,
                "statusReport":7
            }',
            CURLOPT_HTTPHEADER => [
                'Authorization: Basic '.base64_encode("{$this->username}:{$this->password}"),
                'Content-Type: application/json',
                'Accept: application/json'
            ],
        ]);

        $result = curl_exec($curl);
        curl_close($curl);

        if (false === $result) {
            $this->logger->error('Error sending OTP: '.curl_error($curl));

            throw new BusinessLogicException('Error sending OTP');
        }
    }
}
