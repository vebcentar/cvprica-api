<?php

namespace App\Services;

use App\Http\Resources\MessageCollection;
use App\Repositories\MessageRepository;

class ChatService
{
    private MessageRepository $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function conversationsForUser(int $userId, int $limit = 20, int $offset = 0, string $term = null): MessageCollection
    {
        $messageArray = $this->messageRepository->conversationsForUser(
            $userId,
            $limit,
            $offset,
            $term
        );
        return (new MessageCollection($messageArray['messages']))
            ->additional(['meta' => [ 'total' => $messageArray['total'] ]])
        ;
    }

    public function conversationBetweenTwoUsers(int $firstUserId, int $secondUserId, int $limit, int $fromMessageId = null): MessageCollection
    {
        $messageArray = $this->messageRepository->conversationBetweenTwoUsers(
            $firstUserId,
            $secondUserId,
            $limit,
            $fromMessageId
        );

        return (new MessageCollection($messageArray['messages']))
            ->additional(['meta' => [ 'total' => $messageArray['total'] ]])
        ;
    }


}
