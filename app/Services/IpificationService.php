<?php

namespace App\Services;

use App\Exceptions\BusinessLogicException;
use App\Exceptions\ValidationException;
use App\Models\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class IpificationService
{
    private string $baseUrl;

    private string $clientId;

    private string $clientSecret;

    public function __construct(string $baseUrl, string $clientId, string $clientSecret)
    {
        $this->baseUrl = $baseUrl;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function generateVerificationUrl(User $user): string
    {
        return sprintf(
            '%s/%s?response_type=code&client_id=%s&redirect_uri=%s&scope=%s&state=%s&login_hint=%s',
            $this->baseUrl,
            'auth/realms/ipification/protocol/openid-connect/auth',
            $this->clientId,
            $this->redirectUrl(),
            $this->scope(),
            $this->generateStateForUser($user),
            $user->phoneToE164()
        );
    }

    public function generateStateForUser(User $user): string
    {
        $user->validateUserCanVerifyPhone();

        $valueToEncode = sprintf(
            "%s.%s.%s",
            $user->id,
            str_replace('_', '', $user->phone),
            (new \DateTimeImmutable())->format(\DateTimeInterface::RFC3339_EXTENDED)
        );

        return Crypt::encryptString($valueToEncode);
    }

    public function decryptState(string $state): array
    {
        try {
            $decrypted = Crypt::decryptString($state);
        } catch (DecryptException $e) {
            throw new ValidationException('State not valid');
        }

        $values = explode('_', $decrypted);

        if (count($values) !== 3) {
            throw new ValidationException('State not valid');
        }

        /** @var User $user */
        if (!$user = User::query()->find($values[0])) {
            throw new ValidationException('User not found');
        }

        $phone = trim($values[1]);

        if (!$date = \DateTimeImmutable::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, $values[2])) {
            throw new ValidationException('Date created not valid');
        }

        if ($date < new \DateTimeImmutable('now - 1 hour')) {
            throw new ValidationException('State expired');
        }

        return [
            'user' => $user,
            'phone' => $phone,
            'date' => $date
        ];
    }

    public function validateCode(string $code)
    {

    }

    private function redirectUrl()
    {
        return 'https://cvprica.me';
    }

    private function scope(): string
    {
        return 'ip:phone_verify';
    }
}
