<?php

namespace App\Providers;

use App\Services\Firebase\Client\FirebaseClient;
use App\Services\Firebase\Client\FirebaseClientInterface;
use App\Services\Firebase\Client\NullFirebaseClient;
use App\Services\IpificationService;
use App\Services\S3ClientService;
use App\Services\SMSClientService;
use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(IpificationService::class)
            ->needs('$clientId')
            ->give(config('app.ipification.client_id'))
        ;
        $this->app->when(IpificationService::class)
            ->needs('$clientSecret')
            ->give(config('app.ipification.client_secret'))
        ;
        $this->app->when(IpificationService::class)
            ->needs('$baseUrl')
            ->give(config('app.ipification.base_url'))
        ;

        $this->app->when(SMSClientService::class)
            ->needs('$username')
            ->give(config('app.a2p_sms_gateway.user'))
        ;
        $this->app->when(SMSClientService::class)
            ->needs('$password')
            ->give(config('app.a2p_sms_gateway.pass'))
        ;
        $this->app->when(SMSClientService::class)
            ->needs('$baseUrl')
            ->give(config('app.a2p_sms_gateway.base_url'))
        ;
        $this->app->when(SMSClientService::class)
            ->needs('$port')
            ->give(config('app.a2p_sms_gateway.port'))
        ;
        $this->app->when(SMSClientService::class)
            ->needs('$sender')
            ->give(config('app.a2p_sms_gateway.sender'))
        ;

        $firebaseClientClass = env('FIREBASE_CREDENTIALS') ?
            FirebaseClient::class :
            NullFirebaseClient::class
        ;

        $this->app->bind(FirebaseClientInterface::class, $firebaseClientClass);

        $this->app->bind(S3ClientInterface::class, function () {
            return new S3Client([
                'version' => 'latest',
                'region' => config('app.aws.s3.region', 'eu-central-1'),
                'credentials' => [
                    'key' => config('app.aws.s3.credentials.key'),
                    'secret' => config('app.aws.s3.credentials.secret'),
                ]
            ]);
        });

        $this->app->when(S3ClientService::class)
            ->needs('$bucketName')
            ->give(config('app.aws.s3.bucket'))
        ;
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
