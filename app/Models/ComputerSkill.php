<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComputerSkill extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'computer_skill_name_id',
        'computer_skill_knowledge_level_id',
        'created_by',
        'modified_by'
    ];


    public function computer_skill_name() {
        return $this->belongsTo(ComputerSkillName::class);
    }

    public function computer_skill_knowledge_level() {
        return $this->belongsTo(ComputerSkillKnowledgeLevel::class);
    }
}
