<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfWork extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'created_by',
        'created_by',
        'modified_by'
    ];

    public function activeAds(){
        return $this->hasMany(Ad::class, 'type_of_work_id')
            ->where('is_active', 1)
            ->where('is_archived', 0)
            ->where('visible', 1)
            ->where('end_date','>=', date('Y-m-d'))
        ;
    }
}
