<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Video extends Model
{
    use HasFactory;

    const MAX_FILE_SIZE_IN_KB = 51200;

    protected $fillable = [
        'id',
        'path',
        'video_length',
        'created_by',
        'created_at'
    ];

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = ['created_at' => 'immutable_datetime' ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4());
            $model->setAttribute('created_at', new \DateTimeImmutable('now'));
        });
    }

    public static function requestRuleArray(bool $isRequired = true): array
    {
        $data = [ 'file', 'mimes:'.implode(',', self::allowedMimeTypes()), 'max:'.self::MAX_FILE_SIZE_IN_KB ];

        if ($isRequired) {
            $data[] = 'required';
        }

        return $data;
    }

    public static function allowedMimeTypes(): array
    {
        return [ 'mp4', 'mov', 'hevc', 'm4v', 'qt', 'webm', 'mkv' ];
    }
}
