<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    const STARTER = 'Starter';
    const BASIC = 'Basic';
    const PLUS = 'Plus';
    const PRO = 'Pro';
    const ENTERPRISE = 'Enterprise';

    const ID_STARTER = 1;
    const ID_BASIC = 2;
    const ID_PLUS = 3;
    const ID_PRO = 4;
    const ID_ENTERPRISE = 5;

    protected $fillable = [
        'name',
        'description',
        'benefits',
        'active'
    ];

    protected $casts = [
        'benefits' => 'array'
    ];

    public function pricing()
    {
        return $this->hasMany(Pricing::class);
    }

    public static function buildDiscounts(): array
    {
        $discounts = [];

        foreach (self::discountValues() as $adCount => $amount) {
            $discounts[] = [
                'ad_count' => $adCount,
                'title' => sprintf('Do %d oglasa godišnje', $adCount),
                'type' => 'percent',
                'amount' => $amount
            ];
        }

        return $discounts;
    }

    public static function packages(): array
    {
        return [
            self::ID_STARTER => self::STARTER,
            self::ID_BASIC => self::BASIC,
            self::ID_PLUS => self::PLUS,
            self::ID_PRO => self::PRO,
            self::ID_ENTERPRISE => self::ENTERPRISE,
        ];
    }

    public static function idForPackage(string $packageName)
    {
        return array_search($packageName, self::packages());
    }

    private static function discountValues(): array
    {
        return [
            5 => 15,
            10 => 20,
            20 => 25,
            30 => 30,
            50 => 35,
            100 => 50,
        ];
    }
}
