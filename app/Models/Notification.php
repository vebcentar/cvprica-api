<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    const TYPE_SINGLE_AD = 'single_ad';
    const TYPE_EDIT_PROFILE_NOTIFICATION = 'edit_profile_notification';

    protected $fillable = [
        'user_id',
        'sender_id',
        'title',
        'text',
        'seen',
        'created_by',
        'modified_by',
        'type',
        'particular_id'
    ];
}
