<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CvVideo extends Model
{
    use HasFactory;

    const MAX_FILE_SIZE_IN_KB = 51200;

    protected $fillable = [
        'user_id',
        'video',
        'description',
        'created_by',
        'modified_by',
        'video_length'
    ];

    public static function requestRuleArray(bool $isRequired = true): array
    {
        $data = [ 'file', 'mimes:'.implode(',', self::allowedMimeTypes()), 'max:'.self::MAX_FILE_SIZE_IN_KB ];

        if ($isRequired) {
            $data[] = 'required';
        }

        return $data;
    }

    public static function allowedMimeTypes(): array
    {
        return Video::allowedMimeTypes();
    }
}
