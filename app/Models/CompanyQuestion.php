<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
        'question',
        'created_by',
        'modified_by'
    ];

    public function company()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updateQuestion(string $question)
    {
        $this->attributes['question'] = $question;
    }
}
