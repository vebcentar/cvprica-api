<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfWorkNotification extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type_of_work_id'
    ];

    public function type_of_work()
    {
        return $this->belongsTo(TypeOfWork::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
