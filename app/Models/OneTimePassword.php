<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class OneTimePassword extends Model
{
    use HasFactory;

    const TYPE_PHONE = 'phone';
    const TYPE_EMAIL = 'email';

    protected $fillable = [
        'id',
        'code',
        'type',
        'identifier',
        'used_on',
        'valid_until'
    ];

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        'used_on' => 'immutable_datetime',
        'valid_until' => 'immutable_datetime'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4());
        });
    }

    public function isUsed(): bool
    {
        return !\is_null($this->used_on);
    }

    public function isExpired(): bool
    {
        return (new \DateTimeImmutable('now')) >= $this->valid_until;
    }

    public function isCodeValid(string $code): bool
    {
        return $code === $this->code;
    }

    public function isEmail(): bool
    {
        return self::TYPE_EMAIL === $this->type;
    }

    public function isPhone(): bool
    {
        return self::TYPE_PHONE === $this->type;
    }
}
