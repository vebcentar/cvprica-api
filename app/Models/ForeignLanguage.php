<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignLanguage extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'languages_id',
        'language_reads_id',
        'language_writes_id',
        'language_speaks_id',
        'created_by',
        'modified_by'
    ];

    public function language()
    {
        return $this->belongsTo(Language::class, 'languages_id');
    }

    public function languages()
    {
        return $this->belongsTo(Language::class, 'languages_id');
    }

    public function language_read()
    {
        return $this->belongsTo(LanguageRead::class, 'language_reads_id');
    }

    public function language_reads()
    {
        return $this->belongsTo(LanguageRead::class, 'language_reads_id');
    }

    public function language_write()
    {
        return $this->belongsTo(LanguageWrite::class, 'language_writes_id');
    }

    public function language_writes()
    {
        return $this->belongsTo(LanguageWrite::class, 'language_writes_id');
    }

    public function language_speak()
    {
        return $this->belongsTo(LanguageSpeak::class, 'language_speaks_id');
    }

    public function language_speaks()
    {
        return $this->belongsTo(LanguageSpeak::class, 'language_speaks_id');
    }
}
