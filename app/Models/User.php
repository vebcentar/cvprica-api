<?php

namespace App\Models;

use App\Exceptions\BusinessLogicException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Scopes\OrderScope;

class User extends Authenticatable
{
    const ROLE_ADMIN = 'admin';
    const ROLE_COMPANY = 'company';
    const ROLE_CANDIDATE = 'employee';

    const DEFAULT_PROFILE_COMPLETENESS = 11;

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email',
        'password',
        'role',
        'profile_image',
        'phone',
        'birth_year',
        'aditional_info',
        'gender_id',
        'is_active',
        'company_activity',
        'company_description',
        'company_video',
        'number_of_employees_from',
        'number_of_employees_to',
        'pib',
        'pdv',
        'package_id',
        'country_id',
        'city_id',
        'address',
        'zip_code',
        'deleted',
        'created_by',
        'deleted_by',
        'modified_by',
        'is_archived',
        'education_level_id',
        'turn_notification',
        'facebook',
        'instagram',
        'linkedin',
        'website',
        'language_id',
        'background_image',
        'google_id',
        'apple_id',
        'facebook_id',
        'linkedin_id',
        'contact_email',
        'selection_time',
        'selection_type',
        'selection_circles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'birth_year' => 'date',
    ];


    protected static function booted()
    {
        static::addGlobalScope(new OrderScope('priority', 'desc'));
    }

    public function setArchived($value = 1)
    {
        if($value == null)
            $value = 1;
        $this->attributes['is_active'] = ! $value;
        $this->attributes['is_archived'] = $value;
        $this->save();
    }

    public function reset()
    {
        $this->attributes['is_active'] = 1;
        $this->attributes['is_archived'] = 0;
        $this->save();
    }

    public function setEminent(bool $isEminent): User
    {
        $this->attributes['eminent'] = $isEminent;

        return $this;
    }

    public function setPasswordAttribute($value)
    {
        $value ? $this->attributes['password'] = bcrypt($value) : NUll;
    }

    public function setPhoneAttribute($value)
    {
        $pattern = '/^00+/';
        $replacement = '+';
        $this->attributes['phone'] = preg_replace($pattern, $replacement, $value);
        if(substr($this->attributes['phone'], 0, 1) !== '+')
            $this->attributes['phone'] = '+'.$this->attributes['phone'];
    }

    public function setBirthYearAttribute($value)
    {
        $this->attributes['birth_year'] = \Carbon\Carbon::parse($value);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }


    public function adsForMe()
    {
        return Ad::query();
    }

    public function activeAds()
    {
        return $this->hasMany(Ad::class, 'user_id')
            ->where('is_active', 1)
            ->where('is_archived', 0)
            ->where('visible', 1)
            ->where('end_date','>=', date('Y-m-d'))
        ;
    }

    public function activeAdsList()
    {
        return $this->hasMany(Ad::class, "user_id")->with(['city', 'country'])->where("is_active", 1)->where("is_archived", 0)->where("end_date",">=", date('Y-m-d'))->limit(5);
    }

    //@todo Investigate is it used
    public function ad()
    {
        return $this->hasOne(Ad::class)->where("is_active", 1)->where("is_archived", 0)->latest();
    }

    public function latestActiveAd()
    {
        return $this
            ->hasOne(Ad::class)
            ->where("is_active", 1)
            ->where("is_archived", 0)
            ->where('end_date','>=', date('Y-m-d'))
            ->where('visible', 1)
            ->latest()
        ;
    }

    public function applications()
    {
        return $this->hasMany(AdSharedInfo::class, "user_id");
    }

    public function packages(){
        return $this->belongsTo(Package::class, "package_id");
    }

    public function company_users(){
        return $this->hasMany(CompanyUser::class, "user_id");
    }

    public function company_activities(){
        return $this->belongsTo(CompanyActivity::class, "company_activity");
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function uiLanguage(){
        return $this->belongsTo(UserLanguage::class, "language_id");
    }

    public function driver_licences(){
        return $this->hasMany(DriversLicence::class, "user_id");
    }

    public function additional_information(){
        return $this->hasMany(AdditionalInformation::class, "user_id");
    }

    public function notifications() {
        return $this->hasMany(Notification::class)->orderBy('created_at', 'desc');
        // return $this->hasMany(Notification::class)->orderBy('created_at', 'desc')->limit(5);
    }


    public function messages(){
        return $this->hasMany(Message::class)->orderBy('created_at', 'desc');
        // return $this->hasMany(Message::class)->orderBy('created_at', 'desc')->limit(5);
    }

    public function computer_skills(){
        return $this->hasMany(ComputerSkill::class);
    }
    public function educations(){
        return $this->hasMany(Education::class);
    }

    public function education_level()
    {
        return $this->belongsTo(EducationLevel::class);
    }

    public function languages(){
        return $this->hasMany(ForeignLanguage::class);
    }
    public function videos(){
        return $this->hasOne(CvVideo::class)->latest();
    }

    public function work_experiences(){
        return $this->hasMany(WorkExperience::class);
    }

    public function cv_video()
    {
        return $this->hasOne(CvVideo::class)->latest();
    }

    public function documents(){
        return $this->hasMany(UserDocument::class, "user_id");
    }

    public function cv_document(){
        return $this->hasMany(UserDocument::class, "user_id")->latest();
    }

    public function desiredCities(){
        return $this->hasMany(DesireCity::class, "user_id");
    }

    public function city_notifications() {
        return $this->hasMany(CityNotification::class, "user_id");
    }

    public function type_of_work_notifications() {
        return $this->hasMany(TypeOfWorkNotification::class, "user_id");
    }

    public function desiredJobs(){
        return $this->hasMany(DesireJobs::class, "user_id");
    }

    public function companyQuestions()
    {
        return $this->hasMany(CompanyQuestion::class, 'created_by')->orderBy('updated_at', 'DESC');
    }

    public function getProfileCompletelyAttribute()
    {
        if ($this->isAdmin()) {
            return 100;
        }

        $fields = $this->profileCompletenessFields();

        if (count($fields) === 0) {
            return self::DEFAULT_PROFILE_COMPLETENESS;
        }

        $progress = 0;

        foreach ($fields as $field) {
            if ($field) {
                $progress++;
            }
        }

        $percent = floatval(number_format($progress / count($fields) * 100));

        if ($this->isCompany()) {
            return $percent;
        }

        $percent *= 0.8;

        if ($this->cv_video) {
            $percent += 20;
        }

        return floatval(number_format($percent));
    }

    protected function profileCompletenessFields(): array
    {
        if ($this->isCandidate()) {
            return $this->candidateProfileCompletenessFields();
        }

        if ($this->isCompany()) {
            return $this->companyProfileCompletenessFields();
        }

        return [];
    }

    protected function companyProfileCompletenessFields(): array
    {
        return [
            $this->full_name,
            $this->company_activity,
            $this->company_description,
            $this->profile_image,
            $this->background_image,
            $this->email,
            $this->email_verified_at,
            $this->phone,
            $this->phone_verified_at,
            $this->country_id,
            $this->city_id,
            $this->address,
            $this->pib
        ];
    }

    protected function candidateProfileCompletenessFields(): array
    {
        return [
            $this->full_name,
            $this->gender_id,
            $this->birth_year,
            $this->education_level,
            $this->email,
            $this->email_verified_at,
            $this->phone,
            $this->phone_verified_at,
            $this->country_id,
            $this->city_id,
            $this->address,
            $this->zip_code,
            $this->educations()->count(),
            $this->profile_image,
        ];
    }

    public function isCompany(): bool
    {
        return User::ROLE_COMPANY === $this->role;
    }

    public function isCandidate(): bool
    {
        return User::ROLE_CANDIDATE === $this->role;
    }

    public function isAdmin(): bool
    {
        return User::ROLE_ADMIN === $this->role;
    }

    public function isRegisteredViaSocialNetwork() : bool
    {
        return ($this->google_id || $this->apple_id || $this->facebook_id || $this->linkedin_id) ? true : false;
    }

    public function hasRole(string $role): bool
    {
        return $this->role === $role;
    }

    public function isPhoneAndEmailVerified(): bool
    {
        return $this->isEmailVerified() && $this->isPhoneVerified();
    }

    public function isEmailVerified(): bool
    {
        return !\is_null($this->email_verified_at);
    }

    public function isPhoneVerified(): bool
    {
        return !\is_null($this->phone) && !\is_null($this->phone_verified_at);
    }

    public function phoneToE164(): ?string
    {
        if (!$phone = $this->phone) {
            return null;
        }

        if (0 === strpos($phone, '+')) {
            $phone = '11'.substr($phone, 1);
        }

        $phone = preg_replace( '/[^0-9 ]/i', '', $phone);

        return $phone;
    }

    public function hasDeviceToken(): bool
    {
        return !\is_null($this->device_token);
    }

    public function isProfileCompleted(): bool
    {
        return 100 === (int) $this->getProfileCompletelyAttribute();
    }

    public function eraseDeviceToken(): void
    {
        $this->device_token = null;
    }

    public function setDeviceToken(string $deviceToken): void
    {
        $this->device_token = $deviceToken;
    }

    public function validateUserCanVerifyPhone(): void
    {
        if (!$this->phone) {
            throw new BusinessLogicException('User has no phone');
        }

        if ($this->phone_verified_at) {
            throw new BusinessLogicException('Phone is already verified');
        }
    }

    public static function phoneRulesArray(bool $required = false, bool $nullable = false): array
    {
        $rules = [ 'string', 'regex:/^[+]{1}[0-9]{7,15}/', 'min:12', 'max:16' ];

        if ($required) {
            $rules[] = 'required';
        }

        if ($nullable) {
            $rules[] = 'nullable';
        }

        return $rules;
    }
}
