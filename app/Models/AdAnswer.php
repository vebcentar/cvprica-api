<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ad_question_id',
        'text_answer',
        'video_id',
        'created_by',
        'modified_by'
    ];

    protected $with = [ 'video' ];

    public function ad_question()
    {
        return $this->belongsTo(AdQuestion::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
