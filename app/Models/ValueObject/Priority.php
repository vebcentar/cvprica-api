<?php

namespace App\Models\ValueObject;

final class Priority
{
    const LOW = 'low';
    const NORMAL = 'normal';
    const HIGH = 'high';

    private string $value;

    public function __construct(string $value)
    {
        if (false === self::isValid($value)) {
            throw new \UnexpectedValueException(sprintf('Value "%s" is not valid value of class %s', $value, Priority::class));
        }
        $this->value = $value;
    }

    public static function low(): Priority
    {
        return new self(self::LOW);
    }

    public static function normal(): Priority
    {
        return new self(self::NORMAL);
    }

    public static function high(): Priority
    {
        return new self(self::HIGH);
    }

    public static function isValid(string $value): bool
    {
        return in_array($value, self::allValues());
    }

    public static function allValues(): array
    {
        return [
            self::LOW,
            self::NORMAL,
            self::HIGH,
        ];
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function isLow(): bool
    {
        return self::LOW === $this->value;
    }

    public function isNormal(): bool
    {
        return self::NORMAL === $this->value;
    }

    public function isHigh(): bool
    {
        return self::HIGH === $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
