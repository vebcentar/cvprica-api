<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Observers\AdSharedInfoObserver;

class AdSharedInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ad_id',
        'reminder',
        'favourite',
        'applied',
        'selected',
        'created_by',
        'modified_by',
        'seen',
        'viewed',
        'cv',
        'video',
        'full_name',
        'email',
        'profile_image',
        'phone',
        'education',
        'languages',
        'computer_skills',
        'experience',
        'additional_information',
        'gender_id',
        'country_id',
        'city_id',
        'address'
    ];

    // public function boot()
    // {
    //     AdSharedInfo::observe(AdSharedInfoObserver::class);
    // }

    protected $appends = [
        'status',
    ];

    public function ad(){
        return $this->belongsTo(Ad::class);
    }

    public function user(){
        return $this->belongsTo(User::class, "user_id");
    }

    public function scopeSearch($query, $searchTerm) {
        return $query
            ->where('full_name', 'like', "%" . $searchTerm . "%");
    }

    public function setEminent($value = 1)
    {
        if($value == null)
            $value = 1;
        $this->attributes['eminent'] = $value;
        $this->save();
    }

    public function getStatusAttribute(): int
    {
        return $this->ad && Carbon::parse($this->ad->end_date) >= Carbon::now() ? 1 : 0;
    }
}
