<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    const DURATION_DAYS_15 = 15;
    const DURATION_DAYS_30 = 30;

    use HasFactory;

    protected $fillable = [
        'price',
        'duration',
        'package_id'
    ];

    protected $appends = ['discount_prices'];

    protected $table = 'pricing';

    public function getDiscountPricesAttribute()
    {
        return $this->calculateDiscountPrices();
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    private function calculateDiscountPrices(): array
    {
        $discountPrices = [];

        foreach (Package::buildDiscounts() as $discount) {
            $discountPrices[] = [
                'ad_count' => $discount['ad_count'],
                'discounted_price' => ($this->price * $discount['ad_count'] * (100 - $discount['amount']) / 100)
            ];
        }

        return $discountPrices;
    }
}
