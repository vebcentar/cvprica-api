<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVATED = 2;
    const STATUS_ARCHIVED = 3;
    const STATUS_UNPUBLISHED = 4;
    const STATUS_DRAFT = 5;
    const STATUS_EXPIRED = 6;

    const MAX_NUMBER_OF_QUESTIONS = 10;

    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'location',
        'video',
        'number_of_applications',
        'start_date',
        'end_date',
        'is_active',
        'is_archived',
        'city_id',
        'country_id',
        'type_of_work_id',
        'deleted',
        'created_by',
        'modified_by',
        'deleted_by',
        'position',
        'job_type_id',
        'work_time_id',
        'education_level_id',
        'short_description',
        'visible'
    ];


    protected $casts = [
        'published' => 'boolean',
    ];

    protected $appends = [
        'published',
        'status',
        'ready_to_publish',
    ];

    public static function isStatusValid(int $status): bool
    {
        return $status >= 1 && $status <= 6;
    }

    public function setArchived($value = 1)
    {
        if ($value == null) {
            $value = 1;
        }
        $this->attributes['is_archived'] = $value;

        if ($value) {
            $this->attributes['is_active'] = 0;
        }
        $this->save();
    }

    public function reset()
    {
        $this->attributes['is_archived'] = 0;
        $this->save();
    }

    public function setHidden($value = 1)
    {
        if($value == null)
            $value = 1;
        $this->attributes['is_hidden'] = $value;
        $this->save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function job_category()
    {
        return $this->belongsTo(JobCategory::class);
    }

    public function getStatusAttribute()
    {
        if($this->is_active == 1 && $this->is_archived == 0 && $this->is_hidden == 0 && $this->end_date >= date('Y-m-d')) {
            return self::STATUS_ACTIVE;
        } elseif($this->is_active == 1 && $this->is_archived == 0 && $this->is_hidden == 1) {
            return self::STATUS_DEACTIVATED;
        } elseif($this->is_active == 1 && $this->is_archived == 1) {
            return self::STATUS_ARCHIVED;
        } elseif($this->is_active == 0 && $this->ready_to_publish == 1) {
            return self::STATUS_UNPUBLISHED;
        } elseif($this->is_active == 0 && $this->ready_to_publish == 0) {
            return self::STATUS_DRAFT;
        } elseif($this->is_active == 1 && $this->is_archived == 0 && $this->end_date < date('Y-m-d')) {
            return self::STATUS_EXPIRED;
        }
        return 0;
    }

    public function getPublishedAttribute()
    {
        if($this->is_active == 1 && $this->is_archived == 0 && (
            \Carbon\Carbon::parse($this->start_date) <= \Carbon\Carbon::now() &&
            \Carbon\Carbon::parse($this->end_date) >= \Carbon\Carbon::now()))
            return true;
        return false;
    }

    public function getReadyToPublishAttribute()
    {
        if($this->title && $this->country_id && $this->city_id && $this->type_of_work_id && $this->job_type_id && $this->work_time_id && $this->description && $this->start_date && $this->end_date)
            return true;
        return false;
    }

    public function isPublished(): bool
    {
        return 1 === $this->getStatusAttribute();
    }

    public function isActive(): bool
    {
        return 1 === $this->is_active;
    }

    public function deactivate(): void
    {
        $this->attributes['is_active'] = 0;
    }

    public function activate(): void
    {
        $this->attributes['is_active'] = 1;
    }

    public function getNumberOfApplicationsAttribute()
    {
        return $this->shared_adds()->where('ad_shared_infos.applied', 1)->count();
    }

    public function creator(){
        return $this->belongsTo(User::class, "user_id");
    }

    public function company(){
        return $this->creator();
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function type_of_work(){
        return $this->belongsTo(TypeOfWork::class, "type_of_work_id");
    }

    public function shared_adds()
    {
        return $this->hasMany(AdSharedInfo::class);
    }

    public function applications()
    {
        return $this->hasMany(AdSharedInfo::class);
    }

    public function answers(){
        return $this->hasMany(AdAnswer::class);
    }

    public function questions(){
        return $this->hasMany(AdQuestion::class);
    }

    public function favorites() {
        return $this->hasMany(Favorite::class);
    }

    public function scopeSearch($query, $searchTerm) {
        return $query
            ->where('title', 'like', "%" . $searchTerm . "%")
            ->orWhere('description', 'like', "%" . $searchTerm . "%");
    }
}
