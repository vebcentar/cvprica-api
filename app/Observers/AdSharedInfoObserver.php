<?php

namespace App\Observers;

use App\Models\AdSharedInfo;
use App\Models\Notification;

class AdSharedInfoObserver
{
    /**
     * Handle the AdSharedInfo "created" event.
     *
     * @param  \App\Models\AdSharedInfo  $adSharedInfo
     * @return void
     */
    public function created(AdSharedInfo $adSharedInfo)
    {
        //
    }

    /**
     * Handle the AdSharedInfo "updated" event.
     *
     * @param  \App\Models\AdSharedInfo  $adSharedInfo
     * @return void
     */
    public function updated(AdSharedInfo $adSharedInfo)
    {
        //
    }

    /**
     * Handle the AdSharedInfo "deleted" event.
     *
     * @param  \App\Models\AdSharedInfo  $adSharedInfo
     * @return void
     */
    public function deleted(AdSharedInfo $adSharedInfo)
    {
        //
    }

    /**
     * Handle the AdSharedInfo "restored" event.
     *
     * @param  \App\Models\AdSharedInfo  $adSharedInfo
     * @return void
     */
    public function restored(AdSharedInfo $adSharedInfo)
    {
        //
    }

    /**
     * Handle the AdSharedInfo "force deleted" event.
     *
     * @param  \App\Models\AdSharedInfo  $adSharedInfo
     * @return void
     */
    public function forceDeleted(AdSharedInfo $adSharedInfo)
    {
        //
    }
}
