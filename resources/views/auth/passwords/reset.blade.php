<!DOCTYPE html>
<html>
<head>
    <title>Reset password</title>
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap" rel="stylesheet">
</head>
<body>
<script>
    window.onload = function() {
        document.getElementById("password").focus();
    }
</script>
<div class="main">
    <input type="checkbox" id="chk" aria-hidden="true">

    <div class="signup">
            <label >
                <img style="width: 300px;" src="{{ asset('img/logo.png') }}">
            </label>
            <p style="color: #149bff; text-align: center; font-size: 32px;">Unesite novu lozinku!</p>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input id="password" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Nova lozinka">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input id="password-confirm" type="password" class="input form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Ponovite lozinku">
                <button type="submit" style="background-color:#1f7dd5; background-image: linear-gradient(to top, #1f252aab, #0c74f5 ); ">POTVRDITE</button>
            </form>
        <hr style="margin: 20px 0px;">
        <div class="login">
            <form>
                <p style="margin: 5px; color: #51565a;">Niste poslali zahtjev za novu lozinku?</p>
                <a style="color: #149bff; text-decoration: none; font-weight: 200;" href="mailto:info@cvprica.com" >Kontaktirajte našu podršku!</a>
            </form>
        </div>
    </div>
</div>
</body>
</html>
