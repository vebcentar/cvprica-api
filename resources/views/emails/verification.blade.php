<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
Verifikujte svoju e-mail adresu linka ispod:<br>
<a href="{{$url}}">{{$url}}</a>

<br>
<br>
<a href="mailto:support@cvprica.com">Niste se prijavili za CV Priču? Kontaktirajte našu podršku.</a><br>
Copyright © 2021 CV Priča. All Rights Reserved. Powered by <a href="https://www.webcenter.me">Web Center</a>
</body>
</html>



