<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>CV Prica</title>
        <meta property="og:title" content="CV Prica">
        <meta name="description" content="CV Prica">
        <link rel="stylesheet" href="{{ asset('css/verification-success.css') }}">
    </head>

    <body>
        <div class="email-verification-box">
            <div class="container">
                <div class="box-container">
                    <div class="email-body">
                        <div class="logo">
                            <img src="{{ asset('img/logo.png') }}" alt="CV Prica">
                        </div>
                        <div class="email-text">
                            <h2>Verifikacija</h2>
                            <p>Uspješno ste verifikovali vašu e-mail adresu</p>
                        </div>
                        <a href="/" class="back">Nazad na početnu</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
