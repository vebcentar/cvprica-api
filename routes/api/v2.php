<?php

use App\Http\Controllers\Api\V2\Admin\CompanyController as AdminCompanyController;
use App\Http\Controllers\Api\V2\AuthCountroller;
use App\Http\Controllers\Api\V2\Candidate\NotificationController as CandidateNotificationController;
use App\Http\Controllers\Api\V2\CompanyAdController;
use App\Http\Controllers\Api\V2\CompanyQuestionController;
use App\Http\Controllers\Api\V2\InfoController;
use App\Http\Controllers\Api\V2\MeController;
use App\Http\Controllers\Api\V2\MediaController;
use App\Http\Controllers\Api\V2\VerificationController;
use App\Http\Controllers\Api\V2\ResourceController;
use App\Http\Controllers\Api\V2\NotificationController;
use App\Http\Controllers\Api\V2\ChatController;
use App\Http\Controllers\Api\V2\Admin\AdminController;
use App\Http\Controllers\Api\V2\CandidateController;
use App\Http\Controllers\Api\V2\CompanyController;
use App\Http\Controllers\Api\V2\Admin\UserController;
use App\Http\Controllers\Api\V2\Admin\AdController as AdminAdController;
use App\Http\Controllers\Api\V2\EducationController;
use App\Http\Controllers\Api\V2\VideoController;
use App\Http\Controllers\Api\V2\WorkExperienceController;
use App\Http\Controllers\Api\V2\LanguageController;
use App\Http\Controllers\Api\V2\ComputerSkillController;
use App\Http\Controllers\Api\V2\CityController;
use App\Http\Controllers\Api\V2\CountryController;
use App\Http\Controllers\Api\V2\ApplicationController;
use App\Http\Controllers\Api\V2\AdController;
use App\Http\Controllers\Api\V2\Candidate\AdController as CandidateAdController;
use App\Http\Controllers\CallbackController;

Route::group(['prefix' => 'v2', 'as' => 'v2.'], function () {

    Route::get('callback/verify-email/{code}', [ CallbackController::class, 'verifyEmail' ])->name('callback.verify_email');

    //Login
	Route::post('login', [AuthCountroller::class, 'login'])->name('login');
    Route::post('register', [AuthCountroller::class, 'register'])->name('register');
    Route::post('reset-password', [AuthCountroller::class, 'resetPassword'])->name('register');
	Route::group(['middleware' => 'auth:api'], function () {
        Route::post('change-password', [AuthCountroller::class, 'changePassword'])->name('change_password');
    	Route::post('logout', [AuthCountroller::class, 'logout'])->name('logout');
    });
    Route::post('auth/google/verify-token', [ AuthCountroller::class, 'googleVerifyToken' ])->name('google_verify_token');

    //Public
	Route::get('ui-languages', [ResourceController::class, 'uiLanguages']);
	Route::get('languages', [ResourceController::class, 'languages']);
	Route::get('language-reads', [ResourceController::class, 'languageReads']);
	Route::get('language-speaks', [ResourceController::class, 'languageSpeaks']);
	Route::get('language-writes', [ResourceController::class, 'languageWrites']);
	Route::get('computer-skill-names', [ResourceController::class, 'computer_skill_names']);
	Route::get('drivers-licence-categories', [ResourceController::class, 'driversLicenceCategories']);
	Route::get('education-areas', [ResourceController::class, 'educationAreas']);
	Route::get('education-levels', [ResourceController::class, 'educationLevels']);
	Route::get('education-titles', [ResourceController::class, 'educationTitles']);
	Route::get('genders', [ResourceController::class, 'genders']);
	Route::get('job-categories', [ResourceController::class, 'jobCategories']);
	Route::get('job-types', [ResourceController::class, 'jobTypes']);
	Route::get('type-of-works', [ResourceController::class, 'typeOfWorks']);
	Route::get('work-times', [ResourceController::class, 'workTimes']);
	Route::get('packages', [ResourceController::class, 'packages']);
	Route::get('companies', [ResourceController::class, 'companies']);
    Route::get('common-data', [ResourceController::class, 'commonData']);
    Route::get('cities', [CityController::class, 'index'])->name('index');
    Route::get('countries', [CountryController::class, 'index'])->name('index');

    //Public routes per request
    Route::group(['prefix' => 'public', 'as' => 'public.'], function () {
        Route::get('/companies', [CompanyController::class, 'publicCompanies'])->name('companies');
        Route::get('/company/{companyId}/info', [InfoController::class, 'company'])->name('company');
        Route::get('/company/{companyId}/jobs', [AdController::class, 'publicCompanyJobs'])->name('jobs');
        Route::get('/company/{companyId}/jobs/{adId}', [AdController::class, 'publicCompanyJobShow'])->name('job');
        Route::group(['prefix' => 'job-search', 'as' => 'job_search.'], function () {
            Route::get('/', [AdController::class, 'jobSearch'])->name('search')->middleware('throttle:5,1');;
            Route::get('/filters/all', [ResourceController::class, 'jobSearchFilters'])->name('filters.all');
            Route::get('/filters/company', [ResourceController::class, 'jobSearchCompanyFilter'])->name('filters.company');
            Route::get('/filters/type-of-work', [ResourceController::class, 'jobSearchTypeOfWorkFilter'])->name('filters.type_of_work');
            Route::get('/filters/location', [ResourceController::class, 'jobSearchLocationFilter'])->name('filters.location');
            Route::get('/filters/term', [ResourceController::class, 'jobSearchTermFilter'])->name('filters.term');
        });
    });

    //Authenticated user routes
	Route::group(['middleware' => 'auth:api'], function () {

        //Me routes
        Route::group(['prefix' => 'me', 'as' => 'me.'], function () {
            Route::get('/', [MeController::class, 'me'])->name('me');
            Route::get('/profile-completed', [MeController::class, 'profileCompleted'])->name('profile_completed');
            Route::post('/device-token', [MeController::class, 'setDeviceToken'])->name('set_device_token');
            Route::delete('/device-token', [MeController::class, 'deleteDeviceToken'])->name('delete_device_token');
        });

        //Media
        Route::group(['prefix' => 'media', 'as' => 'media.'], function () {
            Route::get('/video-upload-form', [MediaController::class, 'videoUploadForm'])->name('video_upload_form');
        });

        //Verification
        Route::group(['prefix' => 'verification', 'as' => 'verification.'], function () {

            //Phone verification
            Route::group(['prefix' => 'phone', 'as' => 'phone.'], function () {
                Route::post('/state', [VerificationController::class, 'generatePhoneState'])->name('generate-state');
                Route::post('/url', [VerificationController::class, 'generatePhoneVerificationUrl'])->name('generate-url');
                Route::post('/qr-code', [VerificationController::class, 'generatePhoneQRCode'])->name('generate-qr-code');
                Route::post('/send-otp', [VerificationController::class, 'sendOTP'])->name('send_otp')->middleware('throttle:2,1');
                Route::post('/confirm-otp', [VerificationController::class, 'confirmOTP'])->name('confirm_otp');
            });

            //Email verification
            Route::group(['prefix' => 'email', 'as' => 'email.'], function () {
                Route::post('send-verification-email', [VerificationController::class, 'sendVerificationEmail'])->name('send_verification_email')->middleware('throttle:2,1');;
            });
        });

        Route::group(['prefix' => 'video', 'as' => 'video.'], function () {
            Route::post('/', [VideoController::class, 'store'])->name('store');
            Route::delete('/{id}', [VideoController::class, 'delete'])->name('delete');
        });

		Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
			Route::get('/', [NotificationController::class, 'index'])->name('index');
			Route::patch('{id}/set-seen', [NotificationController::class, 'setSeen'])->name('set_seen');
		});

		Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
			Route::get('conversations', [ChatController::class, 'conversations'])->name('conversations');
			Route::get('conversations/{id}', [ChatController::class, 'conversation'])->name('conversation');
			Route::patch('conversations/{id}/set-seen', [ChatController::class, 'conversationSetSeen'])->name('conversation.set_seen');
			Route::post('send-message/{id}', [ChatController::class, 'sendMessage'])->name('send_message');
			Route::post('send-messages', [ChatController::class, 'sendMessages'])->name('send_messages');
		});

		Route::group(['prefix' => 'candidate', 'as' => 'candidate.'], function () {
            Route::group(['middleware' => 'has_role:admin,company'], function () {
                Route::get('{candidate}/info', [InfoController::class, 'candidate'])->name('candidate');
            });

            Route::group(['middleware' => 'has_role:employee'], function () {
                Route::post('fields/update-profile-image', [CandidateController::class, 'fieldsProfileImageUpdate'])->name('fields.update_profile_image');
                Route::post('update-cv-document', [CandidateController::class, 'updateCvDocument'])->name('fields.update_cv_document');
                Route::delete('cv-document', [CandidateController::class, 'deleteCvDocument'])->name('fields.delete_cv_document');
                Route::delete('cv-video', [CandidateController::class, 'deleteProfileVideo'])->name('fields.delete_profile_video');

                Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
                    Route::get('all', [CandidateNotificationController::class, 'allSubscriptions'])->name('subscriptions.all');
                    Route::get('city', [CandidateNotificationController::class, 'citySubscriptions'])->name('subscriptions.city');
                    Route::get('company', [CandidateNotificationController::class, 'companySubscriptions'])->name('subscriptions.company');
                    Route::post('company/{companyId}/subscribe', [CandidateNotificationController::class, 'subscribeToCompany'])->name('subscriptions.company.subscribe');
                    Route::post('company/{companyId}/unsubscribe', [CandidateNotificationController::class, 'unsubscribeFromCompany'])->name('subscriptions.company.unsubscribe');
                    Route::get('type-of-work', [CandidateNotificationController::class, 'typeOfWorkSubscriptions'])->name('subscriptions.type_of_work');
                });
            });

            Route::group(['middleware' => 'has_role:admin,employee'], function () {
                Route::get('dashboard', [CandidateController::class, 'dashboard'])->name('dashboard');
                Route::get('info', [CandidateController::class, 'info'])->name('info');
                Route::patch('update', [CandidateController::class, 'update'])->name('update');
                Route::patch('fields/update', [CandidateController::class, 'fieldsUpdate'])->name('fields.update');
                Route::post('update-cv-video', [CandidateController::class, 'updateProfileVideo'])->name('fields.update_profile_video');
                Route::get('download-cv-video', [CandidateController::class, 'downloadProfileVideo'])->name('download_profile_video');
                Route::get('download-cv-document', [CandidateController::class, 'downloadCVDocument'])->name('download_cv_document');
                Route::patch('update-driver-licence', [CandidateController::class, 'updateDriverLicence'])->name('update_driver_licence');
                Route::patch('deactivate', [CandidateController::class, 'deactivate'])->name('deactivate');
                Route::patch('jobs/{id}/set-favorite', [CandidateAdController::class, 'setFavorite'])->name('set_favorite');

                Route::group(['prefix' => 'educations', 'as' => 'educations.'], function () {
                    Route::get('/', [EducationController::class, 'index'])->name('index');
                    Route::post('/', [EducationController::class, 'store'])->name('store');
                    Route::patch('/{id}', [EducationController::class, 'update'])->name('update');
                    Route::delete('/{id}', [EducationController::class, 'destroy'])->name('destroy');
                });

                Route::group(['prefix' => 'work-experiences', 'as' => 'work-experiences.'], function () {
                    Route::get('/', [WorkExperienceController::class, 'index'])->name('index');
                    Route::post('/', [WorkExperienceController::class, 'store'])->name('store');
                    Route::patch('/{id}', [WorkExperienceController::class, 'update'])->name('update');
                    Route::delete('/{id}', [WorkExperienceController::class, 'destroy'])->name('destroy');
                });

                Route::group(['prefix' => 'computer-skills', 'as' => 'computer-skills.'], function () {
                    Route::get('/', [ComputerSkillController::class, 'index'])->name('index');
                    Route::post('/', [ComputerSkillController::class, 'store'])->name('store');
                    Route::patch('/{id}', [ComputerSkillController::class, 'update'])->name('update');
                    Route::delete('/{id}', [ComputerSkillController::class, 'destroy'])->name('destroy');
                });

                Route::group(['prefix' => 'languages', 'as' => 'languages.'], function () {
                    Route::get('/', [LanguageController::class, 'index'])->name('index');
                    Route::post('/', [LanguageController::class, 'store'])->name('store');
                    Route::patch('/{id}', [LanguageController::class, 'update'])->name('update');
                    Route::delete('/{id}', [LanguageController::class, 'destroy'])->name('destroy');
                });

                Route::group(['prefix' => 'applications', 'as' => 'applications.'], function () {
                    Route::get('/', [ApplicationController::class, 'index'])->name('index');
                    Route::post('/', [ApplicationController::class, 'store'])->name('store');
                    Route::get('/{id}', [ApplicationController::class, 'show'])->name('show');
                    Route::patch('/{id}', [ApplicationController::class, 'update'])->name('update');
                });
            });
		});

		Route::group(['prefix' => 'company', 'as' => 'company.'], function () {
            Route::group(['middleware' => 'has_role:admin'], function () {
                Route::get('/{companyId}/info', [CompanyController::class, 'company'])->name('company');
            });
            Route::group(['middleware' => 'has_role:company'], function () {
                Route::get('dashboard', [CompanyController::class, 'dashboard'])->name('dashboard');
                Route::get('info', [CompanyController::class, 'info'])->name('info');

                Route::group(['prefix' => 'jobs', 'as' => 'jobs.'], function () {
                    Route::get('/', [CompanyAdController::class, 'jobsList'])->name('list');
                    Route::delete('/', [CompanyAdController::class, 'delete'])->name('delete');
                    Route::post('/', [CompanyAdController::class, 'store'])->name('store');
                    Route::post('/show', [CompanyAdController::class, 'showJobs'])->name('show_jobs');
                    Route::post('/hide', [CompanyAdController::class, 'hideJobs'])->name('hide_jobs');
                    Route::post('/archive', [CompanyAdController::class, 'archive'])->name('archive');
                    Route::post('/reset', [CompanyAdController::class, 'unarchive'])->name('reset');
                    Route::get('/selected-applications', [CompanyAdController::class, 'selectedApplications'])->name('selected_applications');
                    Route::post('/applications/deselect', [CompanyAdController::class, 'deselectApplications'])->name('deselect');
                    Route::patch('/{adId}', [CompanyAdController::class, 'update'])->name('update');
                    Route::get('/{adId}/applications', [CompanyAdController::class, 'applications'])->name('applications');
                    Route::post('/{adId}/applications/select', [CompanyAdController::class, 'selectApplications'])->name('select');
                    Route::get('/{adId}/applications/{applicationId}', [CompanyAdController::class, 'application'])->name('application');
                    Route::patch('/{adId}/applications/{id}/set-eminent', [CompanyAdController::class, 'applicationSetEminent'])->name('application_set_eminent');
                });

                Route::group(['prefix' => 'ads', 'as' => 'ads.'], function () {
                    Route::get('/', [CompanyAdController::class, 'jobsList'])->name('list');
                    Route::delete('/', [CompanyAdController::class, 'delete'])->name('delete');
                    Route::post('/', [CompanyAdController::class, 'store'])->name('store');
                    Route::post('/show', [CompanyAdController::class, 'showJobs'])->name('show_jobs');
                    Route::post('/hide', [CompanyAdController::class, 'hideJobs'])->name('hide_jobs');
                    Route::post('/archive', [CompanyAdController::class, 'archive'])->name('archive');
                    Route::post('/reset', [CompanyAdController::class, 'unarchive'])->name('reset');
                    Route::get('/selected-applications', [CompanyAdController::class, 'selectedApplications'])->name('selected_applications');
                    Route::post('/applications/deselect', [CompanyAdController::class, 'deselectApplications'])->name('deselect');
                    Route::patch('/{adId}', [CompanyAdController::class, 'update'])->name('update');
                    Route::get('/{adId}/applications', [CompanyAdController::class, 'applications'])->name('applications');
                    Route::post('/{adId}/applications/select', [CompanyAdController::class, 'selectApplications'])->name('select');
                    Route::get('/{adId}/applications/{applicationId}', [CompanyAdController::class, 'application'])->name('application');
                    Route::patch('/{adId}/applications/{id}/set-eminent', [CompanyAdController::class, 'applicationSetEminent'])->name('application_set_eminent');
                });

                Route::group(['prefix' => 'questions', 'as' => 'questions.'], function () {
                    Route::get('/', [CompanyQuestionController::class, 'index'])->name('index');
                    Route::post('/', [CompanyQuestionController::class, 'store'])->name('store');
                    Route::delete('/', [CompanyQuestionController::class, 'delete'])->name('delete');
                    Route::get('/{id}', [CompanyQuestionController::class, 'show'])->name('show');
                    Route::patch('/{id}', [CompanyQuestionController::class, 'update'])->name('update');
                });

                Route::get('/applications/{applicationId}', [CompanyAdController::class, 'singleApplication'])->name('single_application');
            });

            Route::group(['middleware' => 'has_role:admin,company'], function () {
                Route::patch('update', [CompanyController::class, 'update'])->name('update');
                Route::post('update-video', [CompanyController::class, 'updateProfileVideo'])->name('fields.update_profile_video');
                Route::post('fields/update-profile-image', [CompanyController::class, 'fieldsProfileImageUpdate'])->name('fields.update_profile_image');
                Route::post('fields/update-background-image', [CompanyController::class, 'fieldsBackgroundImageUpdate'])->name('fields.update_background_image');
                Route::patch('deactivate', [CompanyController::class, 'deactivate'])->name('deactivate');
            });
		});

		Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'has_role:admin' ], function () {
			Route::get('dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
			Route::patch('update', [AdminController::class, 'update'])->name('update');
			Route::post('fields/update-profile-image', [AdminController::class, 'fieldsProfileImageUpdate'])->name('fields.update_profile_image');

			Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
				Route::get('/', [UserController::class, 'index'])->name('index');
				Route::post('/', [UserController::class, 'store'])->name('store');
				Route::patch('/', [UserController::class, 'update'])->name('update');
				Route::delete('/{id}', [UserController::class, 'destroy'])->name('destroy');
				Route::delete('/', [UserController::class, 'destroyMultiple'])->name('destroy_multiple');

				Route::patch('{id}/set-archived', [UserController::class, 'setArchived'])->name('set_archived');
				Route::patch('/set-archived', [UserController::class, 'setArchivedMultiple'])->name('set_archived_multiple');
				Route::patch('{id}/set-hidden', [UserController::class, 'setHidden'])->name('set_hidden');
				Route::patch('{id}/reset', [UserController::class, 'reset'])->name('reset');
				Route::patch('/reset', [UserController::class, 'resetMultiple'])->name('reset_multiple');
			});

            Route::get('info', [AdminController::class, 'me'])->name('me');

            Route::group(['prefix' => 'jobs', 'as' => 'jobs.'], function () {
                Route::get('/', [AdminAdController::class, 'index'])->name('index');
                Route::post('/', [AdminAdController::class, 'store'])->name('store');
                Route::get('/{id}', [AdminAdController::class, 'show'])->name('show');

                Route::patch('{id}/set-archived', [AdminAdController::class, 'setArchived'])->name('set_archived');
                Route::patch('/set-archived', [AdminAdController::class, 'setArchivedMultiple'])->name('set_archived_multiple');

                Route::delete('/{id}', [AdminAdController::class, 'destroy'])->name('destroy');
                Route::delete('/', [AdminAdController::class, 'destroyMultiple'])->name('destroy_multiple');
                Route::patch('{id}/set-hidden', [AdminAdController::class, 'setHidden'])->name('set_hidden');
                Route::patch('{id}/reset', [AdminAdController::class, 'reset'])->name('reset');
                Route::patch('/reset', [AdminAdController::class, 'resetMultiple'])->name('reset_multiple');

                Route::patch('/{id}', [AdminAdController::class, 'update'])->name('update');

            });

            Route::group(['prefix' => 'companies', 'as' => 'companies.'], function () {
                Route::post('/{companyId}/set-eminent', [AdminCompanyController::class, 'setEminent'])->name('set-eminent');
            });
        });
	});

    //Optionally authenticated user routes
	Route::group(['middleware' => '?auth:api'], function () {
        Route::group(['prefix' => 'ads', 'as' => 'ads.'], function () {
            Route::get('/', [AdController::class, 'index'])->name('index');
            Route::get('/{id}', [AdController::class, 'show'])->name('show');
        });
		Route::group(['prefix' => 'jobs', 'as' => 'jobs.'], function () {
			Route::get('/', [AdController::class, 'index'])->name('index')->middleware('throttle:8,1');
			Route::get('/{id}', [AdController::class, 'show'])->name('show');
		});
	});
});
