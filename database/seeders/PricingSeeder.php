<?php

namespace Database\Seeders;

use App\Models\Package;
use App\Models\Pricing;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricingSeeder extends Seeder
{
    public function run(): void
    {
        foreach ($this->pricing() as $package => $packagePrices) {
            $packageId = Package::idForPackage($package);
            foreach ($packagePrices as $packagePrice) {
                DB::table('pricing')->insert([
                    'duration' => $packagePrice['duration'],
                    'price' => $packagePrice['price'],
                    'package_id' => $packageId
                ]);
            }
        }
    }

    private function pricing(): array
    {
        return [
            Package::STARTER => [
                [ 'duration' => Pricing::DURATION_DAYS_15, 'price' => 19 ],
                [ 'duration' => Pricing::DURATION_DAYS_30, 'price' => 29 ],
            ],

            Package::BASIC => [
                [ 'duration' => Pricing::DURATION_DAYS_15, 'price' => 44 ],
                [ 'duration' => Pricing::DURATION_DAYS_30, 'price' => 69 ],
            ],

            Package::PLUS => [
                [ 'duration' => Pricing::DURATION_DAYS_15, 'price' => 109 ],
                [ 'duration' => Pricing::DURATION_DAYS_30, 'price' => 149 ],
            ],

            Package::PRO => [
                [ 'duration' => Pricing::DURATION_DAYS_15, 'price' => 199 ],
                [ 'duration' => Pricing::DURATION_DAYS_30, 'price' => 269 ],
            ],

            Package::ENTERPRISE => [
                [ 'duration' => Pricing::DURATION_DAYS_15, 'price' => 385 ],
                [ 'duration' => Pricing::DURATION_DAYS_30, 'price' => 499 ],
            ]
        ];
    }
}
