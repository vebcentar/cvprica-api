<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackagesSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('packages')->delete();

        foreach ($this->packageBenefits() as $package => $benefits) {
            DB::table('packages')->insertOrIgnore([
                'id' => Package::idForPackage($package),
                'name' => $package,
                'description' => '',
                'benefits' => \json_encode($benefits),
                'active' => true
            ]);
        }
    }

    private function packageBenefits(): array
    {
        return [
            Package::STARTER => [
                'Neograničen tekst oglasa' => '400 karaktera',
                'Broj gradova u oglasu' => 1,
                'Objava oglasa u državi' => 1,
                'Isticanje u rezultatima pretrage' => false,
                'Logo u oglasu i rezultatima pretrage' => true,
                'Vaš link za konkurisanje' => false,
                'Opis u rezultatima pretrage' => true,
                'Čuvanje nedovršenog/započetog oglasa' => true,
                'Statistika oglasa' => true,
                'Razgledanje kandidata u video formatu' => true,
                'Obavještenja o novim prijavama' => false,
                'Izdvajanje kandidata u posebnu listu' => false,
                'Slanje i primanje direktnih poruka kandidata i uvid u istoriju prepiske' => false,
                'Postavljanje posebnih pitanja na koje kandidat mora odgovoriti u video formatu' => false,
                'Zakazivanje video intervjua' => false,
                'Postavljanje koorporativnog videa' => false,
                'Prezentacija u sekciji za istaknute poslodavce' => false
            ],

            Package::BASIC => [
                'Neograničen tekst oglasa' => true,
                'Broj gradova u oglasu' => 5,
                'Objava oglasa u državi' => 2,
                'Isticanje u rezultatima pretrage' => false,
                'Logo u oglasu i rezultatima pretrage' => true,
                'Vaš link za konkurisanje' => false,
                'Opis u rezultatima pretrage' => true,
                'Čuvanje nedovršenog/započetog oglasa' => true,
                'Statistika oglasa' => true,
                'Razgledanje kandidata u video formatu' => true,
                'Obavještenja o novim prijavama' => true,
                'Izdvajanje kandidata u posebnu listu' => true,
                'Slanje i primanje direktnih poruka kandidata i uvid u istoriju prepiske' => false,
                'Postavljanje posebnih pitanja na koje kandidat mora odgovoriti u video formatu' => false,
                'Zakazivanje video intervjua' => false,
                'Postavljanje koorporativnog videa' => false,
                'Prezentacija u sekciji za istaknute poslodavce' => false
            ],

            Package::PLUS => [
                'Neograničen tekst oglasa' => true,
                'Broj gradova u oglasu' => true,
                'Objava oglasa u državi' => 3,
                'Isticanje u rezultatima pretrage' => true,
                'Logo u oglasu i rezultatima pretrage' => true,
                'Vaš link za konkurisanje' => true,
                'Opis u rezultatima pretrage' => true,
                'Čuvanje nedovršenog/započetog oglasa' => true,
                'Statistika oglasa' => true,
                'Razgledanje kandidata u video formatu' => true,
                'Obavještenja o novim prijavama' => true,
                'Izdvajanje kandidata u posebnu listu' => true,
                'Slanje i primanje direktnih poruka kandidata i uvid u istoriju prepiske' => true,
                'Postavljanje posebnih pitanja na koje kandidat mora odgovoriti u video formatu' => false,
                'Zakazivanje video intervjua' => false,
                'Postavljanje koorporativnog videa' => false,
                'Prezentacija u sekciji za istaknute poslodavce' => false
            ],

            Package::PRO => [
                'Neograničen tekst oglasa' => true,
                'Broj gradova u oglasu' => true,
                'Objava oglasa u državi' => 4,
                'Isticanje u rezultatima pretrage' => true,
                'Logo u oglasu i rezultatima pretrage' => true,
                'Vaš link za konkurisanje' => true,
                'Opis u rezultatima pretrage' => true,
                'Čuvanje nedovršenog/započetog oglasa' => true,
                'Statistika oglasa' => true,
                'Razgledanje kandidata u video formatu' => true,
                'Obavještenja o novim prijavama' => true,
                'Izdvajanje kandidata u posebnu listu' => true,
                'Slanje i primanje direktnih poruka kandidata i uvid u istoriju prepiske' => true,
                'Postavljanje posebnih pitanja na koje kandidat mora odgovoriti u video formatu' => true,
                'Zakazivanje video intervjua' => true,
                'Postavljanje koorporativnog videa' => false,
                'Prezentacija u sekciji za istaknute poslodavce' => false
            ],

            Package::ENTERPRISE => [
                'Neograničen tekst oglasa' => true,
                'Broj gradova u oglasu' => true,
                'Objava oglasa u državi' => true,
                'Isticanje u rezultatima pretrage' => true,
                'Logo u oglasu i rezultatima pretrage' => true,
                'Vaš link za konkurisanje' => true,
                'Opis u rezultatima pretrage' => true,
                'Čuvanje nedovršenog/započetog oglasa' => true,
                'Statistika oglasa' => true,
                'Razgledanje kandidata u video formatu' => true,
                'Obavještenja o novim prijavama' => true,
                'Izdvajanje kandidata u posebnu listu' => true,
                'Slanje i primanje direktnih poruka kandidata i uvid u istoriju prepiske' => true,
                'Postavljanje posebnih pitanja na koje kandidat mora odgovoriti u video formatu' => true,
                'Zakazivanje video intervjua' => true,
                'Postavljanje koorporativnog videa' => true,
                'Prezentacija u sekciji za istaknute poslodavce' => true
            ]
        ];
    }
}
