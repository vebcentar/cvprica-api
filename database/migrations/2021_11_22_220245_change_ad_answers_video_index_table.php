<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeAdAnswersVideoIndexTable extends Migration
{
    public function up(): void
    {
        DB::statement('ALTER TABLE `ad_answers` DROP FOREIGN KEY `ad_answers_video_id_foreign`;');
        DB::statement('ALTER TABLE `ad_answers` ADD CONSTRAINT `ad_answers_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE;');
    }

    public function down(): void
    {
    }
}
