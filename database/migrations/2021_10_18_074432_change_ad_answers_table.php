<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAdAnswersTable extends Migration
{
    public function up(): void
    {
        Schema::table('ad_answers', function (Blueprint $table) {
            $table->uuid('video_id')->nullable()->after('text_anwser');
            $table->dropColumn('video_anwser');
            $table->renameColumn('text_anwser', 'text_answer');

            $table
                ->foreign('video_id')
                ->references('id')
                ->on('videos')
                ->onDelete('SET NULL')
            ;
        });
    }

    public function down(): void
    {
        Schema::table('ad_answers', function (Blueprint $table) {
            $table->dropForeign('ad_answers_video_id_foreign');
            $table->dropColumn('video_id');
            $table->string('video_anwser')->nullable();
            $table->renameColumn('text_answer', 'text_anwser');
        });
    }
}
