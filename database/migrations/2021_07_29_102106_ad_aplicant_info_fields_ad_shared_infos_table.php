<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdAplicantInfoFieldsAdSharedInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_shared_infos', function (Blueprint $table) {
                $table->string('full_name')->nullable();
                $table->string('email')->nullable();
                $table->string('profile_image')->nullable();
                $table->string('phone')->nullable();
                $table->text('education')->nullable();
                $table->text('languages')->nullable();
                $table->text('computer_skills')->nullable();
                $table->text('experience')->nullable();
                $table->text('additional_information')->nullable();
                $table->bigInteger('gender_id')->unsigned()->nullable();
                $table->bigInteger('country_id')->unsigned()->nullable();
                $table->bigInteger('city_id')->unsigned()->nullable();
                $table->string('address')->nullable();
                

                // foreign keys
                $table->foreign('gender_id')
                    ->references('id')
                    ->on('genders')
                    ->onDelete('cascade');
                $table->foreign('country_id')
                    ->references('id')
                    ->on('countries')
                    ->onDelete('cascade');
                $table->foreign('city_id')
                    ->references('id')
                    ->on('cities')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_shared_infos', function (Blueprint $table) {
            //
        });
    }
}
