<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOneTimePasswordsTable extends Migration
{
    public function up()
    {
        Schema::create('one_time_passwords', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code', 12);
            $table->string('phone', 32);
            $table->dateTime('used_on')->nullable();
            $table->dateTime('valid_until');
        });
    }

    public function down()
    {
        Schema::dropIfExists('one_time_passwords');
    }
}
