<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeNumberOfEmployeesFromDefaultValueOnUsersTable extends Migration
{
    public function up(): void
    {
        DB::statement('ALTER TABLE `users` CHANGE COLUMN `number_of_employees_from` `number_of_employees_from` INT NOT NULL DEFAULT 0;');

    }

    public function down(): void
    {
        DB::statement('ALTER TABLE `users` CHANGE COLUMN `number_of_employees_from` `number_of_employees_from` INT NULL;');
    }
}
