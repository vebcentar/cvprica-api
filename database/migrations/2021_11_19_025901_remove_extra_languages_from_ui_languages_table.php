<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RemoveExtraLanguagesFromUiLanguagesTable extends Migration
{
    public function up(): void
    {
        DB::statement('UPDATE `users` SET `language_id` = 1 WHERE `id` > 4;');
        DB::statement('DELETE FROM `user_languages` WHERE `id` > 4;');
    }

    public function down(): void
    {
    }
}
