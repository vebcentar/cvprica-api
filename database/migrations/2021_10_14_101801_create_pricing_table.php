<?php

use App\Models\Pricing;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingTable extends Migration
{
    public function up(): void
    {
        Schema::create('pricing', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('package_id')->unsigned()->nullable();
            $table->float('price');
            $table->integer('duration')->default(Pricing::DURATION_DAYS_15);

            $table
                ->foreign('package_id')
                ->references('id')
                ->on('packages')
                ->onDelete('cascade')
            ;
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('pricing');
    }
}
