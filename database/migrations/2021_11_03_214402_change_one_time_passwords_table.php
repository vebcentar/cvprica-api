<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOneTimePasswordsTable extends Migration
{
    public function up(): void
    {
        Schema::table('one_time_passwords', function (Blueprint $table) {
            $table->renameColumn('phone', 'identifier');
            $table->string('type', 12)->after('code');
        });
    }

    public function down(): void
    {
        Schema::table('one_time_passwords', function (Blueprint $table) {
            $table->renameColumn('identifier', 'phone');
            $table->dropColumn('type');
        });
    }
}
