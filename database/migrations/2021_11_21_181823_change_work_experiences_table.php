<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeWorkExperiencesTable extends Migration
{
    public function up(): void
    {
        Schema::table('work_experiences', function (Blueprint $table) {
            DB::statement('TRUNCATE `work_experiences`;');
            $table->bigInteger('type_of_work_id')->unsigned()->after('user_id');
            $table->dropForeign('work_experiences_job_category_id_foreign');
            $table->dropColumn('job_category_id');

            $table->foreign('type_of_work_id')
                ->references('id')
                ->on('type_of_works')
                ->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::table('work_experiences', function (Blueprint $table) {
            DB::statement('TRUNCATE `work_experiences`;');
            $table->dropForeign('work_experiences_type_of_work_id_foreign');
            $table->dropColumn('type_of_work_id');
            $table->bigInteger('job_category_id')->unsigned()->after('user_id');
            $table->foreign('job_category_id')
                ->references('id')
                ->on('job_categories')
                ->onDelete('cascade');
        });
    }
}
