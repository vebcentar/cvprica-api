<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePackagesTable extends Migration
{
    public function up(): void
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->text('benefits')->nullable();
            $table->dropForeign('packages_created_by_foreign');
            $table->dropForeign('packages_modified_by_foreign');
            $table->dropForeign('packages_deleted_by_foreign');
            $table->dropColumn([
                'price',
                'duration',
                'created_by',
                'created_at',
                'updated_at',
                'modified_by',
                'deleted_by',
                'deleted_at',
                'package_type'
            ]);
            $table->boolean('active')->default(true);
        });
    }

    public function down(): void
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn([ 'benefits', 'active' ]);
            $table->integer('price');
            $table->integer('duration');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('modified_by')->unsigned()->nullable();
            $table->integer('package_type');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('modified_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('deleted_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }
}
